package oswal.jain.samaj;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import oswal.jain.samaj.ui.activities.HomeActivity;

public class CommunityApp extends MultiDexApplication {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static final String APP_PREFS = "COMMUNITY_PREFS";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        editor = preferences.edit();

        if (!preferences.getBoolean(HomeActivity.nightModeEnabled, false)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            editor.putBoolean(HomeActivity.nightModeEnabled, false);
            editor.apply();
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            editor.putBoolean(HomeActivity.nightModeEnabled, true);
            editor.apply();
        }
    }
}
