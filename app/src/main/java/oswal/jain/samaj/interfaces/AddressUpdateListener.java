package oswal.jain.samaj.interfaces;

public interface AddressUpdateListener {
    void onAddressUpdate();
}
