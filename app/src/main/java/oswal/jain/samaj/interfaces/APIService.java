package oswal.jain.samaj.interfaces;

import oswal.jain.samaj.data.models.Ads_Data;
import oswal.jain.samaj.data.models.Business_Data;
import oswal.jain.samaj.data.models.City_Data;
import oswal.jain.samaj.data.models.Family_Data;
import oswal.jain.samaj.data.models.Family_Data_Update;
import oswal.jain.samaj.data.models.Family_Member;
import oswal.jain.samaj.data.models.Feedback_Data;
import oswal.jain.samaj.data.models.Login_Data;
import oswal.jain.samaj.data.models.News_Data;
import oswal.jain.samaj.data.models.Occupation_Data;
import oswal.jain.samaj.data.models.Password;
import oswal.jain.samaj.data.models.Profile_Data;
import oswal.jain.samaj.data.models.Profile_Setup;
import oswal.jain.samaj.data.models.Register_Data;
import oswal.jain.samaj.data.models.Relation_Data;
import oswal.jain.samaj.data.models.SearchAddressData;
import oswal.jain.samaj.data.models.SearchBloodMatriData;
import oswal.jain.samaj.data.models.States_Data;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @FormUrlEncoded
    @POST("api/login")
    Call<Login_Data> userLogin(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("api/change-password")
    Call<Password> changePassword(
            @Query("Accept") String accept,
            @Header("Authorization") String token,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password
    );

    @FormUrlEncoded
    @POST("api/search-business")
    Call<Password> getAllBussinesses(
            @Query("Accept") String accept,
            @Header("Authorization") String token,
            @Field("occupation_type") String occupation_type,
            @Field("city") String city
    );

    @FormUrlEncoded
    @POST("api/add-member")
    Call<Family_Member> addFamilyMember(
            @Header("Authorization") String token,
            @Field("name") String name,
            @Field("father_name") String father_name,
            @Field("emailId") String emailId,
            @Field("password") String password,
            @Field("mobile_no") String mobile_no,
            @Field("gender") String gender,
            @Field("relation_with_vadil") String relation_with_vadil
    );

    @GET("api/news")
    Call<News_Data> getNewsData(
            @Header("Accept") String accept,
            @Header("Authorization") String authorization
    );

    @GET("api/ads")
    Call<Ads_Data> getAdsData(
            @Header("Accept") String accept,
            @Header("Authorization") String authorization
    );

    @GET("api/occupation")
    Call<Occupation_Data> getOccupationType();


    @Multipart
    @POST("api/fill-remaining-detail")
    Call<Profile_Setup> updateProfileDetails(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Part("profile_image\";filename=\"IMG.jpg\" ")
                    RequestBody file,
            @Part("name")
                    RequestBody name,
            @Part("father_name")
                    RequestBody fathersName,
            @Part("age")
                    RequestBody age,
            @Part("date_of_birth")
                    RequestBody date_of_birth,
            @Part("mobile_no")
                    RequestBody mobile_no,
            @Part("whatsappNo")
                    RequestBody whatsappNo,
            @Part("blood_group")
                    RequestBody blood_group,
            @Part("donor")
                    RequestBody donor,
            @Part("education")
                    RequestBody education,
            @Part("occupation_type")
                    RequestBody occupation_type,
            @Part("designation")
                    RequestBody designation,
            @Part("display_matrimonial")
                    RequestBody display_matrimonial,
            @Part("marital_status")
                    RequestBody marital_status,
            @Part("relation_with_vadil")
                    RequestBody relation_with_vadil,
            @Part("gender")
                    RequestBody gender,
            @Part("gotra")
                    RequestBody gotra,
            @Part("height")
                    RequestBody height,
            @Part("weight")
                    RequestBody weight,
            @Part("mama_name")
                    RequestBody mama_name,
            @Part("mother_mama")
                    RequestBody mother_mama,
            @Part("father_mama")
                    RequestBody father_mama
    );

    @Multipart
    @POST("api/profile-update")
    Call<Profile_Setup> editProfileDetails(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Part("profile_image\";filename=\"IMG.jpg\" ")
                    RequestBody file,
            @Part("name")
                    RequestBody name,
            @Part("father_name")
                    RequestBody fathersName,
            @Part("age")
                    RequestBody age,
            @Part("date_of_birth")
                    RequestBody date_of_birth,
            @Part("mobile_no")
                    RequestBody mobile_no,
            @Part("whatsappNo")
                    RequestBody whatsappNo,
            @Part("blood_group")
                    RequestBody blood_group,
            @Part("donor")
                    RequestBody donor,
            @Part("education")
                    RequestBody education,
            @Part("occupation_type")
                    RequestBody occupation_type,
            @Part("designation")
                    RequestBody designation,
            @Part("display_matrimonial")
                    RequestBody display_matrimonial,
            @Part("marital_status")
                    RequestBody marital_status,
            @Part("relation_with_vadil")
                    RequestBody relation_with_vadil,
            @Part("gender")
                    RequestBody gender,
            @Part("gotra")
                    RequestBody gotra,
            @Part("height")
                    RequestBody height,
            @Part("weight")
                    RequestBody weight,
            @Part("mama_name")
                    RequestBody mama_name,
            @Part("mother_mama")
                    RequestBody mother_mama,
            @Part("father_mama")
                    RequestBody father_mama
    );

    @FormUrlEncoded
    @POST("api/register")
    Call<Register_Data> registerNewUser(
            @Field("name") String name,
            @Field("father_name") String father_name,
            @Field("emailId") String email,
            @Field("password") String password,
            @Field("mobile_no") String mobile,
            @Field("gender") String gender,
            @Field("device_type") String device_type,
            @Field("device_token") String device_token,
            @Field("address_line1") String address_line1,
            @Field("state_id") int state_id,
            @Field("city_id") int city_id,
            @Field("pincode") String pincode,
            @Field("address_line2") String address_line2,
            @Field("native_place") String nativePlace
    );

    @GET("api/states")
    Call<States_Data> getAllStates();

    @GET("api/cities/{slug}")
    Call<City_Data> getAllStateCities(
            @Path(value = "slug", encoded = true) String state_code
    );

    @GET("api/relationship")
    Call<Relation_Data> getAllRelations();

    @FormUrlEncoded
    @POST("api/feedback")
    Call<Feedback_Data> submitFeedback(
            @Header("Authorization") String authorization,
            @Field("family_id") String family_id,
            @Field("name") String name,
            @Field("mobile_no") String mobile_no,
            @Field("ratings") String ratings,
            @Field("feedback_msg") String feedback_msg,
            @Field("device_name") String device_name
    );

    @FormUrlEncoded
    @POST("api/family-detail-update")
    Call<Family_Data_Update> submitUpdatedAddress(
            @Header("Authorization") String authorization,
            @Field("address_line1") String address_line1,
            @Field("address_line2") String address_line2,
            @Field("pincode") String pincode,
            @Field("state_id") int state_id,
            @Field("city_id") int city_id,
            @Field("native_place") String native_place
    );

    @FormUrlEncoded
    @POST("api/search-family")
    Call<SearchAddressData> getAddressBook(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Query("page") int page,
            @Field("state") String state,
            @Field("name") String name,
            @Field("gender") String gender,
            @Field("city") String city
    );

    @FormUrlEncoded
    @POST("api/search-blood-donor")
    Call<SearchBloodMatriData> getBloodBank(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Query("page") int page,
            @Field("state") String state,
            @Field("gender") String gender,
            @Field("city") String city,
            @Field("min_age") String min_age,
            @Field("max_age") String max_age,
            @Field("blood_group") String blood_group
    );

    @FormUrlEncoded
    @POST("api/search-matrimonials")
    Call<SearchBloodMatriData> getMatrimonial(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Query("page") int page,
            @Field("state") String state,
            @Field("gender") String gender,
            @Field("city") String city,
            @Field("min_age") String min_age,
            @Field("max_age") String max_age
    );

    @FormUrlEncoded
    @POST("api/search-business")
    Call<Business_Data> getBusiness(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Query("page") int page,
            @Query("occupation_type") String occupation_type,
            @Field("state") String state,
            @Field("city") String city
    );

    @FormUrlEncoded
    @POST("api/date-from-user-id")
    Call<Profile_Data> getProfileDetails(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Field("user_id") int user_id
    );

    @FormUrlEncoded
    @POST("api/family-detail-from-family-id")
    Call<Family_Data> getFamilyMembers(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Field("family_id") int family_id
    );

}
