package oswal.jain.samaj.interfaces;

public interface OnItemClick {
    void onMenuItemClick(int position);
}