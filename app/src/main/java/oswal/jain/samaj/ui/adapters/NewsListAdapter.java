package oswal.jain.samaj.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;
import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.News_Data;
import oswal.jain.samaj.widgets.BaseModulesAdapter;

import java.util.ArrayList;

public class NewsListAdapter extends BaseModulesAdapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_NEWS = 0;
    private static final int ITEM_TYPE_HEADER = 1;
    Context context;
    ArrayList<News_Data.Data.News_Details> news_list;

    public NewsListAdapter(Context context, ArrayList<News_Data.Data.News_Details> news_list) {
        this.context = context;
        this.news_list = news_list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_NEWS:
                View view = LayoutInflater.from(context).inflate(R.layout.item_news_list, parent, false);
                return new ItemHolderNews(view);
            case ITEM_TYPE_HEADER:
                View view3 = LayoutInflater.from(context).inflate(R.layout.empty_header_view, parent, false);
                return new ItemHolderHeader(view3);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ITEM_TYPE_NEWS) {
            ItemHolderNews holder1 = (ItemHolderNews) holder;
            News_Data.Data.News_Details news_details = news_list.get(position - 1);
            holder1.news_image.setShapeAppearanceModel(
                    new ShapeAppearanceModel.Builder()
                            .setAllCorners(CornerFamily.ROUNDED, 8)
                            .build()
            );
//
            holder1.headline.setText(news_details.getNews_title());
            holder1.category.setText(news_details.getCategory());
            holder1.description.setText(news_details.getNews_message());
            holder1.date.setText("" + news_details.getCreated_at());

            try {
                Glide.with(context)
                        .load(news_details.getNews_image())
                        .placeholder(R.drawable.profile5)
                        .into(holder1.news_image);
            } catch (Exception e) {
                Log.e("TAG", "onBindViewHolder: " + e.getMessage());
            }

            holder1.cardNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return news_list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return ITEM_TYPE_HEADER;
        else
            return ITEM_TYPE_NEWS;
    }

    public static class ItemHolderNews extends RecyclerView.ViewHolder {

        AppCompatTextView headline, category, date, description;
        ShapeableImageView news_image;
        MaterialCardView cardNews;

        public ItemHolderNews(@NonNull View itemView) {
            super(itemView);

            cardNews = itemView.findViewById(R.id.cardNews);
            news_image = itemView.findViewById(R.id.news_image);
            headline = itemView.findViewById(R.id.headline);
            category = itemView.findViewById(R.id.category);
            date = itemView.findViewById(R.id.date);
            description = itemView.findViewById(R.id.description);

        }

    }

    public static class ItemHolderHeader extends RecyclerView.ViewHolder {

        public ItemHolderHeader(@NonNull View itemView) {
            super(itemView);
        }
    }

}
