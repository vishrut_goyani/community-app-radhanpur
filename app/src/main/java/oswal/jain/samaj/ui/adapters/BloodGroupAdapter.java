package oswal.jain.samaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;

import org.json.JSONException;
import org.json.JSONObject;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Profile_Data;
import oswal.jain.samaj.data.models.SearchBloodMatriData;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.ui.activities.LoginActivity;
import oswal.jain.samaj.ui.activities.ProfileDetailsActivity;
import oswal.jain.samaj.ui.activities.SearchListFamilyActivity;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;
import oswal.jain.samaj.widgets.BaseModulesAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class BloodGroupAdapter extends BaseModulesAdapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_HEADER = 0;
    private static final int ITEM_TYPE_BLOOD = 1;
    private static final int ITEM_TYPE_MATRI = 2;
    private static final int ITEM_TYPE_BUSINESS = 3;
    Context context;
    ArrayList<SearchBloodMatriData.Search_Data.SearchList> profile_list_blood;
    ArrayList<SearchBloodMatriData.Search_Data.SearchList> profile_list_matrimonial;
    int searchType;

    public BloodGroupAdapter(Context context, ArrayList<SearchBloodMatriData.Search_Data.SearchList> profile_list_blood, int searchType) {
        this.context = context;
        this.profile_list_blood = profile_list_blood;
        this.searchType = searchType;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_HEADER:
                View view1 = LayoutInflater.from(context).inflate(R.layout.empty_header_view, parent, false);
                return new ItemHolderHeader(view1);
            case ITEM_TYPE_BLOOD:
                View view2 = LayoutInflater.from(context).inflate(R.layout.item_bloodgroup_list, parent, false);
                return new ItemHolderBloodGroup(view2);
            case ITEM_TYPE_MATRI:
                View view3 = LayoutInflater.from(context).inflate(R.layout.item_matrimonial_list, parent, false);
                return new ItemHolderMatrimonial(view3);
            case ITEM_TYPE_BUSINESS:
                View view4 = LayoutInflater.from(context).inflate(R.layout.item_matrimonial_list, parent, false);
                return new ItemHolderBusiness(view4);
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        if (profile_list_blood != null)
            return profile_list_blood.size() + 1;
        else
            return profile_list_matrimonial.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_TYPE_HEADER;
        } else {
            if (Objects.equals(searchType, 1))
                return ITEM_TYPE_BLOOD;
            else if (Objects.equals(searchType, 2)) {
                return ITEM_TYPE_MATRI;
            } else if (Objects.equals(searchType, 3)) {
                return ITEM_TYPE_BUSINESS;
            }
        }
        return ITEM_TYPE_HEADER;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_TYPE_BLOOD:
                ItemHolderBloodGroup holder1 = (ItemHolderBloodGroup) holder;
                SearchBloodMatriData.Search_Data.SearchList search_data = profile_list_blood.get(position - 1);
                Glide.with(context)
                        .load(search_data.getThumb())
                        .placeholder(R.drawable.profile3)
                        .into(holder1.profile_pic);
                holder1.profile_pic.setShapeAppearanceModel(
                        new ShapeAppearanceModel.Builder()
                                .setAllCorners(CornerFamily.ROUNDED, 8)
                                .build()
                );

                holder1.name.setText(search_data.getName());
                holder1.blood_group.setText("Blood Group : " + search_data.getBlood_group());
                holder1.city.setText("City : " + search_data.getCity().getCity());
                holder1.dob.setText("Date of Birth : " + search_data.getDate_of_birth());
                holder1.age.setText("Age : " + search_data.getAge() + " years");
//                holder1.relation_with_vadil.setText(search_data.getRelation_with_vadil());
                loadFamiltyHeadDetails(search_data.getFamily_id(), holder1.relation_with_vadil);
//                holder1.marital_status.setText(search_data.getMarital_status());

                if (!TextUtils.isEmpty(profile_list_blood.get(position - 1).getMobile_no())) {
                    if (Objects.equals(profile_list_blood.get(position - 1).getGender(), "male")) {
                        holder1.phone.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setText(profile_list_blood.get(position - 1).getMobile_no());
                    } else {
                        holder1.phone.setVisibility(View.GONE);
                        holder1.phoneTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.phone.setVisibility(View.GONE);
                    holder1.phoneTxt.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(profile_list_blood.get(position - 1).getWhatsappNo())) {
                    if (Objects.equals(profile_list_blood.get(position - 1).getGender(), "male")) {
                        holder1.whatsapp.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setText(profile_list_blood.get(position - 1).getWhatsappNo());
                    } else {
                        holder1.whatsapp.setVisibility(View.GONE);
                        holder1.whatsappTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.whatsapp.setVisibility(View.GONE);
                    holder1.whatsappTxt.setVisibility(View.GONE);
                }

                holder1.rl1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri u = Uri.parse("tel:" + search_data.getMobile_no());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        try {
                            context.startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });
                holder1.rl2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mobileNumber = search_data.getWhatsappNo();
                        String message = "";
                        boolean installed = AppUtils.getAppInstalledOrNot(context, "com.whatsapp");
                        if (installed) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                holder1.more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, ProfileDetailsActivity.class)
                                .putExtra(ProfileDetailsActivity.USER_ID_NEW, search_data.getId())
                                .putExtra(ProfileDetailsActivity.SEARCH_TYPE, searchType));
                    }
                });
                break;
            case ITEM_TYPE_MATRI:
                ItemHolderMatrimonial holder2 = (ItemHolderMatrimonial) holder;
                SearchBloodMatriData.Search_Data.SearchList search_data1 = profile_list_blood.get(position - 1);
                Glide.with(context)
                        .load(search_data1.getThumb())
                        .placeholder(R.drawable.profile3)
                        .into(holder2.profile_pic);
                holder2.profile_pic.setShapeAppearanceModel(
                        new ShapeAppearanceModel.Builder()
                                .setAllCorners(CornerFamily.ROUNDED, 8)
                                .build()
                );

                holder2.name.setText(search_data1.getName());
                holder2.gotra.setText("Gotra : " + search_data1.getHobbies());
                holder2.city.setText("City : " + search_data1.getCity().getCity());
                holder2.bussiness.setText("Bussiness : " + search_data1.getDesignation() + " " + search_data1.getOccupation_type());
                holder2.dob.setText("Date of Birth : " + search_data1.getDate_of_birth());
                holder2.age.setText("Age : " + search_data1.getAge() + " years");
//                holder2.relation_with_vadil.setText(search_data1.getRelation_with_vadil());
                loadFamiltyHeadDetails(search_data1.getFamily_id(), holder2.relation_with_vadil);
//                holder2.marital_status.setText(search_data1.getMarital_status());
                if (search_data1.getHeight() == null) {
                    holder2.height.setText("Height : " + "-");
                    holder2.weight.setText("Weight : " + "-");
                } else {
                    holder2.height.setText("Height : " + search_data1.getHeight());
                    holder2.weight.setText("Weight : " + search_data1.getWeight());
                }
                holder2.education.setText("Education : " + search_data1.getEducation());

                if (!TextUtils.isEmpty(profile_list_blood.get(position - 1).getMobile_no())) {
                    if (Objects.equals(profile_list_blood.get(position - 1).getGender(), "male")) {
                        holder2.phone.setVisibility(View.VISIBLE);
                        holder2.phoneTxt.setVisibility(View.VISIBLE);
                        holder2.phoneTxt.setText(profile_list_blood.get(position - 1).getMobile_no());
                    } else {
                        holder2.phone.setVisibility(View.GONE);
                        holder2.phoneTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder2.phone.setVisibility(View.GONE);
                    holder2.phoneTxt.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(profile_list_blood.get(position - 1).getWhatsappNo())) {
                    if (Objects.equals(profile_list_blood.get(position - 1).getGender(), "male")) {
                        holder2.whatsapp.setVisibility(View.VISIBLE);
                        holder2.whatsappTxt.setVisibility(View.VISIBLE);
                        holder2.whatsappTxt.setText(profile_list_blood.get(position - 1).getWhatsappNo());
                    } else {
                        holder2.whatsapp.setVisibility(View.GONE);
                        holder2.whatsappTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder2.whatsapp.setVisibility(View.GONE);
                    holder2.whatsappTxt.setVisibility(View.GONE);
                }

                holder2.rl1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri u = Uri.parse("tel:" + search_data1.getMobile_no());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        try {
                            context.startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });
                holder2.rl2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String mobileNumber = search_data1.getWhatsappNo();
                        String message = "";
                        boolean installed = AppUtils.getAppInstalledOrNot(context, "com.whatsapp");
                        if (installed) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                holder2.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("searchType", "onClick: " + searchType);
                        context.startActivity(new Intent(context, ProfileDetailsActivity.class)
                                .putExtra(ProfileDetailsActivity.USER_ID_NEW, search_data1.getId())
                                .putExtra(ProfileDetailsActivity.SEARCH_TYPE, searchType));
                    }
                });
                break;
            case ITEM_TYPE_BUSINESS:
                ItemHolderBloodGroup holder3 = (ItemHolderBloodGroup) holder;
                SearchBloodMatriData.Search_Data.SearchList search_data2 = profile_list_blood.get(position - 1);
                Glide.with(context)
                        .load(search_data2.getThumb())
                        .placeholder(R.drawable.profile3)
                        .into(holder3.profile_pic);
                holder3.profile_pic.setShapeAppearanceModel(
                        new ShapeAppearanceModel.Builder()
                                .setAllCorners(CornerFamily.ROUNDED, 8)
                                .build()
                );

                holder3.name.setText(search_data2.getName());
                holder3.blood_group.setText("Blood Group : " + search_data2.getBlood_group());
                holder3.city.setText("City : " + search_data2.getCity().getCity());
                holder3.dob.setText("Date of Birth : " + search_data2.getDate_of_birth());
                holder3.age.setText("Age : " + search_data2.getAge() + " years");
//                holder3.relation_with_vadil.setText(search_data2.getRelation_with_vadil());
                loadFamiltyHeadDetails(search_data2.getFamily_id(), holder3.relation_with_vadil);
//                holder3.marital_status.setText(search_data2.getMarital_status());

                if (!TextUtils.isEmpty(profile_list_blood.get(position - 1).getMobile_no())) {
                    if (Objects.equals(profile_list_blood.get(position - 1).getGender(), "male")) {
                        holder3.phone.setVisibility(View.VISIBLE);
                        holder3.phoneTxt.setVisibility(View.VISIBLE);
                        holder3.phoneTxt.setText(profile_list_blood.get(position - 1).getMobile_no());
                    } else {
                        holder3.phone.setVisibility(View.GONE);
                        holder3.phoneTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder3.phone.setVisibility(View.GONE);
                    holder3.phoneTxt.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(profile_list_blood.get(position - 1).getWhatsappNo())) {
                    if (Objects.equals(profile_list_blood.get(position - 1).getGender(), "male")) {
                        holder3.whatsapp.setVisibility(View.VISIBLE);
                        holder3.whatsappTxt.setVisibility(View.VISIBLE);
                        holder3.whatsappTxt.setText(profile_list_blood.get(position - 1).getWhatsappNo());
                    } else {
                        holder3.whatsapp.setVisibility(View.GONE);
                        holder3.whatsappTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder3.whatsapp.setVisibility(View.GONE);
                    holder3.whatsappTxt.setVisibility(View.GONE);
                }

                holder3.rl1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri u = Uri.parse("tel:" + search_data2.getMobile_no());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        try {
                            context.startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });
                holder3.rl2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mobileNumber = search_data2.getWhatsappNo();
                        String message = "";
                        boolean installed = AppUtils.getAppInstalledOrNot(context, "com.whatsapp");
                        if (installed) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                holder3.more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, ProfileDetailsActivity.class)
                                .putExtra(ProfileDetailsActivity.USER_ID_NEW, search_data2.getId())
                                .putExtra(ProfileDetailsActivity.SEARCH_TYPE, searchType));
                    }
                });
                break;
        }
    }

    private void loadFamiltyHeadDetails(int family_head_id, TextView txt_head_name) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Profile_Data> call = service.getProfileDetails("application/json", "Bearer " + PreferenceUtils.getInstance(context).getAuthToken(),
                family_head_id);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(context).getAuthToken() + "\n" + family_head_id);

        call.enqueue(new Callback<Profile_Data>() {
            @Override
            public void onResponse(Call<Profile_Data> call, Response<Profile_Data> response) {
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Log.e("TAG", "onResponse: " + response);
                            Profile_Data.Data profile_data = response.body().getData();
                            txt_head_name.setText("Father's Name : " + profile_data.getName());

                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(context, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile_Data> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public static class ItemHolderHeader extends RecyclerView.ViewHolder {

        public ItemHolderHeader(@NonNull View itemView) {
            super(itemView);
        }
    }

    public static class ItemHolderBloodGroup extends RecyclerView.ViewHolder {

        AppCompatTextView name, age, blood_group, dob, relation_with_vadil, /*marital_status,*/
                city;
        ShapeableImageView profile_pic;
        TextView phoneTxt, whatsappTxt;
        RelativeLayout rl1, rl2;
        ImageView phone, whatsapp;
        MaterialCardView cardProfile;
        Button more;

        public ItemHolderBloodGroup(@NonNull View itemView) {
            super(itemView);

            more = itemView.findViewById(R.id.more);
            cardProfile = itemView.findViewById(R.id.cardProfile);
            profile_pic = itemView.findViewById(R.id.profile_pic);
            name = itemView.findViewById(R.id.name);
            city = itemView.findViewById(R.id.city);
            age = itemView.findViewById(R.id.age);
            blood_group = itemView.findViewById(R.id.blood_group);
            dob = itemView.findViewById(R.id.dob);
            relation_with_vadil = itemView.findViewById(R.id.relation_with_vadil);
//            marital_status = itemView.findViewById(R.id.marital_status);
            phone = itemView.findViewById(R.id.phone);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            rl1 = itemView.findViewById(R.id.rl1);
            rl2 = itemView.findViewById(R.id.rl2);
            phoneTxt = itemView.findViewById(R.id.phoneTxt);
            whatsappTxt = itemView.findViewById(R.id.whatsappTxt);
        }

    }

    public static class ItemHolderMatrimonial extends RecyclerView.ViewHolder {

        AppCompatTextView name, age, dob, relation_with_vadil/*, marital_status*/, gotra, city, bussiness, height, weight, education;
        ShapeableImageView profile_pic;
        TextView phoneTxt, whatsappTxt;
        RelativeLayout rl1, rl2;
        ImageView phone, whatsapp;
        MaterialCardView cardProfile;
//        Button more;

        public ItemHolderMatrimonial(@NonNull View itemView) {
            super(itemView);

//            more = itemView.findViewById(R.id.more);
            cardProfile = itemView.findViewById(R.id.cardProfile);
            profile_pic = itemView.findViewById(R.id.profile_pic);
            name = itemView.findViewById(R.id.name);
            gotra = itemView.findViewById(R.id.gotra);
            city = itemView.findViewById(R.id.city);
            age = itemView.findViewById(R.id.age);
            dob = itemView.findViewById(R.id.dob);
            relation_with_vadil = itemView.findViewById(R.id.relation_with_vadil);
//            marital_status = itemView.findViewById(R.id.marital_status);
            phone = itemView.findViewById(R.id.phone);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            rl1 = itemView.findViewById(R.id.rl1);
            rl2 = itemView.findViewById(R.id.rl2);
            phoneTxt = itemView.findViewById(R.id.phoneTxt);
            whatsappTxt = itemView.findViewById(R.id.whatsappTxt);
            bussiness = itemView.findViewById(R.id.bussiness);
            height = itemView.findViewById(R.id.height);
            weight = itemView.findViewById(R.id.weight);
            education = itemView.findViewById(R.id.education);
        }

    }

    public static class ItemHolderBusiness extends RecyclerView.ViewHolder {

        AppCompatTextView name, age, dob, relation_with_vadil/*, marital_status*/, gotra, city, bussiness, height, weight, education;
        ShapeableImageView profile_pic;
        TextView phoneTxt, whatsappTxt;
        RelativeLayout rl1, rl2;
        ImageView phone, whatsapp;
        MaterialCardView cardProfile;
//        Button more;

        public ItemHolderBusiness(@NonNull View itemView) {
            super(itemView);

//            more = itemView.findViewById(R.id.more);
            cardProfile = itemView.findViewById(R.id.cardProfile);
            profile_pic = itemView.findViewById(R.id.profile_pic);
            name = itemView.findViewById(R.id.name);
            gotra = itemView.findViewById(R.id.gotra);
            city = itemView.findViewById(R.id.city);
            age = itemView.findViewById(R.id.age);
            dob = itemView.findViewById(R.id.dob);
            relation_with_vadil = itemView.findViewById(R.id.relation_with_vadil);
//            marital_status = itemView.findViewById(R.id.marital_status);
            phone = itemView.findViewById(R.id.phone);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            rl1 = itemView.findViewById(R.id.rl1);
            rl2 = itemView.findViewById(R.id.rl2);
            phoneTxt = itemView.findViewById(R.id.phoneTxt);
            whatsappTxt = itemView.findViewById(R.id.whatsappTxt);
            bussiness = itemView.findViewById(R.id.bussiness);
            height = itemView.findViewById(R.id.height);
            weight = itemView.findViewById(R.id.weight);
            education = itemView.findViewById(R.id.education);
        }

    }

}
