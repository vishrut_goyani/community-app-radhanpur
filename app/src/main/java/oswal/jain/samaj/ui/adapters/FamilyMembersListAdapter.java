package oswal.jain.samaj.ui.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.City_Data;
import oswal.jain.samaj.data.models.Family_Data;
import oswal.jain.samaj.data.models.Family_Data_Update;
import oswal.jain.samaj.data.models.Profile_Data;
import oswal.jain.samaj.data.models.States_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.interfaces.AddressUpdateListener;
import oswal.jain.samaj.ui.activities.LoginActivity;
import oswal.jain.samaj.ui.activities.ProfileDetailsActivity;
import oswal.jain.samaj.ui.activities.SearchListFamilyActivity;
import oswal.jain.samaj.ui.fragments.SearchFilterBuilder;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;
import oswal.jain.samaj.widgets.BaseModulesAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FamilyMembersListAdapter extends BaseModulesAdapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_MEMBERS = 0;
    private static final int ITEM_TYPE_HEADER = 1;
    private static final int ITEM_TYPE_ADDRESS = 2;
    Context context;
    ArrayList<Family_Data.Data.Family_Details> members_list;
    Family_Data.Data family_data;
    int searchType;

    ArrayAdapter<String> adapterState, adapterCity;
    ArrayList<States_Data.State_Details> stateList;
    ArrayList<City_Data.City_Details> cityList;
    int state_id = -1, city_id = -1;

    AddressUpdateListener addressUpdateListener;

    public FamilyMembersListAdapter(Context context, ArrayList<Family_Data.Data.Family_Details> members_list, Family_Data.Data family_data, int searchType, AddressUpdateListener addressUpdateListener) {
        this.context = context;
        this.members_list = members_list;
        this.family_data = family_data;
        this.searchType = searchType;
        this.addressUpdateListener = addressUpdateListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_ADDRESS:
                View view1 = LayoutInflater.from(context).inflate(R.layout.item_header_address, parent, false);
                return new ItemAddressHeader(view1);
            case ITEM_TYPE_MEMBERS:
                View view2 = LayoutInflater.from(context).inflate(R.layout.item_family_members_list, parent, false);
                return new ItemHolder(view2);
            case ITEM_TYPE_HEADER:
                View view3 = LayoutInflater.from(context).inflate(R.layout.empty_header_view, parent, false);
                return new ItemHolderHeader(view3);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_TYPE_MEMBERS:
                ItemHolder holder1 = (ItemHolder) holder;
                Glide.with(context)
                        .load(members_list.get(position - 2).getThumb())
                        .placeholder(R.drawable.profile3)
                        .into(holder1.profile_pic);
                holder1.profile_pic.setShapeAppearanceModel(
                        new ShapeAppearanceModel.Builder()
                                .setAllCorners(CornerFamily.ROUNDED, 8)
                                .build()
                );

                holder1.name.setText(members_list.get(position - 2).getName());
                loadFamiltyHeadDetails(family_data.getFamily_head_id(), holder1.relation_with_vadil);
//                holder1.relation_with_vadil.setText(holder1.relation_with_vadil.getText() + members_list.get(position - 2).getRelation_with_vadil());
                holder1.age.setText(holder1.age.getText() + String.valueOf(members_list.get(position - 2).getAge()));
                holder1.education.setText(holder1.education.getText() + members_list.get(position - 2).getEducation());
                holder1.marital_status.setText(holder1.marital_status.getText() + members_list.get(position - 2).getMarital_status());
                holder1.blood_group.setText(holder1.blood_group.getText() + members_list.get(position - 2).getBlood_group());
                if (!TextUtils.isEmpty(members_list.get(position - 2).getMobile_no())) {
                    if (Objects.equals(members_list.get(position - 2).getGender(), "male")) {
                        holder1.phone.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setText(members_list.get(position - 2).getMobile_no());
                    } else {
                        holder1.phone.setVisibility(View.GONE);
                        holder1.phoneTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.phone.setVisibility(View.GONE);
                    holder1.phoneTxt.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(members_list.get(position - 2).getWhatsappNo())) {
                    if (Objects.equals(members_list.get(position - 2).getGender(), "male")) {
                        holder1.whatsapp.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setText(members_list.get(position - 2).getWhatsappNo());
                    } else {
                        holder1.whatsapp.setVisibility(View.GONE);
                        holder1.whatsappTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.whatsapp.setVisibility(View.GONE);
                    holder1.whatsappTxt.setVisibility(View.GONE);
                }

                holder1.rl1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri u = Uri.parse("tel:" + members_list.get(position - 2).getMobile_no());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        try {
                            context.startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });

                holder1.rl2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mobileNumber = members_list.get(position - 2).getWhatsappNo();
                        String message = "";
                        boolean installed = AppUtils.getAppInstalledOrNot(context, "com.whatsapp");
                        if (installed) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                holder1.more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, ProfileDetailsActivity.class)
                                .putExtra(ProfileDetailsActivity.USER_ID_NEW,
                                        members_list.get(position - 2).getId())
                                .putExtra(ProfileDetailsActivity.SEARCH_TYPE, searchType));
                    }
                });
                break;
            case ITEM_TYPE_ADDRESS:
                ItemAddressHeader holderAddress = (ItemAddressHeader) holder;
                Family_Data.Data data = family_data;
                loadFamiltyHeadDetails(data.getFamily_head_id(), holderAddress.txt_head_name);
                if (data.getState().getName() == null)
                    holderAddress.txt_header_address.setText(data.getAddress_line1() + "\n"
                            + data.getAddress_line2() + "\n"
                            + data.getNative_place() + "\n"
                            + "-" + "\n"
                            + data.getCity().getCity() + "\n"
                            + data.getPincode());
                else
                    holderAddress.txt_header_address.setText(data.getAddress_line1() + "\n"
                            + data.getAddress_line2() + "\n"
                            + data.getNative_place() + "\n"
                            + data.getState().getName() + "\n"
                            + data.getCity().getCity() + "\n"
                            + data.getPincode());

                if (Objects.equals(PreferenceUtils.getInstance(context).getUserDetails().getRelation_with_vadil().toLowerCase(), "self")) {
                    holderAddress.edit_address_img.setVisibility(View.VISIBLE);
                } else {
                    holderAddress.edit_address_img.setVisibility(View.GONE);
                }
                holderAddress.edit_address_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holderAddress.searchFilterBuilder = SearchFilterBuilder.with(((SearchListFamilyActivity) context).getSupportFragmentManager())
                                .title("Update address")
                                .setupLayout(context, R.layout.bottom_sheet_address_update);
                        holderAddress.searchFilterBuilder.show();
                        View searchSheetView = holderAddress.searchFilterBuilder.getLayout();
                        initAddressUpdatesheet(holderAddress, searchSheetView, holderAddress);
                    }
                });
                break;
        }
    }

    private void loadFamiltyHeadDetails(int family_head_id, TextView txt_head_name) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Profile_Data> call = service.getProfileDetails("application/json", "Bearer " + PreferenceUtils.getInstance(context).getAuthToken(),
                family_head_id);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(context).getAuthToken() + "\n" + family_head_id);

        call.enqueue(new Callback<Profile_Data>() {
            @Override
            public void onResponse(Call<Profile_Data> call, Response<Profile_Data> response) {
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Log.e("TAG", "onResponse: " + response);
                            Profile_Data.Data profile_data = response.body().getData();
                            txt_head_name.setText(profile_data.getName());

                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(context, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile_Data> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return members_list.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return ITEM_TYPE_HEADER;
        else if (position == 1)
            return ITEM_TYPE_ADDRESS;
        else
            return ITEM_TYPE_MEMBERS;

    }

    private void initAddressUpdatesheet(ItemAddressHeader holderAddress, View searchSheetView, ItemAddressHeader header_holder) {
        Button updateBtn;
        TextInputEditText editAddress1, editAddress2, editPincode, editNativePlace;
        AutoCompleteTextView stateDropdown, cityDropdown;

        stateList = new ArrayList<>();
        cityList = new ArrayList<>();
        state_id = -1;
        city_id = -1;

        updateBtn = searchSheetView.findViewById(R.id.updateBtn);
        editAddress1 = searchSheetView.findViewById(R.id.editAddress1);
        editPincode = searchSheetView.findViewById(R.id.editPincode);
        editAddress2 = searchSheetView.findViewById(R.id.editAddress2);
        editNativePlace = searchSheetView.findViewById(R.id.editNativePlace);
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        loadStatesList(stateDropdown, cityDropdown);
        editAddress1.setText(family_data.getAddress_line1());
        editAddress2.setText(family_data.getAddress_line2());
        editPincode.setText(family_data.getPincode());
        editNativePlace.setText(family_data.getNative_place());

        updateBtn.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(editAddress1.getText().toString())
                    && !TextUtils.isEmpty(editPincode.getText().toString())
                    && !TextUtils.isEmpty(editNativePlace.getText().toString())
                    && state_id != -1
                    && city_id != -1) {
                if (AppUtils.isNetworkAvailable(context)) {
                    holderAddress.searchFilterBuilder.dismiss();
                    String address1 = editAddress1.getText().toString();
                    String address2 = editAddress2.getText().toString();
                    String pincode = editPincode.getText().toString();
                    String nativePlace = editNativePlace.getText().toString();

                    submitAddress(address1, address2, pincode, state_id, city_id, nativePlace);
                } else {
                    Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "You must fill all the details to continue", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadStatesList(AutoCompleteTextView stateDropdown, AutoCompleteTextView cityDropdown) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading states...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<States_Data> call = service.getAllStates();

        call.enqueue(new Callback<States_Data>() {
            @Override
            public void onResponse(Call<States_Data> call, Response<States_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        stateDropdown.setEnabled(true);
                        cityDropdown.setEnabled(true);
                        stateList = response.body().getData();
                        String[] statesArray = new String[stateList.size()];
                        adapterState =
                                new ArrayAdapter<String>(
                                        context,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        statesArray);

                        stateDropdown.setAdapter(adapterState);
                        for (int i = 0; i < stateList.size(); i++) {
                            statesArray[i] = stateList.get(i).getName();
                        }
                        stateDropdown.setOnItemClickListener((parent, view, position, id) -> {
                            state_id = stateList.get(position).getId();
                            loadCitiesList(stateList.get(position).getId(), cityDropdown);
                        });

                        stateDropdown.setText(family_data.getState().getName(), false);
                        cityDropdown.setText(family_data.getCity().getCity(), false);

                    } else if (response.code() == 400) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        Toast.makeText(context, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<States_Data> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadCitiesList(int id, AutoCompleteTextView cityDropdown) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading cities...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<City_Data> call = service.getAllStateCities(String.valueOf(id));

        call.enqueue(new Callback<City_Data>() {
            @Override
            public void onResponse(Call<City_Data> call, Response<City_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        cityList = response.body().getData();
                        String[] citiesArray = new String[cityList.size()];
                        for (int i = 0; i < cityList.size(); i++) {
                            citiesArray[i] = cityList.get(i).getCity();
                        }
                        adapterCity =
                                new ArrayAdapter<String>(
                                        context,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        citiesArray);
                        cityDropdown.setAdapter(adapterCity);
                        cityDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            city_id = cityList.get(position).getId();
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(context, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<City_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitAddress(String address_line1, String address_line2, String pincode, int state_id, int city_id, String native_place) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Family_Data_Update> call = service.submitUpdatedAddress(
                "Bearer " + PreferenceUtils.getInstance(context).getAuthToken(),
                address_line1, address_line2, pincode, state_id, city_id, native_place);

        call.enqueue(new Callback<Family_Data_Update>() {
            @Override
            public void onResponse(Call<Family_Data_Update> call, Response<Family_Data_Update> response) {

                if (response.body() != null) {
                    if (response.code() == 200) {
                        try {
                            Toast.makeText(context, "Address updated", Toast.LENGTH_SHORT).show();
                            addressUpdateListener.onAddressUpdate();
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 400) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                    } else if (response.code() == 401) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                    } else if (response.code() == 500) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(context, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Family_Data_Update> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        AppCompatTextView name, relation_with_vadil, education, age, marital_status, blood_group;
        AppCompatImageView phone, whatsapp;
        TextView phoneTxt, whatsappTxt;
        RelativeLayout rl1, rl2;
        ShapeableImageView profile_pic;
        MaterialCardView cardProfile;
        Button more;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            cardProfile = itemView.findViewById(R.id.cardProfile);
            profile_pic = itemView.findViewById(R.id.profile_pic);
            name = itemView.findViewById(R.id.name);
            relation_with_vadil = itemView.findViewById(R.id.relation_with_vadil);
            education = itemView.findViewById(R.id.education);
            age = itemView.findViewById(R.id.age);
            marital_status = itemView.findViewById(R.id.marital_status);
            blood_group = itemView.findViewById(R.id.blood_group);
            rl1 = itemView.findViewById(R.id.rl1);
            rl2 = itemView.findViewById(R.id.rl2);
            phone = itemView.findViewById(R.id.phone);
            phoneTxt = itemView.findViewById(R.id.phoneTxt);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            whatsappTxt = itemView.findViewById(R.id.whatsappTxt);
            more = itemView.findViewById(R.id.more);
        }

    }

    public static class ItemAddressHeader extends RecyclerView.ViewHolder {
        SearchFilterBuilder searchFilterBuilder;
        TextView txt_header_address, txt_head_name;
        AppCompatImageView edit_address_img;

        public ItemAddressHeader(@NonNull View itemView) {
            super(itemView);
            txt_header_address = itemView.findViewById(R.id.txt_header_address);
            txt_head_name = itemView.findViewById(R.id.txt_head_name);
            edit_address_img = itemView.findViewById(R.id.edit_address_img);
        }
    }

    public static class ItemHolderHeader extends RecyclerView.ViewHolder {

        public ItemHolderHeader(@NonNull View itemView) {
            super(itemView);
        }
    }

}
