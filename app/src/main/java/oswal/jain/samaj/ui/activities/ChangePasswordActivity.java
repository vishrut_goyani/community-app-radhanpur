package oswal.jain.samaj.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Password;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.PreferenceUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChangePasswordActivity extends AppCompatActivity {

    TextInputEditText editPassOld, editPassNew;
    Button changePassBtn;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        editPassOld = findViewById(R.id.editPassOld);
        editPassNew = findViewById(R.id.editPassNew);
        changePassBtn = findViewById(R.id.changePassBtn);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

        changePassBtn.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(editPassOld.getText().toString())
                    && !TextUtils.isEmpty(editPassNew.getText().toString())) {
                changePassword(editPassOld.getText().toString(), editPassNew.getText().toString());
            } else if (TextUtils.isEmpty(editPassOld.getText().toString())) {
                editPassOld.requestFocus();
                editPassOld.requestFocus();
            } else {
                editPassNew.requestFocus();
                editPassNew.requestFocus();
            }
        });
    }

    private void changePassword(String oldPassword, String newPassword) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Changing password...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Password> call = service.changePassword("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                oldPassword, newPassword);
        call.enqueue(new Callback<Password>() {
            @Override
            public void onResponse(Call<Password> call, Response<Password> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Toast.makeText(ChangePasswordActivity.this, response.body().getData().getMsg(), Toast.LENGTH_SHORT).show();
                            PreferenceUtils.getInstance(ChangePasswordActivity.this).setLoginStatus(false);
                            startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class));
                            finish();
                        } catch (Exception e) {
                            Toast.makeText(ChangePasswordActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(ChangePasswordActivity.this, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(ChangePasswordActivity.this, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(ChangePasswordActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "Error occured. Login again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        Toast.makeText(ChangePasswordActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Password> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(ChangePasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("TAG", "onResponse: " + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}