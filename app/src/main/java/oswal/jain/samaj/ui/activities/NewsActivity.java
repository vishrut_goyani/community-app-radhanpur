package oswal.jain.samaj.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.json.JSONException;
import org.json.JSONObject;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.News_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.ui.adapters.NewsListAdapter;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;
import oswal.jain.samaj.widgets.BaseRecyclerView;
import oswal.jain.samaj.widgets.HidingScrollListener;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewsActivity extends AppCompatActivity {

    Toolbar toolbar;
    BaseRecyclerView recyclerview;
    ArrayList<News_Data.Data.News_Details> news_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        init();
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.theme_color));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        recyclerview.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });

        if (AppUtils.isNetworkAvailable(this))
            getNewsData();
        else
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
    }

    private void getNewsData() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading news...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<News_Data> call = service.getNewsData("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken());

        call.enqueue(new Callback<News_Data>() {
            @Override
            public void onResponse(Call<News_Data> call, Response<News_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            news_list = response.body().getData().getNews_details();
                            NewsListAdapter newsListAdapter = new NewsListAdapter(NewsActivity.this, news_list);
                            recyclerview.setAdapter(newsListAdapter);
                        } catch (Exception e) {
                            Toast.makeText(NewsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(NewsActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(NewsActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(NewsActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(NewsActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(NewsActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(NewsActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        Toast.makeText(NewsActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<News_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(NewsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hideViews() {
        toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    private void init() {
        news_list = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
    }

}