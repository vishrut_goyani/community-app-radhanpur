package oswal.jain.samaj.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Business_Data;
import oswal.jain.samaj.data.models.City_Data;
import oswal.jain.samaj.data.models.Occupation_Data;
import oswal.jain.samaj.data.models.Pagination;
import oswal.jain.samaj.data.models.SearchAddressData;
import oswal.jain.samaj.data.models.SearchBloodMatriData;
import oswal.jain.samaj.data.models.States_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.interfaces.OnItemClick;
import oswal.jain.samaj.ui.adapters.BloodGroupAdapter;
import oswal.jain.samaj.ui.adapters.BusinessAdapter;
import oswal.jain.samaj.ui.adapters.PageListAdapter;
import oswal.jain.samaj.ui.adapters.ProfilesListAdapter;
import oswal.jain.samaj.ui.fragments.SearchFilterBuilder;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.PreferenceUtils;
import oswal.jain.samaj.widgets.BaseRecyclerView;
import oswal.jain.samaj.widgets.HidingScrollListener;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static oswal.jain.samaj.ui.activities.HomeActivity.BLOOD_GROUP;
import static oswal.jain.samaj.ui.activities.HomeActivity.CITY_ID;
import static oswal.jain.samaj.ui.activities.HomeActivity.MAX_AGE;
import static oswal.jain.samaj.ui.activities.HomeActivity.MIN_AGE;
import static oswal.jain.samaj.ui.activities.HomeActivity.STATE_ID;
import static oswal.jain.samaj.ui.activities.HomeActivity.USER_GENDER;
import static oswal.jain.samaj.ui.activities.HomeActivity.USER_NAME;
import static oswal.jain.samaj.ui.activities.HomeActivity.USER_OCCUPATION;

public class SearchProfilesActivity extends AppCompatActivity {

    public static final String USER_ID_FAMILY_HEAD = "family_head_id";
    public static final String SEARCH_TYPE = "search_type";
    Toolbar toolbar;
    BaseRecyclerView rv_profile_list, rv_pages;
    SearchFilterBuilder searchFilterBuilder;
    AppCompatImageView search_filter;
    ArrayList<SearchAddressData.Search_Data.SearchList> profiles_address_list;
    ArrayList<SearchBloodMatriData.Search_Data.SearchList> profiles_blood_list;
    ArrayList<SearchBloodMatriData.Search_Data.SearchList> profiles_matrimonial_list;
    ArrayList<Business_Data.Business_Details.Data> profiles_business_list;
    ArrayList<Occupation_Data.Occupation_Details> occupationList;
    int searchType;
    String occupation_type;
    String name, gender, blood_group, min_age, max_age, state_id, city_id;
    ArrayList<Pagination> pageList;
    int current_page, last_page;
    RadioButton radioMale, radioFemale;
    AutoCompleteTextView stateDropdown, cityDropdown, bloodGroupDropdown, occupationDropdown;
    TextInputEditText editName, editMinAge, editMaxAge;

    ArrayList<States_Data.State_Details> stateList;
    ArrayList<City_Data.City_Details> cityList;
    ArrayAdapter<String> adapterState, adapterCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_profiles);

        init();

        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.theme_color));
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        rv_profile_list.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });

        searchType = getIntent().getIntExtra(SEARCH_TYPE, -1);
        name = getIntent().getStringExtra(USER_NAME);
        gender = getIntent().getStringExtra(USER_GENDER);
        state_id = getIntent().getStringExtra(STATE_ID);
        city_id = getIntent().getStringExtra(CITY_ID);
        min_age = getIntent().getStringExtra(MIN_AGE);
        max_age = getIntent().getStringExtra(MAX_AGE);
        blood_group = getIntent().getStringExtra(BLOOD_GROUP);
        occupation_type = getIntent().getStringExtra(USER_OCCUPATION);

        if (searchType == 0) {
            loadAddressProfilesList(name, gender, state_id, city_id, 1);
        } else if (searchType == 1) {
            loadBloodProfilesList(gender, state_id, city_id, min_age, max_age, blood_group, 1);
        } else if (searchType == 2) {
            loadMatrimonialProfilesList(gender, state_id, city_id, min_age, max_age, 1);
        } else if (searchType == 3) {
            loadBussinessProfilesList(occupation_type, state_id, city_id, 1);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rv_profile_list.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }

    private void loadAddressProfilesList(String name, String gender, String state_id, String city_id, int page_no) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading profiles...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);

        Call<SearchAddressData> call = service.getAddressBook("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                page_no, state_id, name, gender, city_id);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(this).getAuthToken() + "\n" + state_id + "\n" + name + "\n" + gender + "\n" + city_id);

        call.enqueue(new Callback<SearchAddressData>() {
            @Override
            public void onResponse(Call<SearchAddressData> call, Response<SearchAddressData> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            profiles_address_list = new ArrayList<>();
                            profiles_address_list = response.body().getData().getProfilesList();

                            if (profiles_address_list.size() == 0) {
                                Toast.makeText(SearchProfilesActivity.this, "No User Found", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("TAG", "onProfiles: " + profiles_address_list.size());
                            ProfilesListAdapter profilesListAdapter = new ProfilesListAdapter(SearchProfilesActivity.this, profiles_address_list, searchType);
                            rv_profile_list.setAdapter(profilesListAdapter);
                            Log.e("TAG", "onResponse: " + response);

                            current_page = response.body().getData().getCurrent_page();
                            last_page = response.body().getData().getLast_page();
                            pageList = new ArrayList<>();
                            for (int i = 0; i < last_page; i++) {
                                if (i == (current_page - 1)) {
                                    pageList.add(new Pagination(String.valueOf(i + 1), true));
                                } else {
                                    pageList.add(new Pagination(String.valueOf(i + 1), false));
                                }
                            }
                            PageListAdapter pagelistAdapter = new PageListAdapter(SearchProfilesActivity.this, pageList, new OnItemClick() {
                                @Override
                                public void onMenuItemClick(int position) {
                                    loadAddressProfilesList(name, gender, state_id, city_id, position + 1);
                                }
                            });
                            if (last_page > 1) {
                                rv_pages.setAdapter(pagelistAdapter);
                            }

                        } catch (Exception e) {
                            Toast.makeText(SearchProfilesActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchProfilesActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchProfilesActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchProfilesActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchAddressData> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchProfilesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadBloodProfilesList(String gender, String state_id, String city_id, String min_age, String max_age, String blood_group, int page_no) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading profiles...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<SearchBloodMatriData> call = service.getBloodBank("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                page_no, state_id, gender, city_id, min_age, max_age, blood_group);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(this).getAuthToken() + "\n" + state_id + "\n" + gender + "\n" + city_id);

        call.enqueue(new Callback<SearchBloodMatriData>() {
            @Override
            public void onResponse(Call<SearchBloodMatriData> call, Response<SearchBloodMatriData> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            profiles_blood_list = new ArrayList<>();
                            profiles_blood_list = response.body().getData().getProfilesList();
                            if (profiles_blood_list.size() == 0) {
                                Toast.makeText(SearchProfilesActivity.this, "No User Found", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("TAG", "onProfiles: " + profiles_blood_list.size());
                            BloodGroupAdapter bloodGroupAdapter = new BloodGroupAdapter(SearchProfilesActivity.this, profiles_blood_list, searchType);
                            rv_profile_list.setAdapter(bloodGroupAdapter);
                            Log.e("TAG", "onResponse: " + response);

                            current_page = response.body().getData().getCurrent_page();
                            last_page = response.body().getData().getLast_page();
                            pageList = new ArrayList<>();
                            for (int i = 0; i < last_page; i++) {
                                if (i == (current_page - 1)) {
                                    pageList.add(new Pagination(String.valueOf(i + 1), true));
                                } else {
                                    pageList.add(new Pagination(String.valueOf(i + 1), false));
                                }
                            }
                            PageListAdapter pagelistAdapter = new PageListAdapter(SearchProfilesActivity.this, pageList, new OnItemClick() {
                                @Override
                                public void onMenuItemClick(int position) {
                                    loadBloodProfilesList(gender, state_id, city_id, min_age, max_age, blood_group, position + 1);
                                }
                            });
                            if (last_page > 1) {
                                rv_pages.setAdapter(pagelistAdapter);
                            }
                        } catch (Exception e) {
                            Toast.makeText(SearchProfilesActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchProfilesActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchProfilesActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchProfilesActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchBloodMatriData> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchProfilesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMatrimonialProfilesList(String gender, String state_id, String city_id, String min_age, String max_age, int page_no) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading profiles...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<SearchBloodMatriData> call = service.getMatrimonial("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                page_no, state_id, gender, city_id, min_age, max_age);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(this).getAuthToken() + "\n" + state_id + "\n" + gender + "\n" + city_id);

        call.enqueue(new Callback<SearchBloodMatriData>() {
            @Override
            public void onResponse(Call<SearchBloodMatriData> call, Response<SearchBloodMatriData> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            profiles_matrimonial_list = new ArrayList<>();
                            profiles_matrimonial_list = response.body().getData().getProfilesList();
                            if (profiles_matrimonial_list.size() == 0) {
                                Toast.makeText(SearchProfilesActivity.this, "No User Found", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("TAG", "onProfiles: " + profiles_matrimonial_list.size());
                            BloodGroupAdapter matrimonialAdapter = new BloodGroupAdapter(SearchProfilesActivity.this, profiles_matrimonial_list, searchType);
                            rv_profile_list.setAdapter(matrimonialAdapter);
                            Log.e("TAG", "onResponse: " + response);

                            current_page = response.body().getData().getCurrent_page();
                            last_page = response.body().getData().getLast_page();
                            pageList = new ArrayList<>();
                            for (int i = 0; i < last_page; i++) {
                                if (i == (current_page - 1)) {
                                    pageList.add(new Pagination(String.valueOf(i + 1), true));
                                } else {
                                    pageList.add(new Pagination(String.valueOf(i + 1), false));
                                }
                            }
                            PageListAdapter pagelistAdapter = new PageListAdapter(SearchProfilesActivity.this, pageList, new OnItemClick() {
                                @Override
                                public void onMenuItemClick(int position) {
                                    loadMatrimonialProfilesList(gender, state_id, city_id, min_age, max_age, position + 1);
                                }
                            });
                            if (last_page > 1) {
                                rv_pages.setAdapter(pagelistAdapter);
                            }
                        } catch (Exception e) {
                            Toast.makeText(SearchProfilesActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchProfilesActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchProfilesActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchProfilesActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchBloodMatriData> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchProfilesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadBussinessProfilesList(String occupation_type, String state_id, String city_id, int page_no) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading profiles...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Business_Data> call = service.getBusiness("application/json",
                "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                page_no, occupation_type, state_id, city_id);

        Log.e("TAG", "loadProfilesList: " + state_id + "\n" + occupation_type + "\n" + city_id);

        call.enqueue(new Callback<Business_Data>() {
            @Override
            public void onResponse(Call<Business_Data> call, Response<Business_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            profiles_business_list = new ArrayList<>();
                            profiles_business_list = response.body().getData().getBusiness_list();
                            if (profiles_business_list.size() == 0) {
                                Toast.makeText(SearchProfilesActivity.this, "No User Found", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("TAG", "onProfiles: " + profiles_business_list.size());
                            BusinessAdapter businessAdapter = new BusinessAdapter(SearchProfilesActivity.this, profiles_business_list, searchType);
                            rv_profile_list.setAdapter(businessAdapter);
                            Log.e("TAG", "onResponse: " + response);

                            current_page = response.body().getData().getCurrent_page();
                            last_page = response.body().getData().getLast_page();
                            pageList = new ArrayList<>();
                            for (int i = 0; i < last_page; i++) {
                                if (i == (current_page - 1)) {
                                    pageList.add(new Pagination(String.valueOf(i + 1), true));
                                } else {
                                    pageList.add(new Pagination(String.valueOf(i + 1), false));
                                }
                            }
                            PageListAdapter pagelistAdapter = new PageListAdapter(SearchProfilesActivity.this, pageList, new OnItemClick() {
                                @Override
                                public void onMenuItemClick(int position) {
                                    loadBussinessProfilesList(occupation_type, state_id, city_id, position + 1);
                                }
                            });
                            if (last_page > 1) {
                                rv_pages.setAdapter(pagelistAdapter);
                            }
                        } catch (Exception e) {
                            Toast.makeText(SearchProfilesActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchProfilesActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchProfilesActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchProfilesActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Business_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchProfilesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hideViews() {
        toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    private void init() {
        profiles_address_list = new ArrayList<>();
        profiles_blood_list = new ArrayList<>();
        profiles_matrimonial_list = new ArrayList<>();
        profiles_business_list = new ArrayList<>();
        stateList = new ArrayList<>();
        cityList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        search_filter = findViewById(R.id.search_filter);
        rv_profile_list = findViewById(R.id.rv_profile_list);
        rv_profile_list.setLayoutManager(new LinearLayoutManager(this));

        rv_pages = findViewById(R.id.rv_pages);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.HORIZONTAL);
        rv_pages.setLayoutManager(manager);
        search_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchType == 0) {
                    searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                            .title("Search")
                            .setupLayout(SearchProfilesActivity.this, R.layout.search_filter_sheet_address_book);
                    searchFilterBuilder.show();
                    View searchSheetView1 = searchFilterBuilder.getLayout();
                    initAddressBooksheet(searchSheetView1);
                } else if (searchType == 1) {
                    searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                            .title("Search")
                            .setupLayout(SearchProfilesActivity.this, R.layout.search_filter_sheet_blood_bank);
                    searchFilterBuilder.show();
                    View searchSheetView2 = searchFilterBuilder.getLayout();
                    initBloodbanksheet(searchSheetView2);
                } else if (searchType == 2) {
                    searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                            .title("Search")
                            .setupLayout(SearchProfilesActivity.this, R.layout.search_filter_sheet_matrimonial);
                    searchFilterBuilder.show();
                    View searchSheetView3 = searchFilterBuilder.getLayout();
                    initMatrimonialsheet(searchSheetView3);
                } else if (searchType == 3) {
                    searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                            .title("Search")
                            .setupLayout(SearchProfilesActivity.this, R.layout.search_filter_sheet_bussiness);
                    searchFilterBuilder.show();
                    View searchSheetView4 = searchFilterBuilder.getLayout();
                    initBussinessSheet(searchSheetView4);
                }
            }
        });
    }

    private void initAddressBooksheet(View searchSheetView) {
        radioMale = searchSheetView.findViewById(R.id.radioMale);
        radioFemale = searchSheetView.findViewById(R.id.radioFemale);
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        editName = searchSheetView.findViewById(R.id.editName);

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        gender = "";

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });

        loadStatesList();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            String name = editName.getText().toString();
            searchFilterBuilder.dismiss();
            searchType = 0;

            loadAddressProfilesList(name, gender, state_id, city_id, 1);
        });
    }

    private void initBloodbanksheet(View searchSheetView) {
        radioMale = searchSheetView.findViewById(R.id.radioMale);
        radioFemale = searchSheetView.findViewById(R.id.radioFemale);
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        editMinAge = searchSheetView.findViewById(R.id.editMinAge);
        editMaxAge = searchSheetView.findViewById(R.id.editMaxAge);

        gender = "";
        min_age = "";
        max_age = "";
        blood_group = "";

        ArrayAdapter<String> adapterBloodGroup =
                new ArrayAdapter<String>(
                        SearchProfilesActivity.this,
                        android.R.layout.simple_dropdown_item_1line,
                        getResources().getStringArray(R.array.blood_group_array));

        bloodGroupDropdown =
                searchSheetView.findViewById(R.id.editBloodGroup);
        bloodGroupDropdown.setAdapter(adapterBloodGroup);

        bloodGroupDropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                blood_group = getResources().getStringArray(R.array.blood_group_array)[position];
            }
        });

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });

        loadStatesList();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            min_age = editMinAge.getText().toString();
            max_age = editMaxAge.getText().toString();
            searchFilterBuilder.dismiss();
            searchType = 1;
            loadBloodProfilesList(gender, state_id, city_id, min_age, max_age, blood_group, 1);
        });
    }

    private void initMatrimonialsheet(View searchSheetView) {
        radioMale = searchSheetView.findViewById(R.id.radioMale);
        radioFemale = searchSheetView.findViewById(R.id.radioFemale);
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        editMinAge = searchSheetView.findViewById(R.id.editMinAge);
        editMaxAge = searchSheetView.findViewById(R.id.editMaxAge);

        gender = "";
        min_age = "";
        max_age = "";

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });

        loadStatesList();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            min_age = editMinAge.getText().toString();
            max_age = editMaxAge.getText().toString();
            searchFilterBuilder.dismiss();
            searchType = 2;
            loadMatrimonialProfilesList(gender, state_id, city_id, min_age, max_age, 1);
        });
    }

    private void initBussinessSheet(View searchSheetView) {
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        occupationDropdown = searchSheetView.findViewById(R.id.editOccupationType);
        loadStatesList();
        loadOccupationType();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            searchFilterBuilder.dismiss();
            searchType = 3;
            loadBussinessProfilesList(occupation_type, state_id, city_id, 1);
        });
    }

    private void loadOccupationType() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading bussiness types...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Occupation_Data> call = service.getOccupationType();

        call.enqueue(new Callback<Occupation_Data>() {
            @Override
            public void onResponse(Call<Occupation_Data> call, Response<Occupation_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();


                if (response.body() != null) {
                    if (response.code() == 200) {
                        occupationList = response.body().getData();

                        String[] occupationArray = new String[occupationList.size()];
                        for (int i = 0; i < occupationList.size(); i++) {
                            occupationArray[i] = occupationList.get(i).getOccupation_name();
                        }
                        ArrayAdapter<String> adapterOccupation =
                                new ArrayAdapter<String>(
                                        SearchProfilesActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        occupationArray);
                        occupationDropdown.setAdapter(adapterOccupation);
                        occupationDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            occupation_type = occupationList.get(position).getOccupation_name();
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchProfilesActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchProfilesActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchProfilesActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Occupation_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchProfilesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadStatesList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<States_Data> call = service.getAllStates();

        call.enqueue(new Callback<States_Data>() {
            @Override
            public void onResponse(Call<States_Data> call, Response<States_Data> response) {
                if (response.body() != null) {
                    if (response.code() == 200) {
                        stateDropdown.setEnabled(true);
                        stateList = response.body().getData();
                        String[] statesArray = new String[stateList.size()];
                        for (int i = 0; i < stateList.size(); i++) {
                            statesArray[i] = stateList.get(i).getName();
                        }
                        adapterState =
                                new ArrayAdapter<String>(
                                        SearchProfilesActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        statesArray);

                        stateDropdown.setAdapter(adapterState);
                        stateDropdown.setOnItemClickListener((parent, view, position, id) -> {
                            cityDropdown.setEnabled(true);
                            state_id = String.valueOf(stateList.get(position).getName());
                            Log.e("TAG", "onResponse: " + state_id);
                            loadCitiesList(String.valueOf(stateList.get(position).getId()));
                        });

                    } else if (response.code() == 400) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchProfilesActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchProfilesActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchProfilesActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<States_Data> call, Throwable t) {
                Toast.makeText(SearchProfilesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadCitiesList(String id) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading cities...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<City_Data> call = service.getAllStateCities(id);

        call.enqueue(new Callback<City_Data>() {
            @Override
            public void onResponse(Call<City_Data> call, Response<City_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        cityList = response.body().getData();
                        String[] citiesArray = new String[cityList.size()];
                        for (int i = 0; i < cityList.size(); i++) {
                            citiesArray[i] = cityList.get(i).getCity();
                        }
                        adapterCity =
                                new ArrayAdapter<String>(
                                        SearchProfilesActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        citiesArray);
                        cityDropdown.setAdapter(adapterCity);
                        cityDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            city_id = String.valueOf(cityList.get(position).getId());
                            Log.e("TAG", "onResponse: " + city_id);
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchProfilesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchProfilesActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchProfilesActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchProfilesActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchProfilesActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<City_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchProfilesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}