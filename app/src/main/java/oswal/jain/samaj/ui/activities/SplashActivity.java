package oswal.jain.samaj.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import oswal.jain.samaj.utils.PreferenceUtils;

public class SplashActivity extends AppCompatActivity {

    PreferenceUtils preferenceUtils;
    String device_token;

    private FirebaseCrashlytics mCrashlytics;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceUtils = new PreferenceUtils(this);

        mCrashlytics = FirebaseCrashlytics.getInstance();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        new Handler()
                .postDelayed(() -> {
                    if (preferenceUtils.getLoginStatus()) {
                        if (preferenceUtils.getIsFilled() == 0)
                            startActivity(new Intent(SplashActivity.this, ProfileSetupActivity.class));
                        else startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }
                        finish();
                }, 2000);
    }

}
