package oswal.jain.samaj.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Pagination;
import oswal.jain.samaj.interfaces.OnItemClick;

import java.util.ArrayList;

public class PageListAdapter extends RecyclerView.Adapter<PageListAdapter.VHolder> {

    Context context;
    ArrayList<Pagination> pagesList;
    int selected;
    OnItemClick onItemClick;

    public PageListAdapter(Context context, ArrayList<Pagination> pagesList, OnItemClick onItemClick) {
        this.context = context;
        this.pagesList = pagesList;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_page, parent, false);
        return new VHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder holder, int position) {
        holder.pageTxt.setText(pagesList.get(position).getTitle());
        if (pagesList.get(position).isSelected()) {
            holder.pageTxt.setTextColor(context.getResources().getColor(R.color.white));
            holder.itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.round_bg_accent));
        } else {
            holder.pageTxt.setTextColor(context.getResources().getColor(R.color.textColorPrimary));
            holder.itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.round_bg_transparent));
        }

        holder.pageTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Pagination item : pagesList) {
                    item.setSelected(false);
                }

                pagesList.get(position).setSelected(true);
                onItemClick.onMenuItemClick(position);
                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return pagesList.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {

        TextView pageTxt;

        public VHolder(@NonNull View itemView) {
            super(itemView);

            pageTxt = (TextView) itemView;
        }
    }
}
