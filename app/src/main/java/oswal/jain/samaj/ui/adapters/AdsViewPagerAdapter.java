package oswal.jain.samaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;
import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Ads_Data;
import oswal.jain.samaj.utils.AppUtils;

import java.util.ArrayList;

public class AdsViewPagerAdapter extends PagerAdapter {

    Context context;
    ArrayList<Ads_Data.Ads_Details> ads_list;

    MaterialCardView cardAdItem;
    ShapeableImageView ad_image;

    public AdsViewPagerAdapter(Context context, ArrayList<Ads_Data.Ads_Details> ads_list) {
        this.context = context;
        this.ads_list = ads_list;
    }

    @Override
    public int getCount() {
        return ads_list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_ads_viewpager, container, false);
        init(layout);

        Ads_Data.Ads_Details ads_details = ads_list.get(position);
        ad_image.setShapeAppearanceModel(
                new ShapeAppearanceModel.Builder()
                        .setAllCorners(CornerFamily.ROUNDED, 16)
                        .build());
        try {
            Glide.with(context)
                    .load(ads_details.getAd_image())
                    .placeholder(R.drawable.ic_news)
                    .into(ad_image);
        } catch (Exception e) {
            Log.e("TAG", "onBindViewHolder: " + e.getMessage());
        }
        cardAdItem.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(ads_details.getRedirect_url())) {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(ContextCompat.getColor(context, R.color.colorAccent));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(context, Uri.parse(ads_details.getRedirect_url()));
            } else if (!TextUtils.isEmpty(ads_details.getWhats_app())) {
                String mobileNumber = ads_details.getWhats_app();
                String message = "";
                boolean installed = AppUtils.getAppInstalledOrNot(context, "com.whatsapp");
                if (installed) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        container.addView(layout);
        return layout;
    }

    private void init(ViewGroup layout) {
        cardAdItem = layout.findViewById(R.id.cardAdItem);
        ad_image = layout.findViewById(R.id.ad_image);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        container.removeView((RelativeLayout) view);
    }

}
