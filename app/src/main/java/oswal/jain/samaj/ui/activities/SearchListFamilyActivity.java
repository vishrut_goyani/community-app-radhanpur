package oswal.jain.samaj.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Family_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.interfaces.AddressUpdateListener;
import oswal.jain.samaj.ui.adapters.FamilyMembersListAdapter;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.PreferenceUtils;
import oswal.jain.samaj.widgets.BaseRecyclerView;
import oswal.jain.samaj.widgets.HidingScrollListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static oswal.jain.samaj.ui.activities.SearchProfilesActivity.SEARCH_TYPE;
import static oswal.jain.samaj.ui.activities.SearchProfilesActivity.USER_ID_FAMILY_HEAD;

public class SearchListFamilyActivity extends AppCompatActivity implements AddressUpdateListener {

    int head_id, searchType;
    ArrayList<Family_Data.Data.Family_Details> members_list;

    BaseRecyclerView rv_members_list;
    AddressUpdateListener addressUpdateListener;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list_family);

        init();

        head_id = getIntent().getIntExtra(USER_ID_FAMILY_HEAD, -1);
        searchType = getIntent().getIntExtra(SEARCH_TYPE, -1);
        if (AppConstants.FAMILY_ID != -1) {
            searchType = 0;
            head_id = AppConstants.FAMILY_ID;
        }
        Log.e("TAG", "onCreate: " + head_id);
        loadProfileDetails(head_id);
    }

    private void init() {
        members_list = new ArrayList<>();
        rv_members_list = findViewById(R.id.rv_members_list);
        addressUpdateListener = this;
        toolbar = findViewById(R.id.toolbar);
        rv_members_list.setLayoutManager(new LinearLayoutManager(this));

        rv_members_list.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    private void hideViews() {
        toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    private void loadProfileDetails(int head_id) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading family members...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Family_Data> call = service.getFamilyMembers("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                head_id);
        call.enqueue(new Callback<Family_Data>() {
            @Override
            public void onResponse(Call<Family_Data> call, Response<Family_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Log.e("TAG", "onResponse: " + response);
                            members_list = response.body().getData().getFamily_members();
                            if (members_list.size() == 0){
                                Toast.makeText(SearchListFamilyActivity.this, "No User Found", Toast.LENGTH_SHORT).show();
                            }
                            FamilyMembersListAdapter familyMembersAdapter = new FamilyMembersListAdapter(SearchListFamilyActivity.this, members_list,
                                    response.body().getData(), searchType, addressUpdateListener);
                            rv_members_list.setAdapter(familyMembersAdapter);

                        } catch (Exception e) {
                            Toast.makeText(SearchListFamilyActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(SearchListFamilyActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchListFamilyActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(SearchListFamilyActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(SearchListFamilyActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(SearchListFamilyActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SearchListFamilyActivity.this, "Error occured. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(SearchListFamilyActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<Family_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchListFamilyActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onAddressUpdate() {
        loadProfileDetails(head_id);
    }
}