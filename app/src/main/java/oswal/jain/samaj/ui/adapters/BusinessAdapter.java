package oswal.jain.samaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;

import java.util.ArrayList;
import java.util.Objects;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Business_Data;
import oswal.jain.samaj.ui.activities.ProfileDetailsActivity;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.widgets.BaseModulesAdapter;

public class BusinessAdapter extends BaseModulesAdapter<RecyclerView.ViewHolder> {
    private static final int ITEM_TYPE_HEADER = 0;
    private static final int ITEM_TYPE_BUSINESS = 1;

    Context context;
    int searchType;
    ArrayList<Business_Data.Business_Details.Data> profile_list_business;

    public BusinessAdapter(Context context, ArrayList<Business_Data.Business_Details.Data> profile_list_business, int searchType) {
        this.context = context;
        this.profile_list_business = profile_list_business;
        this.searchType = searchType;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_HEADER:
                View view1 = LayoutInflater.from(context).inflate(R.layout.empty_header_view, parent, false);
                return new ItemHolderHeader(view1);
            case ITEM_TYPE_BUSINESS:
                View view2 = LayoutInflater.from(context).inflate(R.layout.item_business_list, parent, false);
                return new ItemHolderBusiness(view2);
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        if (profile_list_business != null)
            return profile_list_business.size() + 1;
        else return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_TYPE_HEADER;
        }
        return ITEM_TYPE_BUSINESS;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_TYPE_BUSINESS:
                ItemHolderBusiness holder1 = (ItemHolderBusiness) holder;
                Business_Data.Business_Details.Data search_data2 = profile_list_business.get(position - 1);
                Glide.with(context)
                        .load(search_data2.getThumb())
                        .placeholder(R.drawable.profile3)
                        .into(holder1.profile_pic);
                holder1.profile_pic.setShapeAppearanceModel(
                        new ShapeAppearanceModel.Builder()
                                .setAllCorners(CornerFamily.ROUNDED, 8)
                                .build()
                );

                holder1.name.setText(search_data2.getName());
                holder1.email.setText("Email : " + search_data2.getEmailId());
                holder1.dob.setText("Date Of Birth : " + search_data2.getDate_of_birth());
                holder1.state.setText("State : " + search_data2.getState().getName());
                holder1.city.setText("City : " + search_data2.getCity().getCity());
                if (TextUtils.isEmpty(search_data2.getNative_place()))
                    holder1.native_place.setText("Native Place of Rajasthan : " + "-");
                else holder1.native_place.setText(search_data2.getNative_place());

                if (!TextUtils.isEmpty(search_data2.getOccupation_type()) && !TextUtils.isEmpty(search_data2.getDesignation())) {
                    holder1.business.setText("Bussiness : " + search_data2.getDesignation() + " " + search_data2.getOccupation_type());
                } else if (!TextUtils.isEmpty(search_data2.getOccupation_type())) {
                    holder1.business.setText("Bussiness : " + "-" + " " + search_data2.getOccupation_type());
                } else if (!TextUtils.isEmpty(search_data2.getDesignation())) {
                    holder1.business.setText("Bussiness : " + search_data2.getDesignation() + " -");
                } else {
                    holder1.business.setText("Bussiness : " + " - " + " -");
                }


                if (!TextUtils.isEmpty(profile_list_business.get(position - 1).getMobile_no())) {
                    if (Objects.equals(profile_list_business.get(position - 1).getGender(), "male")) {
                        holder1.phone.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setText(profile_list_business.get(position - 1).getMobile_no());
                    } else {
                        holder1.phone.setVisibility(View.GONE);
                        holder1.phoneTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.phone.setVisibility(View.GONE);
                    holder1.phoneTxt.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(profile_list_business.get(position - 1).getWhatsappNo())) {
                    if (Objects.equals(profile_list_business.get(position - 1).getGender(), "male")) {
                        holder1.whatsapp.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setText(profile_list_business.get(position - 1).getWhatsappNo());
                    } else {
                        holder1.whatsapp.setVisibility(View.GONE);
                        holder1.whatsappTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.whatsapp.setVisibility(View.GONE);
                    holder1.whatsappTxt.setVisibility(View.GONE);
                }

                holder1.rl1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri u = Uri.parse("tel:" + search_data2.getMobile_no());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        try {
                            context.startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });
                holder1.rl2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mobileNumber = search_data2.getWhatsappNo();
                        String message = "";
                        boolean installed = AppUtils.getAppInstalledOrNot(context, "com.whatsapp");
                        if (installed) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

//                holder1.more.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        context.startActivity(new Intent(context, ProfileDetailsActivity.class)
//                                .putExtra(ProfileDetailsActivity.USER_ID_NEW, search_data2.getId())
//                                .putExtra(ProfileDetailsActivity.SEARCH_TYPE, 3));
//                    }
//                });
                break;
        }
    }

    public static class ItemHolderHeader extends RecyclerView.ViewHolder {

        public ItemHolderHeader(@NonNull View itemView) {
            super(itemView);
        }
    }

    public static class ItemHolderBusiness extends RecyclerView.ViewHolder {

        AppCompatTextView name, email, dob, business, state, city, native_place;
        ShapeableImageView profile_pic;
        TextView phoneTxt, whatsappTxt;
        RelativeLayout rl1, rl2;
        ImageView phone, whatsapp;
        MaterialCardView cardProfile;
        Button more;

        public ItemHolderBusiness(@NonNull View itemView) {
            super(itemView);

            more = itemView.findViewById(R.id.more);
            cardProfile = itemView.findViewById(R.id.cardProfile);
            profile_pic = itemView.findViewById(R.id.profile_pic);
            name = itemView.findViewById(R.id.name);
            state = itemView.findViewById(R.id.state);
            city = itemView.findViewById(R.id.city);
            native_place = itemView.findViewById(R.id.native_place);
            email = itemView.findViewById(R.id.email);
            dob = itemView.findViewById(R.id.dob);
            phone = itemView.findViewById(R.id.phone);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            rl1 = itemView.findViewById(R.id.rl1);
            rl2 = itemView.findViewById(R.id.rl2);
            phoneTxt = itemView.findViewById(R.id.phoneTxt);
            whatsappTxt = itemView.findViewById(R.id.whatsappTxt);
            business = itemView.findViewById(R.id.bussiness);
        }

    }

}
