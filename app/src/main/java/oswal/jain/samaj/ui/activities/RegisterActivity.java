package oswal.jain.samaj.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.City_Data;
import oswal.jain.samaj.data.models.Register_Data;
import oswal.jain.samaj.data.models.States_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button registerBtn;
    ArrayList<States_Data.State_Details> stateList;
    ArrayList<City_Data.City_Details> cityList;

    ArrayAdapter<String> adapterState, adapterCity;
    TextInputEditText editName, editMobile, editEmail, editPassword, editAddress1, editAddress2, editPincode, editNativePlace, editFathersName;
    int state_id, city_id;
    AutoCompleteTextView stateDropdown, cityDropdown;
    String gender;
    RadioButton radioMale, radioFemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        loadStatesList();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            finish();
        });

        registerBtn.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(editName.getText().toString())
                    && !TextUtils.isEmpty(editMobile.getText().toString())
                    && !TextUtils.isEmpty(gender)
                    && !TextUtils.isEmpty(editPassword.getText().toString())
                    && !TextUtils.isEmpty(editAddress1.getText().toString())
                    && !TextUtils.isEmpty(editFathersName.getText().toString())
                    && !TextUtils.isEmpty(editPincode.getText().toString())) {
                String username = editName.getText().toString();
                String mobile = editMobile.getText().toString();
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();
                String fathersName = editFathersName.getText().toString();
                String pincode = editPincode.getText().toString();
                String address_line1 = editAddress1.getText().toString();
                String address_line2 = editAddress2.getText().toString();
                String nativePlace = editNativePlace.getText().toString();

                if (AppUtils.isNetworkAvailable(this)) {
                    registerAccount(username, fathersName, email, password, mobile, gender, "android", AppUtils.getDeviceToken(this),
                            address_line1, state_id, city_id, address_line2, pincode, nativePlace);
                    Log.e("TAG", "onCreate: " +
                            "\n" + username + "\n" + email + "\n" + password + "\n" + mobile + "\n" + gender + "\n" + "android" + "\n" + "123321" + "\n" + address_line1 + "\n" + address_line2 + "\n"
                            + state_id + "\n" + city_id + "\n" + pincode);
                } else
                    Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "You must fill all the details to register a new account.", Toast.LENGTH_SHORT).show();
        });
    }

    private void init() {
        stateList = new ArrayList<>();
        cityList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        registerBtn = findViewById(R.id.registerBtn);
        editName = findViewById(R.id.editfullName);
        editMobile = findViewById(R.id.editMobile);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
        editFathersName = findViewById(R.id.editFathersName);
        editAddress1 = findViewById(R.id.editAddress1);
        editAddress2 = findViewById(R.id.editAddress2);
        editNativePlace = findViewById(R.id.editNativePlace);
        editPincode = findViewById(R.id.editPincode);
        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);
        stateDropdown = findViewById(R.id.editState);
        cityDropdown = findViewById(R.id.editCity);

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });

        editPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    editNativePlace.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    private void loadStatesList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<States_Data> call = service.getAllStates();

        call.enqueue(new Callback<States_Data>() {
            @Override
            public void onResponse(Call<States_Data> call, Response<States_Data> response) {
                if (response.body() != null) {
                    if (response.code() == 200) {
                        stateDropdown.setEnabled(true);
                        stateList = response.body().getData();
                        String[] statesArray = new String[stateList.size()];
                        for (int i = 0; i < stateList.size(); i++) {
                            statesArray[i] = stateList.get(i).getName();
                        }
                        adapterState =
                                new ArrayAdapter<String>(
                                        RegisterActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        statesArray);

                        stateDropdown.setAdapter(adapterState);
                        stateDropdown.setOnItemClickListener((parent, view, position, id) -> {
                            cityDropdown.setEnabled(true);
                            state_id = stateList.get(position).getId();
                            loadCitiesList(stateList.get(position).getId());
                        });

                    } else if (response.code() == 400) {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(RegisterActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RegisterActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        Toast.makeText(RegisterActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<States_Data> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadCitiesList(int id) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading cities...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<City_Data> call = service.getAllStateCities(String.valueOf(id));

        call.enqueue(new Callback<City_Data>() {
            @Override
            public void onResponse(Call<City_Data> call, Response<City_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        cityList = response.body().getData();
                        String[] citiesArray = new String[cityList.size()];
                        for (int i = 0; i < cityList.size(); i++) {
                            citiesArray[i] = cityList.get(i).getCity();
                        }
                        adapterCity =
                                new ArrayAdapter<String>(
                                        RegisterActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        citiesArray);
                        cityDropdown.setAdapter(adapterCity);
                        cityDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            city_id = cityList.get(position).getId();
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(RegisterActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RegisterActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        Toast.makeText(RegisterActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<City_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void registerAccount(String username, String fathersName, String email, String password, String mobile, String gender, String device_type,
                                 String device_token, String address_line1, int state_id, int city_id, String address_line2, String pincode, String nativePlace) {
        Log.e("TAG", "registerAccount: " + "\n"
                + username + "\n" + email + "\n" + password + "\n" + mobile + "\n" + gender);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Creating account...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Register_Data> call = service.registerNewUser(username, fathersName, email, password, mobile, gender, device_type, device_token,
                address_line1, state_id, city_id, pincode, address_line2, nativePlace);

        call.enqueue(new Callback<Register_Data>() {
            @Override
            public void onResponse(Call<Register_Data> call, Response<Register_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("TAG", "onResponse: " + response);
                            finish();
                        } catch (Exception e) {
                            Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(RegisterActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RegisterActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        Toast.makeText(RegisterActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Register_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}