package oswal.jain.samaj.ui.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.Toolbar;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import oswal.jain.samaj.CommunityApp;
import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Ads_Data;
import oswal.jain.samaj.data.models.City_Data;
import oswal.jain.samaj.data.models.Feedback_Data;
import oswal.jain.samaj.data.models.Occupation_Data;
import oswal.jain.samaj.data.models.States_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.ui.adapters.AdsViewPagerAdapter;
import oswal.jain.samaj.ui.fragments.SearchFilterBuilder;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    public static final String nightModeEnabled = "isNightModeEnabled";
    public static final String SEARCH_TYPE = "search_type", STATE_ID = "state_id", CITY_ID = "city_id", USER_NAME = "user_name",
            USER_GENDER = "user_gender", USER_OCCUPATION = "user_occupation", MIN_AGE = "min_age", MAX_AGE = "max_age", BLOOD_GROUP = "blood_group";
    final long DELAY_MS = 3000;
    final long PERIOD_MS = 3000;
    final Handler handler = new Handler();
    MaterialCardView card1, card2, card3, card4, card5, card6;
    TextView nav_profile_name, nameTxt;
    CircleImageView nav_profile_pic_img, home_profile_img;
    String name;
    DrawerLayout drawer_layout;
    NavigationView navigationView;
    SearchFilterBuilder searchFilterBuilder;
    RadioButton radioMale, radioFemale;
    AutoCompleteTextView stateDropdown, cityDropdown, bloodGroupDropdown, occupationDropdown;
    TextInputEditText editName, editMinAge, editMaxAge;
    ViewPager viewPagerAds;
    AdsViewPagerAdapter adsViewPagerAdapter;
    ArrayList<States_Data.State_Details> stateList;
    ArrayList<City_Data.City_Details> cityList;
    ArrayList<Occupation_Data.Occupation_Details> occupationList;
    ArrayAdapter<String> adapterState, adapterCity;
    String state_id, city_id;
    String min_age, max_age;
    String ratingVal, device_name, gender, blood_group;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean doubleBackToExitPressedOnce = false;
    String occupation_type;
    PreferenceUtils preferenceUtils;
    int currentPage;
    final Runnable swipeRunnable = new Runnable() {
        public void run() {
            if (viewPagerAds.getAdapter() != null) {
                currentPage = viewPagerAds.getCurrentItem();
                if (currentPage == viewPagerAds.getAdapter().getCount() - 1) {
                    currentPage = -1;
                }
                currentPage++;
                viewPagerAds.setCurrentItem(currentPage, true);
            }
        }
    };
    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        preferences = getSharedPreferences(CommunityApp.APP_PREFS, MODE_PRIVATE);
        preferenceUtils = new PreferenceUtils(this);
        init();
        new LoadModules().execute("");
        try {
            device_name = Build.MANUFACTURER + " " + Build.MODEL;
            Log.e("TAG", "onCreate: " + device_name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        stateList = new ArrayList<>();
        cityList = new ArrayList<>();
        occupationList = new ArrayList<>();
        nameTxt = findViewById(R.id.nameTxt);
        card1 = findViewById(R.id.cardAddressBook);
        card2 = findViewById(R.id.cardBloodBank);
        card3 = findViewById(R.id.cardMatrimonial);
        card4 = findViewById(R.id.cardNews);
        card5 = findViewById(R.id.cardFeedback);
        card6 = findViewById(R.id.cardBussiness);
        viewPagerAds = findViewById(R.id.viewPagerAds);
        home_profile_img = findViewById(R.id.home_profile_img);
        drawer_layout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigationView);
        CheckBox chk_night_mode = findViewById(R.id.chk_night_mode);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawer_layout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        setDetailsInHeader(headerView);
        headerView.setOnClickListener(v -> {
            startActivity(new Intent(this, UserProfileActivity.class));
        });
        try {
            Glide.with(this)
                    .load(preferenceUtils.getUserDetails().getThumb())
                    .placeholder(R.drawable.profile3)
                    .into(home_profile_img);
        } catch (Exception e) {
            Log.e("TAG", "init: " + e.getMessage());
        }
        nameTxt.setText(PreferenceUtils.getInstance(this).getUserDetails().getName());

        if (!preferences.getBoolean(nightModeEnabled, false)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            editor = preferences.edit();
            editor.putBoolean(nightModeEnabled, false).apply();
            chk_night_mode.setChecked(false);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            editor = preferences.edit();
            editor.putBoolean(nightModeEnabled, true).apply();
            chk_night_mode.setChecked(true);
        }

        chk_night_mode.setOnCheckedChangeListener(null);
        chk_night_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getBoolean(nightModeEnabled, false)) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    editor = preferences.edit();
                    editor.putBoolean(nightModeEnabled, false).apply();
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    editor = preferences.edit();
                    editor.putBoolean(nightModeEnabled, true).apply();
                }
            }
        });

        card1.setOnClickListener(this);
        card2.setOnClickListener(this);
        card3.setOnClickListener(this);
        card4.setOnClickListener(this);
        card5.setOnClickListener(this);
        card6.setOnClickListener(this);
    }

    private void setDetailsInHeader(View headerView) {
        nav_profile_pic_img = headerView.findViewById(R.id.nav_profile_pic_img);
        nav_profile_name = headerView.findViewById(R.id.nav_profile_name);

        Log.e("TAG", "setDetailsInHeader: " + preferenceUtils.getUserDetails().getName());
        nav_profile_name.setText(preferenceUtils.getUserDetails().getName());
        try {
            Glide.with(this)
                    .load(preferenceUtils.getUserDetails().getThumb())
                    .placeholder(R.drawable.profile3)
                    .into(nav_profile_pic_img);
        } catch (Exception e) {
            Log.e("TAG", "setDetailsInHeader: " + e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        name = null;
        gender = null;
        blood_group = null;
        min_age = null;
        min_age = null;
        state_id = null;
        city_id = null;

        switch (view.getId()) {
            case R.id.cardAddressBook:
                searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                        .title("Search")
                        .setupLayout(HomeActivity.this, R.layout.search_filter_sheet_address_book);
                searchFilterBuilder.show();
                View searchSheetView1 = searchFilterBuilder.getLayout();
                initAddressBooksheet(searchSheetView1);
                break;
            case R.id.cardBloodBank:
                searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                        .title("Search")
                        .setupLayout(HomeActivity.this, R.layout.search_filter_sheet_blood_bank);
                searchFilterBuilder.show();
                View searchSheetView2 = searchFilterBuilder.getLayout();
                initBloodbanksheet(searchSheetView2);
                break;
            case R.id.cardMatrimonial:
                searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                        .title("Search")
                        .setupLayout(HomeActivity.this, R.layout.search_filter_sheet_matrimonial);
                searchFilterBuilder.show();
                View searchSheetView3 = searchFilterBuilder.getLayout();
                initMatrimonialsheet(searchSheetView3);
                break;
            case R.id.cardNews:
                startActivity(new Intent(HomeActivity.this, NewsActivity.class));
                break;
            case R.id.cardFeedback:
                searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                        .title("Search")
                        .setupLayout(HomeActivity.this, R.layout.bottom_sheet_feedback);
                searchFilterBuilder.show();
                View searchSheetView4 = searchFilterBuilder.getLayout();
                initFeedbacksheet(searchSheetView4);
                break;
            case R.id.cardBussiness:
                searchFilterBuilder = SearchFilterBuilder.with(getSupportFragmentManager())
                        .title("Search")
                        .setupLayout(HomeActivity.this, R.layout.search_filter_sheet_bussiness);
                searchFilterBuilder.show();
                View searchSheetView5 = searchFilterBuilder.getLayout();
                initBussinessheet(searchSheetView5);
                break;
        }
    }

    private void initAddressBooksheet(View searchSheetView) {
        radioMale = searchSheetView.findViewById(R.id.radioMale);
        radioFemale = searchSheetView.findViewById(R.id.radioFemale);
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        editName = searchSheetView.findViewById(R.id.editName);

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });

        loadStatesList();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            name = editName.getText().toString();
            searchFilterBuilder.dismiss();
            startActivity(new Intent(HomeActivity.this, SearchProfilesActivity.class)
                    .putExtra(SEARCH_TYPE, 0)
                    .putExtra(USER_NAME, name)
                    .putExtra(USER_GENDER, gender)
                    .putExtra(STATE_ID, state_id)
                    .putExtra(CITY_ID, city_id));
        });

    }

    private void initBloodbanksheet(View searchSheetView) {
        radioMale = searchSheetView.findViewById(R.id.radioMale);
        radioFemale = searchSheetView.findViewById(R.id.radioFemale);
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        editMinAge = searchSheetView.findViewById(R.id.editMinAge);
        editMaxAge = searchSheetView.findViewById(R.id.editMaxAge);

        ArrayAdapter<String> adapterBloodGroup =
                new ArrayAdapter<String>(
                        HomeActivity.this,
                        android.R.layout.simple_dropdown_item_1line,
                        getResources().getStringArray(R.array.blood_group_array));

        bloodGroupDropdown =
                searchSheetView.findViewById(R.id.editBloodGroup);
        bloodGroupDropdown.setAdapter(adapterBloodGroup);

        bloodGroupDropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                blood_group = getResources().getStringArray(R.array.blood_group_array)[position];
            }
        });

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });

        loadStatesList();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            min_age = editMinAge.getText().toString();
            max_age = editMaxAge.getText().toString();
            searchFilterBuilder.dismiss();
            startActivity(new Intent(HomeActivity.this, SearchProfilesActivity.class)
                    .putExtra(SEARCH_TYPE, 1)
                    .putExtra(USER_GENDER, gender)
                    .putExtra(STATE_ID, state_id)
                    .putExtra(CITY_ID, city_id)
                    .putExtra(MIN_AGE, min_age)
                    .putExtra(MAX_AGE, max_age)
                    .putExtra(BLOOD_GROUP, blood_group));
        });
    }

    private void initMatrimonialsheet(View searchSheetView) {
        radioMale = searchSheetView.findViewById(R.id.radioMale);
        radioFemale = searchSheetView.findViewById(R.id.radioFemale);
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        editMinAge = searchSheetView.findViewById(R.id.editMinAge);
        editMaxAge = searchSheetView.findViewById(R.id.editMaxAge);

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });

        loadStatesList();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            min_age = editMinAge.getText().toString();
            max_age = editMaxAge.getText().toString();
            searchFilterBuilder.dismiss();
            startActivity(new Intent(HomeActivity.this, SearchProfilesActivity.class)
                    .putExtra(SEARCH_TYPE, 2)
                    .putExtra(USER_GENDER, gender)
                    .putExtra(STATE_ID, state_id)
                    .putExtra(CITY_ID, city_id)
                    .putExtra(MIN_AGE, min_age)
                    .putExtra(MAX_AGE, max_age));
        });
    }

    private void initFeedbacksheet(View searchSheetView) {
        TextInputEditText editFeedback = searchSheetView.findViewById(R.id.editFeedback);
        Button searchBtn = searchSheetView.findViewById(R.id.feedbackBtn);
        AppCompatRatingBar ratingBar = searchSheetView.findViewById(R.id.ratingBar);
        ratingVal = "0";
        ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, fromUser) -> {
            ratingVal = String.valueOf(rating);
        });
        searchBtn.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(editFeedback.getText().toString())) {
                if (AppUtils.isNetworkAvailable(this)) {
                    searchFilterBuilder.dismiss();
                    String feedback = editFeedback.getText().toString();
                    submitFeedback(
                            PreferenceUtils.getInstance(HomeActivity.this).getUserDetails().getFamily_id(),
                            PreferenceUtils.getInstance(HomeActivity.this).getUserDetails().getName(),
                            PreferenceUtils.getInstance(HomeActivity.this).getUserDetails().getMobile_no(),
                            ratingVal,
                            feedback,
                            device_name);
                } else {
                    Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                editFeedback.setError("Feedback required");
                editFeedback.requestFocus();
            }
        });
    }

    private void initBussinessheet(View searchSheetView) {
        stateDropdown = searchSheetView.findViewById(R.id.editState);
        cityDropdown = searchSheetView.findViewById(R.id.editCity);
        occupationDropdown = searchSheetView.findViewById(R.id.editOccupationType);
        loadStatesList();
        loadOccupationType();

        Button searchBtn = searchSheetView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(v -> {
            searchFilterBuilder.dismiss();
            startActivity(new Intent(HomeActivity.this, SearchProfilesActivity.class)
                    .putExtra(SEARCH_TYPE, 3)
                    .putExtra(USER_OCCUPATION, occupation_type)
                    .putExtra(STATE_ID, state_id)
                    .putExtra(CITY_ID, city_id));
        });
    }

    private void loadStatesList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<States_Data> call = service.getAllStates();

        call.enqueue(new Callback<States_Data>() {
            @Override
            public void onResponse(Call<States_Data> call, Response<States_Data> response) {
                if (response.body() != null) {
                    if (response.code() == 200) {
                        stateDropdown.setEnabled(true);
                        stateList = response.body().getData();
                        String[] statesArray = new String[stateList.size()];
                        for (int i = 0; i < stateList.size(); i++) {
                            statesArray[i] = stateList.get(i).getName();
                        }
                        adapterState =
                                new ArrayAdapter<String>(
                                        HomeActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        statesArray);

                        stateDropdown.setAdapter(adapterState);
                        stateDropdown.setOnItemClickListener((parent, view, position, id) -> {
                            cityDropdown.setEnabled(true);
                            state_id = String.valueOf(stateList.get(position).getId());
                            Log.e("TAG", "onResponse: " + state_id);
                            loadCitiesList(String.valueOf(stateList.get(position).getId()));
                        });

                    } else if (response.code() == 400) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 501) {
                        Toast.makeText(HomeActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(HomeActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<States_Data> call, Throwable t) {
                Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadOccupationType() {
//        ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setCancelable(false);
//        progressDialog.setTitle("Loading bussiness types...");
//        progressDialog.setMessage("please wait...");
//        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Occupation_Data> call = service.getOccupationType();

        call.enqueue(new Callback<Occupation_Data>() {
            @Override
            public void onResponse(Call<Occupation_Data> call, Response<Occupation_Data> response) {
//                if (progressDialo/g.dismiss();


                if (response.body() != null) {
                    if (response.code() == 200) {
                        occupationList = response.body().getData();

                        String[] occupationArray = new String[occupationList.size()];
                        for (int i = 0; i < occupationList.size(); i++) {
                            occupationArray[i] = occupationList.get(i).getOccupation_name();
                        }
                        ArrayAdapter<String> adapterOccupation =
                                new ArrayAdapter<String>(
                                        HomeActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        occupationArray);
                        occupationDropdown.setAdapter(adapterOccupation);
                        occupationDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            occupation_type = occupationList.get(position).getOccupation_name();
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(HomeActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(HomeActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(HomeActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Occupation_Data> call, Throwable t) {
//                if (progressDialog.isShowing())
//                    progressDialog.dismiss();
                Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadCitiesList(String id) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading cities...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<City_Data> call = service.getAllStateCities(id);

        call.enqueue(new Callback<City_Data>() {
            @Override
            public void onResponse(Call<City_Data> call, Response<City_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        cityList = response.body().getData();
                        String[] citiesArray = new String[cityList.size()];
                        for (int i = 0; i < cityList.size(); i++) {
                            citiesArray[i] = cityList.get(i).getCity();
                        }
                        adapterCity =
                                new ArrayAdapter<String>(
                                        HomeActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        citiesArray);
                        cityDropdown.setAdapter(adapterCity);
                        cityDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            city_id = String.valueOf(cityList.get(position).getId());
                            Log.e("TAG", "onResponse: " + city_id);
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(HomeActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(HomeActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(HomeActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<City_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitFeedback(String family_id, String name, String mobile_no, String ratings, String feedback, String device_name) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Submitting feedback...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Feedback_Data> call = service.submitFeedback(
                "Bearer " + PreferenceUtils.getInstance(this).getAuthToken()
                , family_id, name, mobile_no, ratings, feedback, device_name);

        call.enqueue(new Callback<Feedback_Data>() {
            @Override
            public void onResponse(Call<Feedback_Data> call, Response<Feedback_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        try {
                            Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 400) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(HomeActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(HomeActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(HomeActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Feedback_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawer_layout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.nav_edit_profile:
                startActivity(new Intent(this, UserProfileActivity.class));
                return true;
            case R.id.nav_change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                finish();
                return true;
            case R.id.nav_add_family_member:
                startActivity(new Intent(this, AddFamilyMemberActivity.class));
                return true;
            case R.id.nav_logout:
                PreferenceUtils.getInstance(this).setLoginStatus(false);
                Toast.makeText(this, "Logged out", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            case R.id.nav_family_details:
                AppConstants.FAMILY_ID = Integer.parseInt(PreferenceUtils.getInstance(this).getUserDetails().getFamily_id());
                startActivity(new Intent(this, SearchListFamilyActivity.class));
                return true;
            case R.id.nav_about_us:
                opendialog();
                break;
            case R.id.nav_contact_us:
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorAccent));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(this, Uri.parse(AppConstants.CONTACT_US));
                break;
        }
        return false;
    }

    private void opendialog() {
        final Dialog d = new Dialog(this, R.style.mydialog);
        View dialogLayout = getLayoutInflater().inflate(R.layout.dialog_edit_profile, null);
        Window window = d.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        d.setContentView(dialogLayout);
        TextView desc = dialogLayout.findViewById(R.id.desc);
        TextView title = dialogLayout.findViewById(R.id.title);
        title.setText("About Us");
        desc.setGravity(View.TEXT_ALIGNMENT_CENTER);
        desc.setText("This App is made for only Jain Mandariya Sajnan samaj. Which community is belong from Mewad land of Rajasthan But relocate in different cities for Business Purpose many years before. Now this community is very much successful and established Businesses in Different cities. This app will help them to be connect with each other easily.");
        dialogLayout.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        d.setCancelable(false);
        d.show();
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START))
            drawer_layout.closeDrawer(GravityCompat.START);
        else {
            if (doubleBackToExitPressedOnce) {
                finishAffinity();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(swipeRunnable);
        super.onDestroy();
    }

    interface AdsAdapterListener {
        void setAdapter(AdsViewPagerAdapter adapter);
    }

    private class LoadModules extends AsyncTask<String, Void, String> implements AdsAdapterListener {
        ArrayList<Ads_Data.Ads_Details> ads_list = new ArrayList<>();
        AdsAdapterListener adsAdapterListener = this;

        @Override
        protected String doInBackground(String... params) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIService service = retrofit.create(APIService.class);
            Call<Ads_Data> call = service.getAdsData("application/json", "Bearer " + PreferenceUtils.getInstance(HomeActivity.this).getAuthToken());
            call.enqueue(new Callback<Ads_Data>() {
                @Override
                public void onResponse(Call<Ads_Data> call, Response<Ads_Data> response) {
                    if (response.body() != null) {
                        if (response.code() == 200)
                            try {
                                ads_list = response.body().getAds_details();
                                adsViewPagerAdapter = new AdsViewPagerAdapter(HomeActivity.this, ads_list);
                                adsAdapterListener.setAdapter(adsViewPagerAdapter);
                                Log.e("TAG", "onResponse: " + ads_list.size());
                            } catch (Exception e) {
                                Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        else if (response.code() == 400) {
                            Toast.makeText(HomeActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                            PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                            finish();
                        } else if (response.code() == 401) {
                            Toast.makeText(HomeActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                            PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                            finish();
                        } else if (response.code() == 500) {
                            Toast.makeText(HomeActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(HomeActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        try {
                            Toast.makeText(HomeActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Ads_Data> call, Throwable t) {
                    Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(swipeRunnable);
                }
            }, DELAY_MS, PERIOD_MS);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        public void setAdapter(AdsViewPagerAdapter adapter) {
            viewPagerAds.setAdapter(adapter);
        }
    }
}