package oswal.jain.samaj.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Profile_Data;
import oswal.jain.samaj.fragment.BasicDetailsFragment;
import oswal.jain.samaj.fragment.FamilyDetailsFragment;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileDetailsActivity extends AppCompatActivity {

    Toolbar toolbar;
    RelativeLayout rl_phone, rl_whatsapp;
    CircleImageView profile_image;
    int searchType;
    TextView name, address, age, cityState;
//    TabLayout tabLayout;
//    ViewPager viewPager;

    public static final String DATE_OF_BIRTH = "date_of_birth",
            MOBILE = "mobile",
            WHATSAPP = "whatsapp",
            EMAIL = "email",
            RELATION_WITH_VADIL = "relation_with_vadil",
            EDUCATION = "education",
            BLOOD_GROUP = "blood_group",
            MARITAL_STATUS = "marital_status",
            PROFILE_DATA = "profile_data";

    public static final String USER_ID_NEW = "user_id_new",
            HEAD_NAME = "head_name",
            CITY = "city",
            STATE = "state",
            SEARCH_TYPE = "search_type",
            FATHER_NAME = "father_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);

        init();
        searchType = getIntent().getIntExtra(SEARCH_TYPE, -1);
        loadProfileDetails(getIntent().getIntExtra(USER_ID_NEW, -1));
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
//        tabLayout = findViewById(R.sid.tabLayout);
//        viewPager = findViewById(R.id.viewPager);
        rl_phone = findViewById(R.id.rl_phone);
        rl_whatsapp = findViewById(R.id.rl_whatsapp);
        profile_image = findViewById(R.id.profile_image);
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        address = findViewById(R.id.address);
        cityState = findViewById(R.id.cityState);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());

//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
    }

    private void loadProfileDetails(int user_id) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading profiles...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Profile_Data> call = service.getProfileDetails("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                user_id);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(this).getAuthToken() + "\n" + user_id);

        call.enqueue(new Callback<Profile_Data>() {
            @Override
            public void onResponse(Call<Profile_Data> call, Response<Profile_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Log.e("TAG", "onResponse: " + response);
                            Profile_Data.Data profile_data = response.body().getData();
                            Glide.with(ProfileDetailsActivity.this)
                                    .load(profile_data.getOriginal())
                                    .into(profile_image);
                            name.setText(profile_data.getName());
                            age.setText(profile_data.getAge() + " years");
                            address.setText(profile_data.getFamily_detail().getAddress_line1());
                            cityState.setText(profile_data.getCity().getCity() + ", " + profile_data.getState().getName());

                            if (Objects.equals(profile_data.getGender(), "female")) {
                                rl_phone.setVisibility(View.GONE);
                                rl_whatsapp.setVisibility(View.GONE);
                            } else {
                                rl_phone.setVisibility(View.VISIBLE);
                                rl_whatsapp.setVisibility(View.VISIBLE);
                            }

                            rl_phone.setOnClickListener(v -> {
                                Uri u = Uri.parse("tel:" + profile_data.getMobile_no());
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                try {
                                    startActivity(i);
                                } catch (Exception e) {
                                    Toast.makeText(ProfileDetailsActivity.this, e.getMessage(), Toast.LENGTH_LONG)
                                            .show();
                                }
                            });

                            rl_whatsapp.setOnClickListener(v -> {

                                String mobileNumber = profile_data.getWhatsappNo();
                                String message = "";
                                boolean installed = AppUtils.getAppInstalledOrNot(ProfileDetailsActivity.this, "com.whatsapp");
                                if (installed) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(ProfileDetailsActivity.this, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                                }
                            });

                            loadFamiltyHeadDetails(response.body().getData().getFamily_detail().getFamily_head_id());

                        } catch (Exception e) {
                            Toast.makeText(ProfileDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(ProfileDetailsActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(ProfileDetailsActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(ProfileDetailsActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(ProfileDetailsActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(ProfileDetailsActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProfileDetailsActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        Toast.makeText(ProfileDetailsActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(ProfileDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loadFamiltyHeadDetails(int family_head_id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Profile_Data> call = service.getProfileDetails("application/json", "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                family_head_id);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(this).getAuthToken() + "\n" + family_head_id);

        call.enqueue(new Callback<Profile_Data>() {
            @Override
            public void onResponse(Call<Profile_Data> call, Response<Profile_Data> response) {
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Log.e("TAG", "onResponse: " + response);
                            Profile_Data.Data profile_data = response.body().getData();
                            ArrayList<Fragment> fragments = new ArrayList<>();
                            fragments.add(FamilyDetailsFragment.newInstance(
                                    profile_data.getName(),
                                    profile_data.getFatherName(),
                                    profile_data.getMobile_no(),
                                    profile_data.getWhatsappNo(),
                                    profile_data.getCity().getCity(),
                                    profile_data.getState().getName(),
                                    profile_data.getFamily_detail().getFamily_head_id()
                            ));
                            Log.e("TAG", "onResponse: " + profile_data.getFamily_detail().getId());
                            fragments.add(
                                    BasicDetailsFragment.newInstance(
                                            new Gson().toJson(profile_data),
                                            searchType)
                            );

                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.about_container, BasicDetailsFragment.newInstance(new Gson().toJson(profile_data), searchType))
                                    .commit();

                        } catch (Exception e) {
                            Toast.makeText(ProfileDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(ProfileDetailsActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(ProfileDetailsActivity.this, LoginActivity.class));
                        finish();
                    }else if (response.code() == 401) {
                        Toast.makeText(ProfileDetailsActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(ProfileDetailsActivity.this, LoginActivity.class));
                        finish();
                    }else if (response.code() == 500) {
                        Toast.makeText(ProfileDetailsActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProfileDetailsActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        Toast.makeText(ProfileDetailsActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile_Data> call, Throwable t) {
                Toast.makeText(ProfileDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}