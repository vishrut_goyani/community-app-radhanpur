package oswal.jain.samaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;

import org.json.JSONException;
import org.json.JSONObject;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Profile_Data;
import oswal.jain.samaj.data.models.SearchAddressData;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.ui.activities.LoginActivity;
import oswal.jain.samaj.ui.activities.SearchListFamilyActivity;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;
import oswal.jain.samaj.widgets.BaseModulesAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import oswal.jain.samaj.ui.activities.SearchProfilesActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfilesListAdapter extends BaseModulesAdapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_ADDRESS = 0;
    private static final int ITEM_TYPE_HEADER = 1;
    Context context;
    ArrayList<SearchAddressData.Search_Data.SearchList> profile_list;
    int searchType;

    public ProfilesListAdapter(Context context, ArrayList<SearchAddressData.Search_Data.SearchList> profile_list, int searchType) {
        this.context = context;
        this.profile_list = profile_list;
        this.searchType = searchType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_ADDRESS:
                View view1 = LayoutInflater.from(context).inflate(R.layout.item_profiles_list, parent, false);
                return new ItemHolder(view1);
            case ITEM_TYPE_HEADER:
                View view3 = LayoutInflater.from(context).inflate(R.layout.empty_header_view, parent, false);
                return new ItemHolderHeader(view3);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_TYPE_ADDRESS:
                ItemHolder holder1 = (ItemHolder) holder;
                SearchAddressData.Search_Data.SearchList search_data = profile_list.get(position - 1);
                Glide.with(context)
                        .load(search_data.getFamily_head_details().getThumb())
                        .placeholder(R.drawable.profile3)
                        .into(holder1.profile_pic);
                holder1.profile_pic.setShapeAppearanceModel(
                        new ShapeAppearanceModel.Builder()
                                .setAllCorners(CornerFamily.ROUNDED, 8)
                                .build()
                );

                holder1.name.setText(search_data.getFamily_head_details().getName());
                holder1.gotra.setText("Gotra : " + search_data.getFamily_head_details().getHobbies());
                holder1.city.setText("City : " + search_data.getCity().getCity());
                if (search_data.getFamily_head_details().getNative_place() == null) {
                    holder1.native_place.setText("Native Place of Rajasthan : " + "-");
                } else {
                    holder1.native_place.setText("Native Place of Rajasthan : " + search_data.getFamily_head_details().getNative_place());
                }
                holder1.occupation.setText("Bussiness Type : " + search_data.getFamily_head_details().getOccupation_type());
                holder1.designation.setText("Bussiness Details : " + search_data.getFamily_head_details().getDesignation());
                holder1.dob.setText("Date of Birth : " + search_data.getFamily_head_details().getDate_of_birth());
                holder1.age.setText("Age : " + search_data.getFamily_head_details().getAge() + " years");
//                holder1.relation_with_vadil.setText(search_data.getFamily_head_details().getRelation_with_vadil());
//                holder1.marital_status.setText(search_data.getFamily_head_details().getMarital_status());

                loadFamiltyHeadDetails(search_data.getFamily_head_id(), holder1.relation_with_vadil);

                if (!TextUtils.isEmpty(profile_list.get(position - 1).getFamily_head_details().getMobile_no())) {
                    if (Objects.equals(profile_list.get(position - 1).getFamily_head_details().getGender(), "male")) {
                        holder1.phone.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setVisibility(View.VISIBLE);
                        holder1.phoneTxt.setText(profile_list.get(position - 1).getFamily_head_details().getMobile_no());
                    } else {
                        holder1.phone.setVisibility(View.GONE);
                        holder1.phoneTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.phone.setVisibility(View.GONE);
                    holder1.phoneTxt.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(profile_list.get(position - 1).getFamily_head_details().getWhatsappNo())) {
                    if (Objects.equals(profile_list.get(position - 1).getFamily_head_details().getGender(), "male")) {
                        holder1.whatsapp.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setVisibility(View.VISIBLE);
                        holder1.whatsappTxt.setText(profile_list.get(position - 1).getFamily_head_details().getWhatsappNo());
                    } else {
                        holder1.whatsapp.setVisibility(View.GONE);
                        holder1.whatsappTxt.setVisibility(View.GONE);
                    }
                } else {
                    holder1.whatsapp.setVisibility(View.GONE);
                    holder1.whatsappTxt.setVisibility(View.GONE);
                }

                holder1.rl1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri u = Uri.parse("tel:" + search_data.getFamily_head_details().getMobile_no());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        try {
                            context.startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });
                holder1.rl2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mobileNumber = search_data.getFamily_head_details().getWhatsappNo();
                        String message = "";
                        boolean installed = AppUtils.getAppInstalledOrNot(context, "com.whatsapp");
                        if (installed) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + "+91" + mobileNumber + "&text=" + message));
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, "Whatsapp is not installed on your device.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                holder1.more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, SearchListFamilyActivity.class)
                                .putExtra(SearchProfilesActivity.USER_ID_FAMILY_HEAD,
                                        search_data.getFamily_head_details().getFamily_id())
                                .putExtra(SearchProfilesActivity.SEARCH_TYPE, searchType));
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return profile_list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return ITEM_TYPE_HEADER;
        else {
            return ITEM_TYPE_ADDRESS;
        }
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        AppCompatTextView name, dob, age, relation_with_vadil, /*marital_status,*/ gotra, city, native_place, occupation, designation;
        TextView phoneTxt, whatsappTxt;
        RelativeLayout rl1, rl2;
        ShapeableImageView profile_pic;
        ImageView phone, whatsapp;
        Button more;
        MaterialCardView cardProfile;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            more = itemView.findViewById(R.id.more);
            cardProfile = itemView.findViewById(R.id.cardProfile);
            profile_pic = itemView.findViewById(R.id.profile_pic);
            name = itemView.findViewById(R.id.name);
            gotra = itemView.findViewById(R.id.gotra);
            city = itemView.findViewById(R.id.city);
            native_place = itemView.findViewById(R.id.native_place);
            occupation = itemView.findViewById(R.id.occupation);
            designation = itemView.findViewById(R.id.designation);
            dob = itemView.findViewById(R.id.dob);
            age = itemView.findViewById(R.id.age);
            relation_with_vadil = itemView.findViewById(R.id.relation_with_vadil);
//            marital_status = itemView.findViewById(R.id.marital_status);
            phone = itemView.findViewById(R.id.phone);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            rl1 = itemView.findViewById(R.id.rl1);
            rl2 = itemView.findViewById(R.id.rl2);
            phoneTxt = itemView.findViewById(R.id.phoneTxt);
            whatsappTxt = itemView.findViewById(R.id.whatsappTxt);
        }

    }

    public static class ItemHolderHeader extends RecyclerView.ViewHolder {

        public ItemHolderHeader(@NonNull View itemView) {
            super(itemView);
        }
    }

    private void loadFamiltyHeadDetails(int family_head_id, TextView txt_head_name) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Profile_Data> call = service.getProfileDetails("application/json", "Bearer " + PreferenceUtils.getInstance(context).getAuthToken(),
                family_head_id);

        Log.e("TAG", "loadProfilesList: " + PreferenceUtils.getInstance(context).getAuthToken() + "\n" + family_head_id);

        call.enqueue(new Callback<Profile_Data>() {
            @Override
            public void onResponse(Call<Profile_Data> call, Response<Profile_Data> response) {
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Log.e("TAG", "onResponse: " + response);
                            Profile_Data.Data profile_data = response.body().getData();
                            txt_head_name.setText("Father's Name : " + profile_data.getName());

                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(context, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(context).setLoginStatus(false);
                        context.startActivity(new Intent(context, LoginActivity.class));
                        ((SearchListFamilyActivity) context).finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(context, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile_Data> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
