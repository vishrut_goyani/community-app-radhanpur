package oswal.jain.samaj.ui.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Occupation_Data;
import oswal.jain.samaj.data.models.Profile_Setup;
import oswal.jain.samaj.data.models.Relation_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.PreferenceUtils;
import oswal.jain.samaj.widgets.CropImageViewOptions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserProfileActivity extends AppCompatActivity implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener {

    public static int GALLERY_REQUEST_CODE = 100;
    TextInputEditText editName, editMobile, editEmail, editAge, editDOB, editWhatsappNo, editEducation, editDesignation, editGotra,
            editHeight, editWeight, editMama, editMotherMama, editFatherMama, editFathersName;
    AutoCompleteTextView bloodGroupDropdown, maritalStatusDropdown, relationWithVadilDropdown, occupationDropdown;
    RadioButton radioMale, radioFemale;
    RadioButton radioDonorTrue, radioDonorFalse;
    RadioButton radioDisplayTrue, radioDisplayFalse;
    LinearLayout rlDisplayInMatrimonialTxt;
    Toolbar toolbar;
    Button submitBtn;
    AppCompatImageView edit_profile_pic;
    CircleImageView profile_pic;
    Calendar calendar;
    int day, month, year;
    ArrayList<Relation_Data.Relation_Details> relationList;
    ArrayList<Occupation_Data.Occupation_Details> occupationList;
    String name, age, date_of_birth, mobile_no, whatsappNo, blood_group, donor = "yes", education, occupation_type, designation,
            display_matrimonial = "no", profile_image, marital_status, relation_with_vadil, gender, gotra, height, weight, mama_name, mother_mama, father_mama;
    Uri selectedImage, CropUri, mCropImageUri;
    File file;
    InputStream is;
    RequestBody requestBody;
    MultipartBody.Part part;
    private CropImageView mCropImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        init();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

    }

    private void init() {
        relationList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        submitBtn = findViewById(R.id.submitBtn);
        editName = findViewById(R.id.editfullName);
        editMobile = findViewById(R.id.editMobile);
        editEmail = findViewById(R.id.editEmail);
        editAge = findViewById(R.id.editAge);
        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);
        rlDisplayInMatrimonialTxt = findViewById(R.id.rlDisplayInMatrimonialTxt);
        editDOB = findViewById(R.id.editDOB);
        editWhatsappNo = findViewById(R.id.editWhatsappNo);
        editEducation = findViewById(R.id.editEducation);
        occupationDropdown = findViewById(R.id.editOccupationType);
        editDesignation = findViewById(R.id.editDesignation);
        relationWithVadilDropdown = findViewById(R.id.editRelationWithVadil);
        editGotra = findViewById(R.id.editGotra);
        radioDonorTrue = findViewById(R.id.radioDonorTrue);
        radioDonorFalse = findViewById(R.id.radioDonorFalse);
        radioDisplayTrue = findViewById(R.id.radioDisplayTrue);
        radioDisplayFalse = findViewById(R.id.radioDisplayFalse);
        profile_pic = findViewById(R.id.profile_pic);
        edit_profile_pic = findViewById(R.id.edit_profile_pic);
        editHeight = findViewById(R.id.editHeight);
        editWeight = findViewById(R.id.editWeight);
        editMama = findViewById(R.id.editMama);
        editMotherMama = findViewById(R.id.editMotherMama);
        editFatherMama = findViewById(R.id.editFatherMama);
        editFathersName = findViewById(R.id.editFathersName);

        loadRelationsList();
        loadOccupationType();
        ArrayAdapter<String> adapterBloodGroup =
                new ArrayAdapter<String>(
                        UserProfileActivity.this,
                        android.R.layout.simple_dropdown_item_1line,
                        getResources().getStringArray(R.array.blood_group_array));
        bloodGroupDropdown = findViewById(R.id.editBloodGroup);
        bloodGroupDropdown.setAdapter(adapterBloodGroup);
        bloodGroupDropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                blood_group = getResources().getStringArray(R.array.blood_group_array)[position];
            }
        });

        ArrayAdapter<String> adapterMaritalStatus =
                new ArrayAdapter<String>(
                        UserProfileActivity.this,
                        android.R.layout.simple_dropdown_item_1line,
                        getResources().getStringArray(R.array.marital_status_array));
        maritalStatusDropdown = findViewById(R.id.editMaritalStatus);
        maritalStatusDropdown.setAdapter(adapterMaritalStatus);
        maritalStatusDropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    rlDisplayInMatrimonialTxt.setVisibility(View.GONE);
                    display_matrimonial = "no";
                } else {
                    rlDisplayInMatrimonialTxt.setVisibility(View.VISIBLE);
                }
                marital_status = getResources().getStringArray(R.array.marital_status_array)[position];
            }
        });

        editAge.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    editWhatsappNo.requestFocus();
                    return true;
                }
                return false;
            }
        });

        editDesignation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    editGotra.requestFocus();
                    return true;
                }
                return false;
            }
        });

        for (int i = 0; i < getResources().getStringArray(R.array.marital_status_array).length; i++) {
            if (Objects.equals(PreferenceUtils.getInstance(UserProfileActivity.this).getUserDetails().getMarital_status(), getResources().getStringArray(R.array.marital_status_array)[i])) {
                maritalStatusDropdown.setText(getResources().getStringArray(R.array.marital_status_array)[i], false);
                if (Objects.equals(maritalStatusDropdown.getText().toString().toLowerCase(), "married")) {
                    rlDisplayInMatrimonialTxt.setVisibility(View.GONE);
                }
            }
        }
        for (int i = 0; i < getResources().getStringArray(R.array.blood_group_array).length; i++) {
            if (Objects.equals(PreferenceUtils.getInstance(UserProfileActivity.this).getUserDetails().getBlood_group(), getResources().getStringArray(R.array.blood_group_array)[i])) {
                bloodGroupDropdown.setText(getResources().getStringArray(R.array.blood_group_array)[i], false);
            }
        }

        if (PreferenceUtils.getInstance(this).getUserDetails().getRelation_with_vadil().toLowerCase().equals("self")) {
            relationWithVadilDropdown.setText("Self", false);
            relationWithVadilDropdown.setEnabled(false);
        }

        Glide.with(this)
                .load(PreferenceUtils.getInstance(this).getUserDetails().getThumb())
                .into(profile_pic);
        editName.setText(PreferenceUtils.getInstance(this).getUserDetails().getName());
        editFathersName.setText(PreferenceUtils.getInstance(this).getUserDetails().getFather_name());
        editEmail.setText(PreferenceUtils.getInstance(this).getUserDetails().getEmailId());
        editMobile.setText(PreferenceUtils.getInstance(this).getUserDetails().getMobile_no());
        radioMale.setChecked(PreferenceUtils.getInstance(this).getUserDetails().getGender().toLowerCase().equals("male"));
        radioFemale.setChecked(PreferenceUtils.getInstance(this).getUserDetails().getGender().toLowerCase().equals("female"));
        radioDonorTrue.setChecked(String.valueOf(PreferenceUtils.getInstance(this).getUserDetails().getDonor()).toLowerCase().equals("true"));
        radioDonorFalse.setChecked(String.valueOf(PreferenceUtils.getInstance(this).getUserDetails().getDonor()).toLowerCase().equals("false"));
        radioDisplayTrue.setChecked(PreferenceUtils.getInstance(this).getUserDetails().getDisplay_matrimonial().equals("yes"));
        radioDisplayFalse.setChecked(PreferenceUtils.getInstance(this).getUserDetails().getDisplay_matrimonial().equals("no"));

        Log.e("TAG", "initMatrimonial: " + PreferenceUtils.getInstance(this).getUserDetails().getDisplay_matrimonial());

        Log.e("TAG", "initM: " + PreferenceUtils.getInstance(this).getUserDetails().getDisplay_matrimonial());
        Log.e("TAG", "initMama: " + PreferenceUtils.getInstance(this).getUserDetails().getMama_name()
                + "\n" + PreferenceUtils.getInstance(this).getUserDetails().getMother_mama()
                + "\n" + PreferenceUtils.getInstance(this).getUserDetails().getFather_mama());

        editMobile.setText(PreferenceUtils.getInstance(this).getUserDetails().getMobile_no());
        editEmail.setText(PreferenceUtils.getInstance(this).getUserDetails().getEmailId());
        editAge.setText(PreferenceUtils.getInstance(this).getUserDetails().getAge());
        editDOB.setText(PreferenceUtils.getInstance(this).getUserDetails().getDate_of_birth());
        editEducation.setText(PreferenceUtils.getInstance(this).getUserDetails().getEducation());
        editWhatsappNo.setText(PreferenceUtils.getInstance(this).getUserDetails().getWhatsappNo());
        occupationDropdown.setText(PreferenceUtils.getInstance(this).getUserDetails().getOccupation_type(), false);
        editDesignation.setText(PreferenceUtils.getInstance(this).getUserDetails().getDesignation());
        editHeight.setText(PreferenceUtils.getInstance(this).getUserDetails().getHeight());
        editWeight.setText(PreferenceUtils.getInstance(this).getUserDetails().getWeight());
        editMama.setText(PreferenceUtils.getInstance(this).getUserDetails().getMama_name());
        editMotherMama.setText(PreferenceUtils.getInstance(this).getUserDetails().getMother_mama());
        editFatherMama.setText(PreferenceUtils.getInstance(this).getUserDetails().getFather_mama());
        editGotra.setText(PreferenceUtils.getInstance(this).getUserDetails().getHobbies());

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);
        radioDonorTrue.setOnCheckedChangeListener(null);
        radioDonorFalse.setOnCheckedChangeListener(null);
        radioDisplayTrue.setOnCheckedChangeListener(null);
        radioDisplayFalse.setOnCheckedChangeListener(null);

        if (radioFemale.isChecked())
            gender = radioFemale.getText().toString();
        else
            gender = radioMale.getText().toString();

        if (radioDonorTrue.isChecked())
            donor = "yes";
        else
            donor = "no";

        if (radioDisplayTrue.isChecked())
            display_matrimonial = "yes";
        else
            display_matrimonial = "no";

        initProfileDetails();
        initClickListeners();

        maritalStatusDropdown.setText(marital_status, false);
    }

    private void initProfileDetails() {
        name = PreferenceUtils.getInstance(this).getUserDetails().getName();
        age = PreferenceUtils.getInstance(this).getUserDetails().getAge();
        date_of_birth = PreferenceUtils.getInstance(this).getUserDetails().getDate_of_birth();
        mobile_no = PreferenceUtils.getInstance(this).getUserDetails().getMobile_no();
        whatsappNo = PreferenceUtils.getInstance(this).getUserDetails().getWhatsappNo();
        blood_group = PreferenceUtils.getInstance(this).getUserDetails().getBlood_group();
        donor = String.valueOf(PreferenceUtils.getInstance(this).getUserDetails().getDonor());
        education = PreferenceUtils.getInstance(this).getUserDetails().getEducation();
        occupation_type = PreferenceUtils.getInstance(this).getUserDetails().getOccupation_type();
        designation = PreferenceUtils.getInstance(this).getUserDetails().getDesignation();
        display_matrimonial = PreferenceUtils.getInstance(this).getUserDetails().getDisplay_matrimonial();
        marital_status = PreferenceUtils.getInstance(this).getUserDetails().getMarital_status();
        relation_with_vadil = PreferenceUtils.getInstance(this).getUserDetails().getRelation_with_vadil();
        gender = PreferenceUtils.getInstance(this).getUserDetails().getGender();
        height = PreferenceUtils.getInstance(this).getUserDetails().getHeight();
        weight = PreferenceUtils.getInstance(this).getUserDetails().getWeight();
        mama_name = PreferenceUtils.getInstance(this).getUserDetails().getMama_name();
        mother_mama = PreferenceUtils.getInstance(this).getUserDetails().getMother_mama();
        father_mama = PreferenceUtils.getInstance(this).getUserDetails().getFather_mama();
        gotra = PreferenceUtils.getInstance(this).getUserDetails().getHobbies();
    }

    private void initClickListeners() {
        edit_profile_pic.setOnClickListener(v -> {
            boolean result = checkPermission();
            if (result) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, GALLERY_REQUEST_CODE);
            }
        });

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = "male";
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = "female";
        });

        radioDonorTrue.setOnClickListener(v -> {
            radioDonorTrue.setChecked(true);
            radioDonorFalse.setChecked(false);
            donor = "yes";
        });

        radioDonorFalse.setOnClickListener(v -> {
            radioDonorTrue.setChecked(false);
            radioDonorFalse.setChecked(true);
            donor = "no";
        });

        radioDisplayTrue.setOnClickListener(v -> {
            radioDisplayTrue.setChecked(true);
            radioDisplayFalse.setChecked(false);
            display_matrimonial = "yes";
        });

        radioDisplayFalse.setOnClickListener(v -> {
            radioDisplayTrue.setChecked(false);
            radioDisplayFalse.setChecked(true);
            display_matrimonial = "no";
        });

        editDOB.setOnClickListener(v -> {
            showDatePicker();
        });

        submitBtn.setOnClickListener(v -> {

            Log.e("TAG", "initClickListeners: " + editName.getText().toString()
                    + "\n" + editAge.getText().toString()
                    + "\n" + editDOB.getText().toString()
                    + "\n" + editMobile.getText().toString()
                    + "\n" + editWhatsappNo.getText().toString()
                    + "\n" + blood_group
                    + "\n" + donor
                    + "\n" + editEducation.getText().toString()
                    + "\n" + occupation_type
                    + "\n" + editDesignation.getText().toString()
                    + "\n" + marital_status
                    + "\n" + relation_with_vadil
                    + "\n" + gender
                    + "\n" + editGotra.getText().toString()
                    + "\n" + display_matrimonial
            );

            if (!TextUtils.isEmpty(editName.getText().toString())
                    && !TextUtils.isEmpty(editAge.getText().toString())
                    && !TextUtils.isEmpty(editDOB.getText().toString())
                    && !TextUtils.isEmpty(editMobile.getText().toString())
                    && !TextUtils.isEmpty(editWhatsappNo.getText().toString())
                    && !TextUtils.isEmpty(blood_group)
                    && !TextUtils.isEmpty(editFathersName.getText().toString())
                    && !TextUtils.isEmpty(donor)
                    && !TextUtils.isEmpty(editEducation.getText().toString())
                    && !TextUtils.isEmpty(occupation_type)
                    && !TextUtils.isEmpty(editDesignation.getText().toString())
                    && !TextUtils.isEmpty(marital_status)
                    && !TextUtils.isEmpty(relation_with_vadil)
                    && !TextUtils.isEmpty(gender)
                    && !TextUtils.isEmpty(editGotra.getText().toString())) {
                try {
                    name = editName.getText().toString();
                    age = editAge.getText().toString();
                    mobile_no = editMobile.getText().toString();
                    whatsappNo = editWhatsappNo.getText().toString();
                    education = editEducation.getText().toString();
                    designation = editDesignation.getText().toString();
                    gotra = editGotra.getText().toString();
                    height = editHeight.getText().toString();
                    weight = editWeight.getText().toString();
                    mama_name = editMama.getText().toString();
                    mother_mama = editMotherMama.getText().toString();
                    father_mama = editFatherMama.getText().toString();
                    String fathersName = editFathersName.getText().toString();

                    submitProfileDetails(
                            name,
                            fathersName,
                            age,
                            date_of_birth,
                            mobile_no,
                            whatsappNo,
                            blood_group,
                            donor,
                            education,
                            occupation_type,
                            designation,
                            display_matrimonial,
                            marital_status,
                            relation_with_vadil,
                            gender,
                            gotra,
                            height,
                            weight,
                            mama_name,
                            mother_mama,
                            father_mama);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "You must fill all the details to continue", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadRelationsList() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading states...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Relation_Data> call = service.getAllRelations();

        call.enqueue(new Callback<Relation_Data>() {
            @Override
            public void onResponse(Call<Relation_Data> call, Response<Relation_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        relationList = response.body().getData();
                        String[] relationArray = new String[relationList.size()];
                        for (int i = 0; i < relationList.size(); i++) {
                            relationArray[i] = relationList.get(i).getName();
                        }
                        ArrayAdapter<String> adapterRelation =
                                new ArrayAdapter<String>(
                                        UserProfileActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        relationArray);
                        relationWithVadilDropdown.setAdapter(adapterRelation);
                        relationWithVadilDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            relation_with_vadil = relationArray[position];
                        });

                        for (int i = 0; i < relationList.size(); i++) {
                            if (Objects.equals(PreferenceUtils.getInstance(UserProfileActivity.this).getUserDetails().getRelation_with_vadil(), relationList.get(i).getName())) {
                                relationWithVadilDropdown.setText(relationList.get(i).getName(), false);
                            }
                        }

                    } else if (response.code() == 400) {
                        Toast.makeText(UserProfileActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(UserProfileActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(UserProfileActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(UserProfileActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(UserProfileActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(UserProfileActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(UserProfileActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Relation_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(UserProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadOccupationType() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading bussiness types...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Occupation_Data> call = service.getOccupationType();

        call.enqueue(new Callback<Occupation_Data>() {
            @Override
            public void onResponse(Call<Occupation_Data> call, Response<Occupation_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();


                if (response.body() != null) {
                    if (response.code() == 200) {
                        occupationList = response.body().getData();
                        String[] occupationArray = new String[occupationList.size()];
                        for (int i = 0; i < occupationList.size(); i++) {
                            occupationArray[i] = occupationList.get(i).getOccupation_name();
                        }
                        ArrayAdapter<String> adapterOccupation =
                                new ArrayAdapter<String>(
                                        UserProfileActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        occupationArray);
                        occupationDropdown.setAdapter(adapterOccupation);
                        occupationDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            occupation_type = occupationArray[position];
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(UserProfileActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(UserProfileActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(UserProfileActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(UserProfileActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(UserProfileActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(UserProfileActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(UserProfileActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Occupation_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(UserProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitProfileDetails(String name, String fathersName, String age, String date_of_birth, String mobile_no, String whatsappNo, String blood_group, String donor,
                                      String education, String occupation_type, String designation, String display_matrimonial,
                                      String marital_status, String relation_with_vadil, String gender, String gotra,
                                      String height, String weight, String mama_name, String mother_mama, String father_mama) {
        if (CropUri != null) {
            file = new File(getRealPathFromURI(CropUri));
            requestBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(getContentResolver().getType(CropUri))), file);

            part = MultipartBody.Part.createFormData("profile_image", file.getName(), requestBody);
            part = MultipartBody.Part.createFormData("profile_image", "IMG.jpg", RequestBody.create(
                    MediaType.parse("image/jpeg"), PreferenceUtils.getInstance(UserProfileActivity.this).getUserDetails().getThumb()));
            Log.e("TAG", "submitProfileDetails: " + file + " | " + getContentResolver().getType(CropUri));
        }

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Updating profile...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);

        Call<Profile_Setup> call = service.editProfileDetails(
                "application/json",
                "Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                requestBody,
                RequestBody.create(MediaType.parse("text/plain"), name),
                RequestBody.create(MediaType.parse("text/plain"), fathersName),
                RequestBody.create(MediaType.parse("text/plain"), age),
                RequestBody.create(MediaType.parse("text/plain"), date_of_birth),
                RequestBody.create(MediaType.parse("text/plain"), mobile_no),
                RequestBody.create(MediaType.parse("text/plain"), whatsappNo),
                RequestBody.create(MediaType.parse("text/plain"), blood_group),
                RequestBody.create(MediaType.parse("text/plain"), donor),
                RequestBody.create(MediaType.parse("text/plain"), education),
                RequestBody.create(MediaType.parse("text/plain"), occupation_type),
                RequestBody.create(MediaType.parse("text/plain"), designation),
                RequestBody.create(MediaType.parse("text/plain"), display_matrimonial),
                RequestBody.create(MediaType.parse("text/plain"), marital_status),
                RequestBody.create(MediaType.parse("text/plain"), relation_with_vadil),
                RequestBody.create(MediaType.parse("text/plain"), gender),
                RequestBody.create(MediaType.parse("text/plain"), gotra),
                RequestBody.create(MediaType.parse("text/plain"), height),
                RequestBody.create(MediaType.parse("text/plain"), weight),
                RequestBody.create(MediaType.parse("text/plain"), mama_name),
                RequestBody.create(MediaType.parse("text/plain"), mother_mama),
                RequestBody.create(MediaType.parse("text/plain"), father_mama));

        Log.e("TAG", "onSubmit: " +
                "\n" + name + "\n" + age + "\n" + date_of_birth + "\n" + mobile_no + "\n" + whatsappNo + "\n" + blood_group + "\n" + donor + "\n" + education + "\n" + occupation_type + "\n"
                + designation + "\n" + display_matrimonial + "\n" + marital_status + "\n" + relation_with_vadil + "\n" + gender + "\n" + gotra + "\n" + fathersName);

        call.enqueue(new Callback<Profile_Setup>() {
            @Override
            public void onResponse(Call<Profile_Setup> call, Response<Profile_Setup> response) {
                Log.e("TAG", "onResponse: " + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200)
                        try {
                            Toast.makeText(UserProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("TAG", "onResponseSuccess: " + response.body().getData().getUser_details().getName());
                            PreferenceUtils.getInstance(UserProfileActivity.this).setLoginStatus(true);
                            PreferenceUtils.getInstance(UserProfileActivity.this).setuserDetailsFill(response.body().getData().getUser_details());
                            PreferenceUtils.getInstance(UserProfileActivity.this).setIsFilled(response.body().getData().getIs_filled());
                            Log.e("TAG", "onResponse: " + response.body().getData().getIs_filled());
                            Intent intent = new Intent(UserProfileActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        } catch (Exception e) {
                            Toast.makeText(UserProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    else if (response.code() == 400) {
                        Toast.makeText(UserProfileActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(UserProfileActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(UserProfileActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(UserProfileActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(UserProfileActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(UserProfileActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(UserProfileActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile_Setup> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(UserProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(UserProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(UserProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) UserProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) &&
                        ActivityCompat.shouldShowRequestPermissionRationale((Activity) UserProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(UserProfileActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission needed");
                    alertBuilder.setMessage("Write storage permission is necessary to choose image!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(UserProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) UserProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            try {
                selectedImage = data.getData();
                Bitmap mBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                int viewWidth;
                int viewHeight;
                int bmWidth;
                int bmHeight;
                double bmRatio;
                double viewRatio;
                viewWidth = getResources().getDisplayMetrics().widthPixels;
                viewHeight = (getResources().getDisplayMetrics().heightPixels) * 80 / 100;
                viewRatio = (double) viewHeight / (double) viewWidth;
                bmRatio = (double) mBitmap.getHeight() / (double) mBitmap.getWidth();
                if (bmRatio < viewRatio) {
                    bmWidth = viewWidth;
                    bmHeight = (int) (((double) viewWidth) * ((double) (mBitmap.getHeight()) / (double) (mBitmap.getWidth())));
                } else {
                    bmHeight = viewHeight;
                    bmWidth = (int) (((double) viewHeight) * ((double) (mBitmap.getWidth()) / (double) (mBitmap.getHeight())));
                }
                mBitmap = Bitmap.createScaledBitmap(mBitmap, bmWidth, bmHeight, false);
                opencropdialog(mBitmap);
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            handleCropResult(result);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startCropImageActivity(mCropImageUri);
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).setMultiTouchEnabled(true).start(this);
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (contentUri != null) {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();
            return result;
        } else {
            return null;
        }
    }

    private void opencropdialog(Bitmap mBitmap) {
        final Dialog d = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        View dialogLayout = getLayoutInflater().inflate(R.layout.dialog_crop_profile, null);
        Window window = d.getWindow();
        window.setBackgroundDrawableResource(R.color.dialog_transparent);
        d.setContentView(dialogLayout);
        dialogLayout.findViewById(R.id.main_action_crop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCropImageView.getCroppedImageAsync();
                d.dismiss();
            }
        });
        dialogLayout.findViewById(R.id.rotate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCropImageView.rotateImage(90);
            }
        });
        dialogLayout.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });

        mCropImageView = dialogLayout.findViewById(R.id.cropImageView);
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);
        updateCurrentCropViewOptions();
        mCropImageView.setImageBitmap(mBitmap);
        d.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        d.setCancelable(false);
        d.show();
    }

    public void updateCurrentCropViewOptions() {
        CropImageViewOptions options = new CropImageViewOptions();
        options.scaleType = mCropImageView.getScaleType();
        options.cropShape = mCropImageView.getCropShape();
        options.guidelines = mCropImageView.getGuidelines();
        options.aspectRatio = mCropImageView.getAspectRatio();
        options.fixAspectRatio = mCropImageView.isFixAspectRatio();
        options.showCropOverlay = mCropImageView.isShowCropOverlay();
        options.showProgressBar = mCropImageView.isShowProgressBar();
        options.autoZoomEnabled = mCropImageView.isAutoZoomEnabled();
        options.maxZoomLevel = mCropImageView.getMaxZoom();
        options.flipHorizontally = mCropImageView.isFlippedHorizontally();
        options.flipVertically = mCropImageView.isFlippedVertically();
    }

    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {
            if (result.getUri() != null) {
                CropUri = result.getUri();
                profile_pic.setImageURI(result.getUri());
                profile_image = String.valueOf(CropUri);
            } else {
                String path = MediaStore.Images.Media.insertImage(getContentResolver(), result.getBitmap(), "IMG", null);
                Log.e("URI", "?" + getRealPathFromURI(Uri.parse(path)));
                CropUri = Uri.parse(path);
                profile_pic.setImageBitmap(result.getBitmap());
                profile_image = String.valueOf(CropUri);
                Log.e("TAG", "handleCropResult: " + profile_image);
            }
            try {
                is = getContentResolver().openInputStream(Uri.parse(profile_image));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("AIC", "Failed to crop image", result.getError());
            Toast.makeText(this, "Image crop failed: " + result.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void showDatePicker() {
        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(UserProfileActivity.this,
                (datePicker, year, monthOfYear, dayOfMonth) -> {
                    date_of_birth = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    editDOB.setText(date_of_birth);
                }, year, month, day);
        datePickerDialog.show();
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            Toast.makeText(this, "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("AIC", "Failed to load image by URI", error);
            Toast.makeText(this, "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}