package oswal.jain.samaj.ui.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import oswal.jain.samaj.CommunityApp;
import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Login_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static oswal.jain.samaj.ui.activities.HomeActivity.nightModeEnabled;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button loginBtn;
    TextInputEditText editMail, editPass;
    SharedPreferences preferences;
    TextView signup_here;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

        preferences = getSharedPreferences(CommunityApp.APP_PREFS, MODE_PRIVATE);
        if (preferences.getBoolean(nightModeEnabled, false)) {
            findViewById(R.id.content_login).setBackgroundColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        } else {
            findViewById(R.id.content_login).setBackgroundColor(ContextCompat.getColor(this, R.color.light_theme_color));
        }
    }

    private void init() {
        loginBtn = findViewById(R.id.loginBtn);
        editMail = findViewById(R.id.editMail);
        editPass = findViewById(R.id.editPass);
        signup_here = findViewById(R.id.signup_here);

        loginBtn.setOnClickListener(this);
        signup_here.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                if (!TextUtils.isEmpty(editMail.getText().toString()) && !TextUtils.isEmpty(editPass.getText().toString())) {
                    if (AppUtils.isNetworkAvailable(this))
                        loginUser(editMail.getText().toString(), editPass.getText().toString());
                    else
                        Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(editMail.getText().toString())) {
                    editMail.setError("Email or Phone No. required.");
                    editMail.requestFocus();
                } else {
                    editPass.setError("Password required.");
                    editPass.requestFocus();
                }
                break;
            case R.id.signup_here:
                opendialog();
                break;
        }
    }

    private void opendialog() {
        final Dialog d = new Dialog(this, R.style.mydialog);
        View dialogLayout = getLayoutInflater().inflate(R.layout.dialog_edit_profile, null);
        Window window = d.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        d.setContentView(dialogLayout);
        TextView desc = dialogLayout.findViewById(R.id.desc);
        desc.setText(getResources().getString(R.string.how_to_use_txt, Html.fromHtml("<b>Important:</b>\n" +
                "        <br/><br/>to register follow steps\n" +
                "        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. 1st registration must done by Family head(or his Mobile no.)\n" +
                "        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Once Family Head Login, he will add other family members.\n" +
                "        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. After family member get add they don’t need to register they can directly login with their own mobile number.\n" +
                "        <br/><br/>(Note: Family head have to wait for 2-3 days for approval by admin for security purpose)\n" +
                "        <br/><br/><b>रजिस्टर करने के लिए नीचे दिए गए स्टेप फॉलो करें :</b>\n" +
                "        <br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. पहले रजिस्ट्रेशन फैमिली के हेड द्वारा ही किया जाना चाहिए या उनके मोबाइल नंबर द्वारा\n" +
                "        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.एक बार फैमिली हेड लॉगइन कर ले उसके बाद वह अपने अन्य फैमिली मेंबर्स को ऐड करेंगे.\n" +
                "        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.फैमिली मेंबर ऐड होने के बाद फैमिली मेंबर्स को रजिस्टर कराने की आवश्यकता नहीं है वह सीधे तौर पर अपने मोबाइल नंबर से लॉगिन कर सकते हैं.\n" +
                "        <br/><br/>नोट:  फैमिली हेड को अप्रूव करने में 2 से 3 दिन का वक्त लग सकता है यह सिक्योरिटी के लिए रखा गया है.")));
        dialogLayout.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
        d.setCancelable(false);
        d.show();
    }

    private void loginUser(String email, String password) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Logging in...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Login_Data> call = service.userLogin(email, password);

        call.enqueue(new Callback<Login_Data>() {
            @Override
            public void onResponse(Call<Login_Data> call, Response<Login_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200) {
                        try {
                            Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            PreferenceUtils.getInstance(LoginActivity.this).setLoginStatus(true);
                            PreferenceUtils.getInstance(LoginActivity.this).setAuthToken(response.body().getData().getToken());
                            PreferenceUtils.getInstance(LoginActivity.this).setuserDetails(response.body().getData().getUser_details());
                            PreferenceUtils.getInstance(LoginActivity.this).setIsFilled(response.body().getData().getIs_filled());
                            Log.e("TAG", "onResponse: " + response.body().getData().getUser_details().getFamily_id());
                            if (PreferenceUtils.getInstance(LoginActivity.this).getIsFilled() == 0) {
                                startActivity(new Intent(LoginActivity.this, ProfileSetupActivity.class));
                                finish();
                            } else {
                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                finish();
                            }
                        } catch (Exception e) {
                            Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 400) {
                        Toast.makeText(LoginActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                    } else if (response.code() == 401) {
                        Toast.makeText(LoginActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                    } else if (response.code() == 500) {
                        Toast.makeText(LoginActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(LoginActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Login_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
