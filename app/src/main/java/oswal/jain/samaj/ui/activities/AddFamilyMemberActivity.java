package oswal.jain.samaj.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Family_Member;
import oswal.jain.samaj.data.models.Relation_Data;
import oswal.jain.samaj.interfaces.APIService;
import oswal.jain.samaj.utils.AppConstants;
import oswal.jain.samaj.utils.AppUtils;
import oswal.jain.samaj.utils.PreferenceUtils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddFamilyMemberActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button registerBtn;
    TextInputEditText editName, editMobile, editEmail, editPassword, editFathersName;
    RadioButton radioMale, radioFemale;
    ArrayList<Relation_Data.Relation_Details> relationList;
    String gender, relation_with_vadil;
    AutoCompleteTextView relationWithVadilDropdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_family_member);
        init();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            finish();
        });

        registerBtn.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(editName.getText().toString())
                    && !TextUtils.isEmpty(editMobile.getText().toString())
                    && !TextUtils.isEmpty(gender)
                    && !TextUtils.isEmpty(editFathersName.getText().toString())
                    && !TextUtils.isEmpty(editPassword.getText().toString())) {
                String username = editName.getText().toString();
                String mobile = editMobile.getText().toString();
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();
                String relation_with_vadil = editPassword.getText().toString();
                String fathersName = editFathersName.getText().toString();
                if (AppUtils.isNetworkAvailable(this)) {
                    addFamilyMember(username, fathersName, email, password, mobile, gender, relation_with_vadil);
                } else
                    Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "You must fill all the details to register a new account.", Toast.LENGTH_SHORT).show();
        });
        loadRelationsList();
    }

    private void loadRelationsList() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading states...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Relation_Data> call = service.getAllRelations();

        call.enqueue(new Callback<Relation_Data>() {
            @Override
            public void onResponse(Call<Relation_Data> call, Response<Relation_Data> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.code() == 200) {
                        relationList = response.body().getData();
                        String[] relationArray = new String[relationList.size()];
                        for (int i = 0; i < relationList.size(); i++) {
                            relationArray[i] = relationList.get(i).getName();
                        }
                        ArrayAdapter<String> adapterRelation =
                                new ArrayAdapter<String>(
                                        AddFamilyMemberActivity.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        relationArray);
                        relationWithVadilDropdown.setAdapter(adapterRelation);
                        relationWithVadilDropdown.setOnItemClickListener((parent, view, position, id1) -> {
                            relation_with_vadil = relationArray[position];
                        });
                    } else if (response.code() == 400) {
                        Toast.makeText(AddFamilyMemberActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(AddFamilyMemberActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(AddFamilyMemberActivity.this, response.body().getMessage() + ". Login again", Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(AddFamilyMemberActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(AddFamilyMemberActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddFamilyMemberActivity.this, "Error occurred. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(AddFamilyMemberActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Relation_Data> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(AddFamilyMemberActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addFamilyMember(String username, String fathersName, String email, String password, String mobile, String gender, String relation_with_vadil) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Adding family member...");
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Family_Member> call = service.addFamilyMember("Bearer " + PreferenceUtils.getInstance(this).getAuthToken(),
                username, fathersName, email, password, mobile, gender, relation_with_vadil);
        call.enqueue(new Callback<Family_Member>() {
            @Override
            public void onResponse(@NonNull Call<Family_Member> call, @NonNull Response<Family_Member> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200) {
                        try {
                            Toast.makeText(AddFamilyMemberActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (Exception e) {
                            Toast.makeText(AddFamilyMemberActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 400) {
                        Toast.makeText(AddFamilyMemberActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        Log.e("TAG", "ggez: " + "fdfdsfsfd");
                        startActivity(new Intent(AddFamilyMemberActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 401) {
                        Toast.makeText(AddFamilyMemberActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceUtils.getInstance(getApplicationContext()).setLoginStatus(false);
                        startActivity(new Intent(AddFamilyMemberActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.code() == 500) {
                        Toast.makeText(AddFamilyMemberActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddFamilyMemberActivity.this, "Error occured. Login again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Toast.makeText(AddFamilyMemberActivity.this, new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Family_Member> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(AddFamilyMemberActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void init() {
        relationList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        relationWithVadilDropdown = findViewById(R.id.editRelationWithVadil);
        registerBtn = findViewById(R.id.registerBtn);
        editName = findViewById(R.id.editfullName);
        editMobile = findViewById(R.id.editMobile);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);
        editFathersName = findViewById(R.id.editFathersName);

        radioMale.setOnCheckedChangeListener(null);
        radioFemale.setOnCheckedChangeListener(null);

        radioMale.setOnClickListener(v -> {
            radioMale.setChecked(true);
            radioFemale.setChecked(false);
            gender = radioMale.getText().toString();
        });

        radioFemale.setOnClickListener(v -> {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
            gender = radioFemale.getText().toString();
        });
    }
}