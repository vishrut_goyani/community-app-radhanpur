package oswal.jain.samaj.utils;

public class AppConstants {
    public static final String BASE_URL = "http://oswal.gbtechnobrain.com/";
    public static final String NEW_USER = "new_user";
    public static final String FAMILY_MEMBER = "family_member";
    public static final String REGISTER_TYPE = "register_type";
    public static final String CONTACT_US = "https://forms.gle/dSXRY8edMj3ndLNz5";

    public static int FAMILY_ID = -1;
    public static int SEARCH_TYPE = -1;
}
