package oswal.jain.samaj.utils;

import android.content.Context;
import android.content.SharedPreferences;

import oswal.jain.samaj.data.models.Login_Data;
import oswal.jain.samaj.data.models.Profile_Setup;

import static oswal.jain.samaj.CommunityApp.APP_PREFS;

public class PreferenceUtils {

    public static SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private static PreferenceUtils mInstance;
    public static final String isLoggedIn = "isLoggedIn";
    public static Context mContext;

    public static final String KEY_AUTH_TOKEN = "auth_token";
    public static final String KEY_USER_ID = "_id";
    public static final String KEY_USER_FAMILY_ID = "family_id";
    public static final String KEY_USER_NAME = "name";
    public static final String KEY_USER_GENDER = "gender";
    public static final String KEY_USER_AGE = "age";
    public static final String KEY_USER_DOB = "date_of_birth";
    public static final String KEY_USER_COUNTRY_CODE = "country_code";
    public static final String KEY_USER_MOBILE = "mobile_no";
    public static final String KEY_USER_COUNTRY_CODE2 = "country_code2";
    public static final String KEY_USER_MOBILE2 = "mobile_no2";
    public static final String KEY_USER_REL_WITH_VADIL = "relation_with_vadil";
    public static final String KEY_USER_EDUCATION = "education";
    public static final String KEY_USER_EMAIL = "emailId";
    public static final String KEY_USER_PROFILE = "profile_image";
    public static final String KEY_USER_BLOOD_GROUP = "blood_group";
    public static final String KEY_USER_DONOR = "donor";
    public static final String KEY_USER_MARITAL_STATUS = "marital_status";
    public static final String KEY_USER_WHATSAPP_NO = "whatsappNo";
    public static final String KEY_USER_OCCUPATION_TYPE = "occupation_type";
    public static final String KEY_USER_COMPANY_NAME = "company_name";
    public static final String KEY_USER_COMPANY_ADDRESS = "company_address";
    public static final String KEY_USER_DESIGNATION = "designation";
    public static final String KEY_USER_NO_OF_UNMARRIED_BROTHER = "no_of_unmarried_brother";
    public static final String KEY_USER_NO_OF_UNMARRIED_SISTER = "no_of_unmarried_sister";
    public static final String KEY_USER_FATHER_NAME = "father_name";
    public static final String KEY_USER_MOTHER_NAME = "mother_name";
    public static final String KEY_USER_HEIGHT = "height";
    public static final String KEY_USER_WEIGHT = "weight";
    public static final String KEY_USER_MAMA_NAME = "mama_name";
    public static final String KEY_USER_MOTHER_MAMA = "mother_mama";
    public static final String KEY_USER_FATHER_MAMA = "father_mama";
    public static final String KEY_USER_TOTAL_BROTHER = "total_brother";
    public static final String KEY_USER_TOTAL_SISTER = "total_sister";
    public static final String KEY_USER_MOSAL_NAME = "mosal_name";
    public static final String KEY_USER_KUTUM_MOTU_NAME = "kutum_motu_name";
    public static final String KEY_USER_FATHER_PROFESSION = "father_profession";
    public static final String KEY_USER_HOBBIES = "hobbies";
    public static final String KEY_USER_DISPLAY_MATRIMONIAL = "display_matrimonial";
    public static final String KEY_USER_ANNIVERSARY_DATE = "anniversary_date";
    public static final String KEY_USER_SECTOR = "sector";
    public static final String KEY_USER_OTP = "otp";
//    public static final String KEY_USER_IS_OTP_VERIFY = "is_otp_verify";
    public static final String KEY_DEVICE_TOKEN = "device_token";
    public static final String KEY_DEVICE_TYPE = "device_type";
    public static final String KEY_USER_IS_ACTIVE = "is_active";
    public static final String KEY_USER_IS_FILLED = "is_filled";
    public static final String KEY_USER_THUMB = "thumb";

    public PreferenceUtils(Context context) {
        mContext = context;
        try {
            prefs = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized PreferenceUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferenceUtils(context);
        }
        return mInstance;
    }

    public void setLoginStatus(boolean status) {
        prefs.edit().putBoolean(isLoggedIn, status).apply();
    }

    public void setAuthToken(String authToken) {
        prefs.edit().putString(KEY_AUTH_TOKEN, authToken).apply();
    }

    public String getAuthToken() {
        return prefs.getString(KEY_AUTH_TOKEN, null);
    }

    public void setDeviceToken(String deviceToken) {
        prefs.edit().putString(KEY_DEVICE_TOKEN, deviceToken).apply();
    }

    public String getDeviceToken() {
        return prefs.getString(KEY_DEVICE_TOKEN, null);
    }

    public boolean getLoginStatus() {
        return prefs.getBoolean(isLoggedIn, false);
    }

    public void setuserDetails(Login_Data.User_Data.User_Details user_details) {
        editor = prefs.edit();
        editor.putInt(KEY_USER_ID, user_details.getId());
        editor.putString(KEY_USER_FAMILY_ID, user_details.getFamily_id());
        editor.putString(KEY_USER_NAME, user_details.getName());
        editor.putString(KEY_USER_GENDER, user_details.getGender());
        editor.putString(KEY_USER_AGE, user_details.getAge());
        editor.putString(KEY_USER_DOB, user_details.getDate_of_birth());
        editor.putString(KEY_USER_COUNTRY_CODE, user_details.getCountry_code());
        editor.putString(KEY_USER_MOBILE, user_details.getMobile_no());
        editor.putString(KEY_USER_COUNTRY_CODE2, user_details.getCountry_code2());
        editor.putString(KEY_USER_MOBILE2, user_details.getMobile_no2());
        editor.putString(KEY_USER_REL_WITH_VADIL, user_details.getRelation_with_vadil());
        editor.putString(KEY_USER_EDUCATION, user_details.getEducation());
        editor.putString(KEY_USER_EMAIL, user_details.getEmailId());
        editor.putString(KEY_USER_PROFILE, user_details.getProfile_image());
        editor.putString(KEY_USER_BLOOD_GROUP, user_details.getBlood_group());
        editor.putBoolean(KEY_USER_DONOR, user_details.getDonor());
        editor.putString(KEY_USER_MARITAL_STATUS, user_details.getMarital_status());
        editor.putString(KEY_USER_WHATSAPP_NO, user_details.getWhatsappNo());
        editor.putString(KEY_USER_OCCUPATION_TYPE, user_details.getOccupation_type());
        editor.putString(KEY_USER_COMPANY_NAME, user_details.getCompany_name());
        editor.putString(KEY_USER_COMPANY_ADDRESS, user_details.getCompany_address());
        editor.putString(KEY_USER_DESIGNATION, user_details.getDesignation());
        editor.putString(KEY_USER_NO_OF_UNMARRIED_BROTHER, user_details.getNo_of_unmarried_brother());
        editor.putString(KEY_USER_NO_OF_UNMARRIED_SISTER, user_details.getNo_of_unmarried_sister());
        editor.putString(KEY_USER_FATHER_NAME, user_details.getFather_name());
        editor.putString(KEY_USER_MOTHER_NAME, user_details.getMother_name());
        editor.putString(KEY_USER_HEIGHT, user_details.getHeight());
        editor.putString(KEY_USER_WEIGHT, user_details.getWeight());
        editor.putString(KEY_USER_MAMA_NAME, user_details.getMama_name());
        editor.putString(KEY_USER_MOTHER_MAMA, user_details.getMother_mama());
        editor.putString(KEY_USER_FATHER_MAMA, user_details.getFather_mama());
        editor.putString(KEY_USER_TOTAL_BROTHER, user_details.getTotal_brother());
        editor.putString(KEY_USER_TOTAL_SISTER, user_details.getTotal_sister());
        editor.putString(KEY_USER_MOSAL_NAME, user_details.getMosal_name());
        editor.putString(KEY_USER_KUTUM_MOTU_NAME, user_details.getKutum_motu_name());
        editor.putString(KEY_USER_FATHER_PROFESSION, user_details.getFather_profession());
        editor.putString(KEY_USER_HOBBIES, user_details.getHobbies());
        editor.putString(KEY_USER_DISPLAY_MATRIMONIAL, user_details.getDisplay_matrimonial());
        editor.putString(KEY_USER_ANNIVERSARY_DATE, user_details.getAnniversary_date());
        editor.putString(KEY_USER_SECTOR, user_details.getSector());
        editor.putString(KEY_USER_OTP, user_details.getOtp());
        editor.putString(KEY_DEVICE_TOKEN, user_details.getDevice_token());
        editor.putString(KEY_DEVICE_TYPE, user_details.getDevice_type());
        editor.putInt(KEY_USER_IS_ACTIVE, user_details.getIs_active());
        editor.putInt(KEY_USER_IS_FILLED, user_details.getIs_active());
        editor.putString(KEY_USER_THUMB, user_details.getThumb());
        editor.apply();
    }

    public void setuserDetailsFill(Profile_Setup.User_Data.User_Details user_details) {
        editor = prefs.edit();
        editor.putInt(KEY_USER_ID, user_details.getId());
        editor.putString(KEY_USER_NAME, user_details.getName());
        editor.putString(KEY_USER_GENDER, user_details.getGender());
        editor.putString(KEY_USER_AGE, user_details.getAge());
        editor.putString(KEY_USER_DOB, user_details.getDate_of_birth());
        editor.putString(KEY_USER_COUNTRY_CODE, user_details.getCountry_code());
        editor.putString(KEY_USER_MOBILE, user_details.getMobile_no());
        editor.putString(KEY_USER_COUNTRY_CODE2, user_details.getCountry_code2());
        editor.putString(KEY_USER_MOBILE2, user_details.getMobile_no2());
        editor.putString(KEY_USER_REL_WITH_VADIL, user_details.getRelation_with_vadil());
        editor.putString(KEY_USER_EDUCATION, user_details.getEducation());
        editor.putString(KEY_USER_EMAIL, user_details.getEmailId());
        editor.putString(KEY_USER_PROFILE, user_details.getProfile_image());
        editor.putString(KEY_USER_BLOOD_GROUP, user_details.getBlood_group());
        editor.putBoolean(KEY_USER_DONOR, user_details.isDonor());
        editor.putString(KEY_USER_MARITAL_STATUS, user_details.getMarital_status());
        editor.putString(KEY_USER_WHATSAPP_NO, user_details.getWhatsappNo());
        editor.putString(KEY_USER_OCCUPATION_TYPE, user_details.getOccupation_type());
        editor.putString(KEY_USER_COMPANY_NAME, user_details.getCompany_name());
        editor.putString(KEY_USER_COMPANY_ADDRESS, user_details.getCompany_address());
        editor.putString(KEY_USER_DESIGNATION, user_details.getDesignation());
        editor.putString(KEY_USER_NO_OF_UNMARRIED_BROTHER, user_details.getNo_of_unmarried_brother());
        editor.putString(KEY_USER_NO_OF_UNMARRIED_SISTER, user_details.getNo_of_unmarried_sister());
        editor.putString(KEY_USER_FATHER_NAME, user_details.getFather_name());
        editor.putString(KEY_USER_MOTHER_NAME, user_details.getMother_name());
        editor.putString(KEY_USER_HEIGHT, user_details.getHeight());
        editor.putString(KEY_USER_WEIGHT, user_details.getWeight());
        editor.putString(KEY_USER_MAMA_NAME, user_details.getMama_name());
        editor.putString(KEY_USER_MOTHER_MAMA, user_details.getMother_mama());
        editor.putString(KEY_USER_FATHER_MAMA, user_details.getFather_mama());
        editor.putString(KEY_USER_TOTAL_BROTHER, user_details.getTotal_brother());
        editor.putString(KEY_USER_TOTAL_SISTER, user_details.getTotal_sister());
        editor.putString(KEY_USER_MOSAL_NAME, user_details.getMosal_name());
        editor.putString(KEY_USER_KUTUM_MOTU_NAME, user_details.getKutum_motu_name());
        editor.putString(KEY_USER_FATHER_PROFESSION, user_details.getFather_profession());
        editor.putString(KEY_USER_HOBBIES, user_details.getHobbies());
        editor.putString(KEY_USER_DISPLAY_MATRIMONIAL, user_details.getDisplay_matrimonial());
        editor.putString(KEY_USER_ANNIVERSARY_DATE, user_details.getAnniversary_date());
        editor.putString(KEY_USER_SECTOR, user_details.getSector());
        editor.putString(KEY_USER_OTP, user_details.getOtp());
        editor.putString(KEY_DEVICE_TOKEN, user_details.getDevice_token());
        editor.putString(KEY_DEVICE_TYPE, user_details.getDevice_type());
        editor.putInt(KEY_USER_IS_ACTIVE, user_details.getIs_active());
        editor.putInt(KEY_USER_IS_FILLED, user_details.getIs_active());
        editor.putString(KEY_USER_THUMB, user_details.getThumb());
        editor.apply();
    }

    public Login_Data.User_Data.User_Details getUserDetails() {
        return new Login_Data.User_Data.User_Details(
                prefs.getInt(KEY_USER_ID, 0),
                prefs.getString(KEY_USER_FAMILY_ID, null),
                prefs.getString(KEY_USER_NAME, null),
                prefs.getString(KEY_USER_GENDER, null),
                prefs.getString(KEY_USER_AGE, null),
                prefs.getString(KEY_USER_DOB, null),
                prefs.getString(KEY_USER_COUNTRY_CODE, null),
                prefs.getString(KEY_USER_MOBILE, null),
                prefs.getString(KEY_USER_COUNTRY_CODE2, null),
                prefs.getString(KEY_USER_MOBILE2, null),
                prefs.getString(KEY_USER_REL_WITH_VADIL, null),
                prefs.getString(KEY_USER_EDUCATION, null),
                prefs.getString(KEY_USER_EMAIL, null),
                prefs.getString(KEY_USER_PROFILE, null),
                prefs.getString(KEY_USER_BLOOD_GROUP, null),
                prefs.getBoolean(KEY_USER_DONOR, false),
                prefs.getString(KEY_USER_MARITAL_STATUS, null),
                prefs.getString(KEY_USER_WHATSAPP_NO, null),
                prefs.getString(KEY_USER_OCCUPATION_TYPE, null),
                prefs.getString(KEY_USER_COMPANY_NAME, null),
                prefs.getString(KEY_USER_COMPANY_ADDRESS, null),
                prefs.getString(KEY_USER_DESIGNATION, null),
                prefs.getString(KEY_USER_NO_OF_UNMARRIED_BROTHER, null),
                prefs.getString(KEY_USER_NO_OF_UNMARRIED_SISTER, null),
                prefs.getString(KEY_USER_FATHER_NAME, null),
                prefs.getString(KEY_USER_MOTHER_NAME, null),
                prefs.getString(KEY_USER_HEIGHT, null),
                prefs.getString(KEY_USER_WEIGHT, null),
                prefs.getString(KEY_USER_MAMA_NAME, null),
                prefs.getString(KEY_USER_MOTHER_MAMA, null),
                prefs.getString(KEY_USER_FATHER_MAMA, null),
                prefs.getString(KEY_USER_TOTAL_BROTHER, null),
                prefs.getString(KEY_USER_TOTAL_SISTER, null),
                prefs.getString(KEY_USER_MOSAL_NAME, null),
                prefs.getString(KEY_USER_KUTUM_MOTU_NAME, null),
                prefs.getString(KEY_USER_FATHER_PROFESSION, null),
                prefs.getString(KEY_USER_HOBBIES, null),
                prefs.getString(KEY_USER_DISPLAY_MATRIMONIAL, null),
                prefs.getString(KEY_USER_ANNIVERSARY_DATE, null),
                prefs.getString(KEY_USER_SECTOR, null),
                prefs.getString(KEY_USER_OTP, null),
                prefs.getString(KEY_DEVICE_TOKEN, null),
                prefs.getString(KEY_DEVICE_TYPE, null),
                prefs.getInt(KEY_USER_IS_ACTIVE, 0),
                prefs.getString(KEY_USER_THUMB, null)
        );
    }

    public void setIsFilled(int is_filled){
        prefs.edit().putInt(KEY_USER_IS_FILLED, is_filled).apply();
    }

    public int getIsFilled(){
        return prefs.getInt(KEY_USER_IS_FILLED, 0);
    }

}
