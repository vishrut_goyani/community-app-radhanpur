package oswal.jain.samaj.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import oswal.jain.samaj.R;

public class AppUtils {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getDeviceToken(Context context) {
        try {
            FirebaseApp.initializeApp(context);
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Log.w("TOOK", "getInstanceId failed", task.getException());
                            return;
                        }

                        String token = task.getResult().getToken();
                        String device_token = context.getString(R.string.device_token, token);
                        PreferenceUtils.getInstance(context).setDeviceToken(device_token);
                        Log.e("Token", " : " + device_token);
                    });


        } catch (Exception e) {
            Log.e("TokenError", " : " + e.getMessage());
        }
        return PreferenceUtils.getInstance(context).getDeviceToken();
    }

    public static boolean getAppInstalledOrNot(Context context, String url) {
        PackageManager packageManager = context.getPackageManager();
        boolean app_installed;
        try {
            packageManager.getPackageInfo(url, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
