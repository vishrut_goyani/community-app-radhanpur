package oswal.jain.samaj.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import oswal.jain.samaj.R;
import oswal.jain.samaj.ui.activities.ProfileDetailsActivity;

public class FamilyDetailsFragment extends Fragment {

    TextView head_name, father_name, mobile, whatsapp, city, state;
    Context context;
    int user_id;

    public static FamilyDetailsFragment newInstance(String head_name, String fatherName, String mobile, String whatsapp, String city, String state, int user_id) {
        FamilyDetailsFragment fragment = new FamilyDetailsFragment();

        Bundle args = new Bundle();
        args.putString(ProfileDetailsActivity.HEAD_NAME, head_name);
        args.putString(ProfileDetailsActivity.FATHER_NAME, fatherName);
        args.putString(ProfileDetailsActivity.MOBILE, mobile);
        args.putString(ProfileDetailsActivity.WHATSAPP, whatsapp);
        args.putString(ProfileDetailsActivity.CITY, city);
        args.putString(ProfileDetailsActivity.STATE, state);
        args.putInt(ProfileDetailsActivity.USER_ID_NEW, user_id);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_family_details, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        head_name = view.findViewById(R.id.head_name);
        father_name = view.findViewById(R.id.father_name);
        mobile = view.findViewById(R.id.mobile);
        whatsapp = view.findViewById(R.id.whatsapp);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);

        head_name.setText(getArguments().getString(ProfileDetailsActivity.HEAD_NAME));
        father_name.setText(getArguments().getString(ProfileDetailsActivity.FATHER_NAME));
        mobile.setText(getArguments().getString(ProfileDetailsActivity.MOBILE));
        whatsapp.setText(getArguments().getString(ProfileDetailsActivity.WHATSAPP));
        city.setText(getArguments().getString(ProfileDetailsActivity.CITY));
        state.setText(getArguments().getString(ProfileDetailsActivity.STATE));
        user_id = getArguments().getInt(ProfileDetailsActivity.USER_ID_NEW);
        head_name.setOnClickListener(v -> {
            context.startActivity(new Intent(context, ProfileDetailsActivity.class)
                    .putExtra(ProfileDetailsActivity.USER_ID_NEW, user_id));
        });
    }

}
