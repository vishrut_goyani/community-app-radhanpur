package oswal.jain.samaj.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import oswal.jain.samaj.R;
import oswal.jain.samaj.data.models.Profile_Data;
import oswal.jain.samaj.ui.activities.ProfileDetailsActivity;

public class BasicDetailsFragment extends Fragment {

    TextView date_of_birth, mobile, whatsapp, email, relation_with_vadil, education, blood_group,
            marital_status, donor, occupationType, designation, native_place, height, weight, mama_name, mother_mama, father_mama;
    LinearLayout ll8, ll9, ll10, ll11, ll12;
    int searchType;

    public static BasicDetailsFragment newInstance(String profile_data, int searchType) {
        BasicDetailsFragment fragment = new BasicDetailsFragment();

        Bundle args = new Bundle();
        args.putString(ProfileDetailsActivity.PROFILE_DATA, profile_data);
        args.putInt(ProfileDetailsActivity.SEARCH_TYPE, searchType);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basic_details, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        date_of_birth = view.findViewById(R.id.date_of_birth);
        mobile = view.findViewById(R.id.mobile);
        whatsapp = view.findViewById(R.id.whatsapp);
        email = view.findViewById(R.id.email);
        relation_with_vadil = view.findViewById(R.id.relation_with_vadil);
        education = view.findViewById(R.id.education);
        blood_group = view.findViewById(R.id.blood_group);
        marital_status = view.findViewById(R.id.marital_status);
        donor = view.findViewById(R.id.donor);
        occupationType = view.findViewById(R.id.occupationType);
        designation = view.findViewById(R.id.designation);
        native_place = view.findViewById(R.id.native_place);
        ll8 = view.findViewById(R.id.ll8);
        ll9 = view.findViewById(R.id.ll9);
        ll10 = view.findViewById(R.id.ll10);
        ll11 = view.findViewById(R.id.ll11);
        ll12 = view.findViewById(R.id.ll12);
        height = view.findViewById(R.id.height);
        weight = view.findViewById(R.id.weight);
        mama_name = view.findViewById(R.id.mama_name);
        mother_mama = view.findViewById(R.id.mother_mama);
        father_mama = view.findViewById(R.id.father_mama);

        searchType = getArguments().getInt(ProfileDetailsActivity.SEARCH_TYPE, -1);

        Gson gson = new Gson();
        String json = getArguments().getString(ProfileDetailsActivity.PROFILE_DATA);
        Profile_Data.Data obj = gson.fromJson(json, Profile_Data.Data.class);

        if (searchType == 2) {
            ll8.setVisibility(View.VISIBLE);
            ll9.setVisibility(View.VISIBLE);
            ll10.setVisibility(View.VISIBLE);
            ll11.setVisibility(View.VISIBLE);
            ll12.setVisibility(View.VISIBLE);
            height.setText(obj.getHeight());
            weight.setText(obj.getWeight());
            mama_name.setText(obj.getMama_name());
            mother_mama.setText(obj.getMother_mama());
            father_mama.setText(obj.getFather_mama());
        } else if (searchType == 3) {
            ll8.setVisibility(View.GONE);
            ll9.setVisibility(View.GONE);
            ll10.setVisibility(View.GONE);
            ll11.setVisibility(View.GONE);
            ll12.setVisibility(View.GONE);
        } else {
            ll8.setVisibility(View.GONE);
            ll9.setVisibility(View.GONE);
            ll10.setVisibility(View.GONE);
            ll11.setVisibility(View.GONE);
            ll12.setVisibility(View.GONE);
        }
        date_of_birth.setText(obj.getDate_of_birth());
        mobile.setText(obj.getMobile_no());
        whatsapp.setText(obj.getWhatsappNo());
        email.setText(obj.getEmailId());
        relation_with_vadil.setText(obj.getRelation_with_vadil());
        education.setText(obj.getEducation());
        blood_group.setText(obj.getBlood_group());
        marital_status.setText(obj.getMarital_status());
        donor.setText(obj.getDonor());
        occupationType.setText(obj.getOccupation_type());
        designation.setText(obj.getDesignation());
        native_place.setText(obj.getNative_place());
    }
}
