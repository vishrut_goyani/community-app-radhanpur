package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchAddressData {

    @SerializedName("data")
    @Expose
    Search_Data data;

    @SerializedName("message")
    String message;

    public static class Search_Data {
        @SerializedName("current_page")
        int current_page;

        @SerializedName("data")
        @Expose
        ArrayList<SearchList> data;

        @SerializedName("from")
        int from;

        @SerializedName("total")
        int total;

        @SerializedName("last_page")
        int last_page;

        public ArrayList<SearchList> getData() {
            return data;
        }

        public int getFrom() {
            return from;
        }

        public int getTotal() {
            return total;
        }

        public int getLast_page() {
            return last_page;
        }

        public static class SearchList {
            @SerializedName("id")
            int id;

            @SerializedName("name")
            String name;

            @SerializedName("family_code")
            String family_code;

            @SerializedName("family_head_id")
            int family_head_id;

            @SerializedName("address_line1")
            String address_line1;

            @SerializedName("address_line2")
            String address_line2;

            @SerializedName("state_id")
            int state_id;

            @SerializedName("city_id")
            int city_id;

            @SerializedName("pincode")
            String pincode;

            @SerializedName("family_head_detail")
            FamilyHeadDetails family_head_detail;

            @SerializedName("city")
            CityData city;

            @SerializedName("state")
            StateData state;

            public CityData getCity() {
                return city;
            }

            public StateData getState() {
                return state;
            }

            public static class CityData {
                @SerializedName("id")
                int id;

                @SerializedName("city")
                String city;

                @SerializedName("state_id")
                int state_id;

                public int getId() {
                    return id;
                }

                public String getCity() {
                    return city;
                }

                public int getState_id() {
                    return state_id;
                }
            }

            public static class StateData {
                @SerializedName("id")
                int id;

                @SerializedName("name")
                String name;

                @SerializedName("country_id")
                int country_id;

                public int getId() {
                    return id;
                }

                public String getName() {
                    return name;
                }

                public int getCountry_id() {
                    return country_id;
                }
            }

            public static class FamilyHeadDetails {
                @SerializedName("id")
                int id;

                @SerializedName("family_id")
                int family_id;

                @SerializedName("name")
                String name;

                @SerializedName("gender")
                String gender;

                @SerializedName("age")
                int age;

                @SerializedName("date_of_birth")
                String date_of_birth;

                @SerializedName("country_code")
                String country_code;

                @SerializedName("mobile_no")
                String mobile_no;

                @SerializedName("relation_with_vadil")
                String relation_with_vadil;

                @SerializedName("education")
                String education;

                @SerializedName("emailId")
                String emailId;

                @SerializedName("profile_image")
                String profile_image;

                @SerializedName("blood_group")
                String blood_group;

                @SerializedName("donor")
                String donor;

                @SerializedName("marital_status")
                String marital_status;

                @SerializedName("whatsappNo")
                String whatsappNo;

                @SerializedName("occupation_type")
                String occupation_type;

                @SerializedName("company_name")
                String company_name;

                @SerializedName("company_address")
                String company_address;

                @SerializedName("designation")
                String designation;

                @SerializedName("hobbies")
                String hobbies;

                @SerializedName("display_matrimonial")
                String display_matrimonial;

                @SerializedName("city_id")
                String city_id;

                @SerializedName("native_place")
                String native_place;

                @SerializedName("state_id")
                String state_id;

                @SerializedName("thumb")
                String thumb;

                @SerializedName("original")
                String original;

                public int getId() {
                    return id;
                }

                public int getFamily_id() {
                    return family_id;
                }

                public String getName() {
                    return name;
                }

                public String getGender() {
                    return gender;
                }

                public int getAge() {
                    return age;
                }

                public String getDate_of_birth() {
                    return date_of_birth;
                }

                public String getCountry_code() {
                    return country_code;
                }

                public String getMobile_no() {
                    return mobile_no;
                }

                public String getRelation_with_vadil() {
                    return relation_with_vadil;
                }

                public String getEducation() {
                    return education;
                }

                public String getEmailId() {
                    return emailId;
                }

                public String getProfile_image() {
                    return profile_image;
                }

                public String getBlood_group() {
                    return blood_group;
                }

                public String getDonor() {
                    return donor;
                }

                public String getMarital_status() {
                    return marital_status;
                }

                public String getWhatsappNo() {
                    return whatsappNo;
                }

                public String getOccupation_type() {
                    return occupation_type;
                }

                public String getCompany_name() {
                    return company_name;
                }

                public String getCompany_address() {
                    return company_address;
                }

                public String getDesignation() {
                    return designation;
                }

                public String getHobbies() {
                    return hobbies;
                }

                public String getDisplay_matrimonial() {
                    return display_matrimonial;
                }

                public String getCity_id() {
                    return city_id;
                }

                public String getNative_place() {
                    return native_place;
                }

                public String getState_id() {
                    return state_id;
                }

                public String getThumb() {
                    return thumb;
                }

                public String getOriginal() {
                    return original;
                }
            }

            public int getId() {
                return id;
            }

            public String getFamily_code() {
                return family_code;
            }

            public int getFamily_head_id() {
                return family_head_id;
            }

            public String getAddress_line1() {
                return address_line1;
            }

            public String getAddress_line2() {
                return address_line2;
            }

            public int getState_id() {
                return state_id;
            }

            public int getCity_id() {
                return city_id;
            }

            public String getPincode() {
                return pincode;
            }

            public FamilyHeadDetails getFamily_head_details() {
                return family_head_detail;
            }

            public String getName() {
                return name;
            }
        }

        public int getCurrent_page() {
            return current_page;
        }

        public ArrayList<SearchList> getProfilesList() {
            return data;
        }
    }

    public Search_Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
