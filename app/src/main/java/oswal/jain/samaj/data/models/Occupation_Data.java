package oswal.jain.samaj.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Occupation_Data {
    String status, message;
    ArrayList<Occupation_Details> data;

    @Data
    public static class Occupation_Details {
        @SerializedName("id")
        int id;

        @SerializedName("occupation_name")
        String occupation_name;
    }

}
