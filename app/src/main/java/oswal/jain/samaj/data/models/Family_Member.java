package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Family_Member {

    @SerializedName("data")
    @Expose
    Family_User_Data data;

    @SerializedName("message")
    String message;

    public static class Family_User_Data {
        @SerializedName("user")
        @Expose
        Family_User_Details user;

        public static class Family_User_Details {
            @SerializedName("family_id")
            int family_id;

            @SerializedName("name")
            String name;

            @SerializedName("emailId")
            String emailId;

            @SerializedName("mobile_no")
            String mobile_no;

            @SerializedName("gender")
            String gender;

            @SerializedName("relation_with_vadil")
            String relation_with_vadil;

            @SerializedName("updated_at")
            String updated_at;

            @SerializedName("created_at")
            String created_at;

            @SerializedName("id")
            String id;

            @SerializedName("thumb")
            String thumb;

            @SerializedName("original")
            String original;

            public int getFamily_id() {
                return family_id;
            }

            public String getName() {
                return name;
            }

            public String getEmailId() {
                return emailId;
            }

            public String getMobile_no() {
                return mobile_no;
            }

            public String getGender() {
                return gender;
            }

            public String getRelation_with_vadil() {
                return relation_with_vadil;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public String getId() {
                return id;
            }

            public String getThumb() {
                return thumb;
            }

            public String getOriginal() {
                return original;
            }
        }

        public Family_User_Details getUser() {
            return user;
        }
    }

    public Family_User_Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
