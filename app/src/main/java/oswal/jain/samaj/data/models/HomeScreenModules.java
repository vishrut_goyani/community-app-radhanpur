package oswal.jain.samaj.data.models;

public class HomeScreenModules {

    int image;
    String nameText;

    public HomeScreenModules() {
    }

    public HomeScreenModules(int image, String nameText) {
        this.image = image;
        this.nameText = nameText;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNameText() {
        return nameText;
    }

    public void setNameText(String nameText) {
        this.nameText = nameText;
    }
}
