package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login_Data {

    @SerializedName("data")
    User_Data data;

    @SerializedName("message")
    String message;

    public static class User_Data {
        @SerializedName("token")
        String token;

        @SerializedName("user")
        @Expose
        User_Details user_details;

        @SerializedName("is_filled")
        int is_filled;

        public static class User_Details {
            @SerializedName("id")
            int id;

            @SerializedName("family_id")
            String family_id;

            @SerializedName("name")
            String name;

            @SerializedName("gender")
            String gender;

            @SerializedName("age")
            String age;

            @SerializedName("date_of_birth")
            String date_of_birth;

            @SerializedName("country_code")
            String country_code;

            @SerializedName("mobile_no")
            String mobile_no;

            @SerializedName("country_code2")
            String country_code2;

            @SerializedName("mobile_no2")
            String mobile_no2;

            @SerializedName("relation_with_vadil")
            String relation_with_vadil;

            @SerializedName("education")
            String education;

            @SerializedName("emailId")
            String emailId;

            @SerializedName("profile_image")
            String profile_image;

            @SerializedName("blood_group")
            String blood_group;

            @SerializedName("donor")
            boolean donor;

            @SerializedName("marital_status")
            String marital_status;

            @SerializedName("whatsappNo")
            String whatsappNo;

            @SerializedName("occupation_type")
            String occupation_type;

            @SerializedName("company_name")
            String company_name;

            @SerializedName("company_address")
            String company_address;

            @SerializedName("designation")
            String designation;

            @SerializedName("height")
            String height;

            @SerializedName("weight")
            String weight;

            @SerializedName("mama_name")
            String mama_name;

            @SerializedName("mother_mama")
            String mother_mama;

            @SerializedName("father_mama")
            String father_mama;

            @SerializedName("no_of_unmarried_brother")
            String no_of_unmarried_brother;

            @SerializedName("no_of_unmarried_sister")
            String no_of_unmarried_sister;

            @SerializedName("father_name")
            String father_name;

            @SerializedName("mother_name")
            String mother_name;

            @SerializedName("total_brother")
            String total_brother;

            @SerializedName("total_sister")
            String total_sister;

            @SerializedName("mosal_name")
            String mosal_name;

            @SerializedName("kutum_motu_name")
            String kutum_motu_name;

            @SerializedName("father_profession")
            String father_profession;

            @SerializedName("hobbies")
            String hobbies;

            @SerializedName("display_matrimonial")
            String display_matrimonial;

            @SerializedName("anniversary_date")
            String anniversary_date;

            @SerializedName("sector")
            String sector;

            @SerializedName("otp")
            String otp;

//            @SerializedName("is_otp_verify")
//            int is_otp_verify;

            @SerializedName("device_token")
            String device_token;

            @SerializedName("device_type")
            String device_type;

            @SerializedName("created_at")
            String created_at;

            @SerializedName("updated_at")
            String updated_at;

            @SerializedName("deleted_at")
            String deleted_at;

            @SerializedName("is_active")
            int is_active;

            @SerializedName("thumb")
            String thumb;

            @SerializedName("original")
            String original;

            public User_Details() {
            }

            public User_Details(int id, String family_id, String name, String gender, String age, String date_of_birth, String country_code, String mobile_no, String country_code2,
                                String mobile_no2, String relation_with_vadil, String education, String emailId, String profile_image, String blood_group, boolean donor,
                                String marital_status, String whatsappNo, String occupation_type, String company_name, String company_address, String designation,
                                String no_of_unmarried_brother, String no_of_unmarried_sister, String father_name, String mother_name,
                                String height, String weight, String mama_name, String mother_mama, String father_mama,
                                String total_brother,
                                String total_sister, String mosal_name, String kutum_motu_name, String father_profession, String hobbies, String display_matrimonial, String anniversary_date,
                                String sector, String otp/*, int is_otp_verify*/, String device_token, String device_type, int is_active, String thumb) {
                this.id = id;
                this.family_id = family_id;
                this.name = name;
                this.gender = gender;
                this.age = age;
                this.date_of_birth = date_of_birth;
                this.country_code = country_code;
                this.mobile_no = mobile_no;
                this.country_code2 = country_code2;
                this.mobile_no2 = mobile_no2;
                this.relation_with_vadil = relation_with_vadil;
                this.education = education;
                this.emailId = emailId;
                this.profile_image = profile_image;
                this.blood_group = blood_group;
                this.donor = donor;
                this.marital_status = marital_status;
                this.whatsappNo = whatsappNo;
                this.occupation_type = occupation_type;
                this.company_name = company_name;
                this.company_address = company_address;
                this.designation = designation;
                this.no_of_unmarried_brother = no_of_unmarried_brother;
                this.no_of_unmarried_sister = no_of_unmarried_sister;
                this.father_name = father_name;
                this.mother_name = mother_name;
                this.height = height;
                this.weight = weight;
                this.mama_name = mama_name;
                this.mother_mama = mother_mama;
                this.father_mama = father_mama;
                this.total_brother = total_brother;
                this.total_sister = total_sister;
                this.mosal_name = mosal_name;
                this.kutum_motu_name = kutum_motu_name;
                this.father_profession = father_profession;
                this.hobbies = hobbies;
                this.display_matrimonial = display_matrimonial;
                this.anniversary_date = anniversary_date;
                this.sector = sector;
                this.otp = otp;
//                this.is_otp_verify = is_otp_verify;
                this.device_token = device_token;
                this.device_type = device_type;
                this.is_active = is_active;
                this.thumb = thumb;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getFamily_id() {
                return family_id;
            }

            public void setFamily_id(String family_id) {
                this.family_id = family_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getDate_of_birth() {
                return date_of_birth;
            }

            public void setDate_of_birth(String date_of_birth) {
                this.date_of_birth = date_of_birth;
            }

            public String getCountry_code() {
                return country_code;
            }

            public void setCountry_code(String country_code) {
                this.country_code = country_code;
            }

            public String getMobile_no() {
                return mobile_no;
            }

            public void setMobile_no(String mobile_no) {
                this.mobile_no = mobile_no;
            }

            public String getCountry_code2() {
                return country_code2;
            }

            public void setCountry_code2(String country_code2) {
                this.country_code2 = country_code2;
            }

            public String getMobile_no2() {
                return mobile_no2;
            }

            public void setMobile_no2(String mobile_no2) {
                this.mobile_no2 = mobile_no2;
            }

            public String getRelation_with_vadil() {
                return relation_with_vadil;
            }

            public void setRelation_with_vadil(String relation_with_vadil) {
                this.relation_with_vadil = relation_with_vadil;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getProfile_image() {
                return profile_image;
            }

            public void setProfile_image(String profile_image) {
                this.profile_image = profile_image;
            }

            public String getBlood_group() {
                return blood_group;
            }

            public void setBlood_group(String blood_group) {
                this.blood_group = blood_group;
            }

            public boolean getDonor() {
                return donor;
            }

            public void setDonor(boolean donor) {
                this.donor = donor;
            }

            public String getMarital_status() {
                return marital_status;
            }

            public void setMarital_status(String marital_status) {
                this.marital_status = marital_status;
            }

            public String getWhatsappNo() {
                return whatsappNo;
            }

            public void setWhatsappNo(String whatsappNo) {
                this.whatsappNo = whatsappNo;
            }

            public String getOccupation_type() {
                return occupation_type;
            }

            public void setOccupation_type(String occupation_type) {
                this.occupation_type = occupation_type;
            }

            public String getCompany_name() {
                return company_name;
            }

            public void setCompany_name(String company_name) {
                this.company_name = company_name;
            }

            public String getCompany_address() {
                return company_address;
            }

            public void setCompany_address(String company_address) {
                this.company_address = company_address;
            }

            public String getDesignation() {
                return designation;
            }

            public void setDesignation(String designation) {
                this.designation = designation;
            }

            public String getNo_of_unmarried_brother() {
                return no_of_unmarried_brother;
            }

            public void setNo_of_unmarried_brother(String no_of_unmarried_brother) {
                this.no_of_unmarried_brother = no_of_unmarried_brother;
            }

            public String getNo_of_unmarried_sister() {
                return no_of_unmarried_sister;
            }

            public void setNo_of_unmarried_sister(String no_of_unmarried_sister) {
                this.no_of_unmarried_sister = no_of_unmarried_sister;
            }

            public String getFather_name() {
                return father_name;
            }

            public void setFather_name(String father_name) {
                this.father_name = father_name;
            }

            public String getMother_name() {
                return mother_name;
            }

            public void setMother_name(String mother_name) {
                this.mother_name = mother_name;
            }

            public String getHeight() {
                return height;
            }

            public void setHeight(String height) {
                this.height = height;
            }

            public String getWeight() {
                return weight;
            }

            public String getMama_name() {
                return mama_name;
            }

            public String getMother_mama() {
                return mother_mama;
            }

            public String getFather_mama() {
                return father_mama;
            }

            public void setWeight(String weight) {
                this.weight = weight;
            }

            public String getTotal_brother() {
                return total_brother;
            }

            public void setTotal_brother(String total_brother) {
                this.total_brother = total_brother;
            }

            public String getTotal_sister() {
                return total_sister;
            }

            public void setTotal_sister(String total_sister) {
                this.total_sister = total_sister;
            }

            public String getMosal_name() {
                return mosal_name;
            }

            public void setMosal_name(String mosal_name) {
                this.mosal_name = mosal_name;
            }

            public String getKutum_motu_name() {
                return kutum_motu_name;
            }

            public void setKutum_motu_name(String kutum_motu_name) {
                this.kutum_motu_name = kutum_motu_name;
            }

            public String getFather_profession() {
                return father_profession;
            }

            public void setFather_profession(String father_profession) {
                this.father_profession = father_profession;
            }

            public String getHobbies() {
                return hobbies;
            }

            public void setHobbies(String hobbies) {
                this.hobbies = hobbies;
            }

            public String getDisplay_matrimonial() {
                return display_matrimonial;
            }

            public void setDisplay_matrimonial(String display_matrimonial) {
                this.display_matrimonial = display_matrimonial;
            }

            public String getAnniversary_date() {
                return anniversary_date;
            }

            public void setAnniversary_date(String anniversary_date) {
                this.anniversary_date = anniversary_date;
            }

            public String getSector() {
                return sector;
            }

            public void setSector(String sector) {
                this.sector = sector;
            }

            public String getOtp() {
                return otp;
            }

            public void setOtp(String otp) {
                this.otp = otp;
            }

//            public int getIs_otp_verify() {
//                return is_otp_verify;
//            }

//            public void setIs_otp_verify(int is_otp_verify) {
//                this.is_otp_verify = is_otp_verify;
//            }

            public String getDevice_token() {
                return device_token;
            }

            public void setDevice_token(String device_token) {
                this.device_token = device_token;
            }

            public String getDevice_type() {
                return device_type;
            }

            public void setDevice_type(String device_type) {
                this.device_type = device_type;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(String deleted_at) {
                this.deleted_at = deleted_at;
            }

            public int getIs_active() {
                return is_active;
            }

            public void setIs_active(int is_active) {
                this.is_active = is_active;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }

            public String getOriginal() {
                return original;
            }

            public void setOriginal(String original) {
                this.original = original;
            }
        }

        public String getToken() {
            return token;
        }

        public User_Details getUser_details() {
            return user_details;
        }

        public int getIs_filled() {
            return is_filled;
        }

        public void setIs_filled(int is_filled) {
            this.is_filled = is_filled;
        }
    }

    public User_Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
