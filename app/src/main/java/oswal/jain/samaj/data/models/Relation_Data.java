package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Relation_Data {
    @SerializedName("data")
    @Expose
    ArrayList<Relation_Details> data;

    @SerializedName("message")
    String message;

    public static class Relation_Details {
        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public ArrayList<Relation_Details> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
