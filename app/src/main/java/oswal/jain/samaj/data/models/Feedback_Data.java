package oswal.jain.samaj.data.models;

import com.google.gson.annotations.SerializedName;

public class Feedback_Data {
    @SerializedName("message")
    String message;

    public String getMessage() {
        return message;
    }
}
