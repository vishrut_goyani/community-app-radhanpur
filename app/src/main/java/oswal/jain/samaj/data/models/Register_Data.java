package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Register_Data {

    @SerializedName("token")
    String token;

    @SerializedName("user")
    @Expose
    User_Details user_details;

    @SerializedName("message")
    String message;

    public static class User_Details {
        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        @SerializedName("email")
        String email;

        @SerializedName("mobile")
        long mobile;

        @SerializedName("gender")
        String gender;

        @SerializedName("otp")
        int otp;

        @SerializedName("avatar")
        String avatar;

        @SerializedName("address")
        String address;

        @SerializedName("city_id")
        String city_id;

        @SerializedName("user_type")
        int user_type;

        @SerializedName("status")
        int status;

        @SerializedName("device_token")
        String device_token;

        @SerializedName("device_type")
        String device_type;

        @SerializedName("lat")
        String lat;

        @SerializedName("lng")
        String lng;

        @SerializedName("email_verified_at")
        String email_verified_at;

        @SerializedName("created_at")
        String created_at;

        @SerializedName("updated_at")
        String updated_at;

        @SerializedName("deleted_at")
        String deleted_at;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public long getMobile() {
            return mobile;
        }

        public String getGender() {
            return gender;
        }

        public int getOtp() {
            return otp;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getAddress() {
            return address;
        }

        public String getCity_id() {
            return city_id;
        }

        public int getUser_type() {
            return user_type;
        }

        public int getStatus() {
            return status;
        }

        public String getDevice_token() {
            return device_token;
        }

        public String getDevice_type() {
            return device_type;
        }

        public String getLat() {
            return lat;
        }

        public String getLng() {
            return lng;
        }

        public String getEmail_verified_at() {
            return email_verified_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public String getDeleted_at() {
            return deleted_at;
        }
    }

    public String getToken() {
        return token;
    }

    public User_Details getUser_details() {
        return user_details;
    }

    public String getMessage() {
        return message;
    }
}
