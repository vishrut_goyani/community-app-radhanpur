package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class News_Data {

    @SerializedName("data")
    Data data;

    @SerializedName("message")
    String message;

    public static class Data {
        @SerializedName("current_page")
        int current_page;

        @SerializedName("data")
        @Expose
        ArrayList<News_Details> news_details;

        public static class News_Details {
            @SerializedName("id")
            int id;

            @SerializedName("category")
            String category;

            @SerializedName("news_image")
            String news_image;

            @SerializedName("news_title")
            String news_title;

            @SerializedName("news_message")
            String news_message;

            @SerializedName("status")
            int status;

            @SerializedName("created_at")
            String created_at;

            @SerializedName("updated_at")
            String updated_at;

            @SerializedName("deleted_at")
            String deleted_at;

            public int getId() {
                return id;
            }

            public String getCategory() {
                return category;
            }

            public String getNews_image() {
                return news_image;
            }

            public String getNews_title() {
                return news_title;
            }

            public String getNews_message() {
                return news_message;
            }

            public int getStatus() {
                return status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public String getDeleted_at() {
                return deleted_at;
            }
        }

        public int getCurrent_page() {
            return current_page;
        }

        public ArrayList<News_Details> getNews_details() {
            return news_details;
        }
    }

    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
