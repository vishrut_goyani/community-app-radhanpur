package oswal.jain.samaj.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Family_Data {

    @SerializedName("data")
    Data data;

    @SerializedName("message")
    String message;

    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public static class Data {
        @SerializedName("id")
        int id;

        @SerializedName("family_code")
        String family_code;

        @SerializedName("family_head_id")
        int family_head_id;

        @SerializedName("address_line1")
        String address_line1;

        @SerializedName("address_line2")
        String address_line2;

        @SerializedName("state_id")
        int state_id;

        @SerializedName("city_id")
        int city_id;

        @SerializedName("pincode")
        String pincode;

        @SerializedName("native_place")
        String native_place;

        @SerializedName("city")
        City city;

        @SerializedName("state")
        State state;

        public int getId() {
            return id;
        }

        public String getFamily_code() {
            return family_code;
        }

        public int getFamily_head_id() {
            return family_head_id;
        }

        public String getAddress_line1() {
            return address_line1;
        }

        public String getAddress_line2() {
            return address_line2;
        }

        public int getState_id() {
            return state_id;
        }

        public int getCity_id() {
            return city_id;
        }

        public String getPincode() {
            return pincode;
        }

        public String getNative_place() {
            return native_place;
        }

        public City getCity() {
            return city;
        }

        public State getState() {
            return state;
        }

        public static class City {
            @SerializedName("id")
            int id;

            @SerializedName("city")
            String city;

            public int getId() {
                return id;
            }

            public String getCity() {
                return city;
            }
        }

        public static class State {
            @SerializedName("id")
            int id;

            @SerializedName("name")
            String name;

            public int getId() {
                return id;
            }

            public String getName() {
                return name;
            }
        }

        @SerializedName("family_member")
        ArrayList<Family_Details> family_member;

        public ArrayList<Family_Details> getFamily_members() {
            return family_member;
        }

        public static class Family_Details {
            @SerializedName("id")
            int id;

            @SerializedName("family_id")
            int family_id;

            @SerializedName("name")
            String name;

            @SerializedName("gender")
            String gender;

            @SerializedName("age")
            int age;

            @SerializedName("date_of_birth")
            String date_of_birth;

            @SerializedName("mobile_no")
            String mobile_no;

            @SerializedName("relation_with_vadil")
            String relation_with_vadil;

            @SerializedName("education")
            String education;

            @SerializedName("emailId")
            String emailId;

            @SerializedName("city_id")
            int city_id;

            @SerializedName("state_id")
            int state_id;

            @SerializedName("blood_group")
            String blood_group;

            @SerializedName("donor")
            String donor;

            @SerializedName("marital_status")
            String marital_status;

            @SerializedName("whatsappNo")
            String whatsappNo;

            @SerializedName("display_matrimonial")
            String display_matrimonial;

            @SerializedName("occupation_type")
            String occupation_type;

            @SerializedName("designation")
            String designation;

            @SerializedName("thumb")
            String thumb;

            public int getId() {
                return id;
            }

            public int getFamily_id() {
                return family_id;
            }

            public String getName() {
                return name;
            }

            public String getGender() {
                return gender;
            }

            public int getAge() {
                return age;
            }

            public String getDate_of_birth() {
                return date_of_birth;
            }

            public String getMobile_no() {
                return mobile_no;
            }

            public String getRelation_with_vadil() {
                return relation_with_vadil;
            }

            public String getEducation() {
                return education;
            }

            public String getEmailId() {
                return emailId;
            }

            public int getCity_id() {
                return city_id;
            }

            public int getState_id() {
                return state_id;
            }

            public String getBlood_group() {
                return blood_group;
            }

            public String getDonor() {
                return donor;
            }

            public String getMarital_status() {
                return marital_status;
            }

            public String getWhatsappNo() {
                return whatsappNo;
            }

            public String getDisplay_matrimonial() {
                return display_matrimonial;
            }

            public String getOccupation_type() {
                return occupation_type;
            }

            public String getDesignation() {
                return designation;
            }

            public String getThumb() {
                return thumb;
            }
        }
    }
}
