package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Password {
    @SerializedName("data")
    @Expose
    Password_Data data;

    public static class Password_Data {
        @SerializedName("msg")
        String msg;

        @SerializedName("message")
        String message;

        public String getMsg() {
            return msg;
        }

        public String getMessage() {
            return message;
        }
    }

    public Password_Data getData() {
        return data;
    }
}
