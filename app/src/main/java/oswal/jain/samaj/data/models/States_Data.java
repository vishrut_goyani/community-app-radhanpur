package oswal.jain.samaj.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class States_Data {
    @SerializedName("data")
    ArrayList<State_Details> data;

    @SerializedName("message")
    String message;

    public static class State_Details {
        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        @SerializedName("country_id")
        int country_id;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getCountry_id() {
            return country_id;
        }
    }

    public ArrayList<State_Details> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
