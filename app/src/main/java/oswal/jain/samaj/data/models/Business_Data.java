package oswal.jain.samaj.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class Business_Data {
    @SerializedName("status")
    boolean status;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    Business_Details data;

    @lombok.Data
    public static class Business_Details {
        @SerializedName("current_page")
        int current_page;

        @SerializedName("last_page")
        int last_page;

        @SerializedName("data")
        ArrayList<Data> business_list;

        @lombok.Data
        public static class Data {
            @SerializedName("id")
            int id;

            @SerializedName("name")
            String name;

            @SerializedName("gender")
            String gender;

            @SerializedName("age")
            String age;

            @SerializedName("mobile_no")
            String mobile_no;

            @SerializedName("whatsappNo")
            String whatsappNo;

            @SerializedName("relation_with_vadil")
            String relation_with_vadil;

            @SerializedName("native_place")
            String native_place;

            @SerializedName("date_of_birth")
            String date_of_birth;

            @SerializedName("emailId")
            String emailId;

            @SerializedName("city")
            City_Details city;

            @SerializedName("designation")
            String designation;

            @SerializedName("occupation_type")
            String occupation_type;

            @SerializedName("state")
            State_Details state;

            @SerializedName("thumb")
            String thumb;

            @lombok.Data
            public static class City_Details {
                @SerializedName("id")
                int id;

                @SerializedName("city")
                String city;
            }

            @lombok.Data
            public static class State_Details {
                @SerializedName("id")
                int id;

                @SerializedName("name")
                String name;
            }
        }
    }
}
