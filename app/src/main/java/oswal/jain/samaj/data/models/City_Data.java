package oswal.jain.samaj.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class City_Data {

    @SerializedName("data")
    ArrayList<City_Details> data;

    @SerializedName("message")
    String message;

    public static class City_Details {
        @SerializedName("id")
        int id;

        @SerializedName("city")
        String city;

        @SerializedName("country_id")
        int country_id;

        @SerializedName("created_at")
        String created_at;

        @SerializedName("updated_at")
        String updated_at;

        @SerializedName("deleted_at")
        String deleted_at;

        public int getId() {
            return id;
        }

        public String getCity() {
            return city;
        }

        public int getCountry_id() {
            return country_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public String getDeleted_at() {
            return deleted_at;
        }
    }

    public ArrayList<City_Details> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
