package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Ads_Data {
    @SerializedName("status")
    boolean status;

    @SerializedName("message")
    String message;
    @SerializedName("data")
    @Expose
    ArrayList<Ads_Details> ads_details;

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Ads_Details> getAds_details() {
        return ads_details;
    }

    public static class Ads_Details {
        @SerializedName("id")
        int id;

        @SerializedName("ad_image")
        String ad_image;

        @SerializedName("whats_app")
        String whats_app;

        @SerializedName("redirect_url")
        String redirect_url;

        @SerializedName("status")
        String status;

        @SerializedName("created_at")
        String created_at;

        @SerializedName("updated_at")
        String updated_at;

        @SerializedName("deleted_at")
        String deleted_at;

        public int getId() {
            return id;
        }

        public String getAd_image() {
            return ad_image;
        }

        public String getWhats_app() {
            return whats_app;
        }

        public String getRedirect_url() {
            return redirect_url;
        }

        public String getStatus() {
            return status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public String getDeleted_at() {
            return deleted_at;
        }
    }
}
