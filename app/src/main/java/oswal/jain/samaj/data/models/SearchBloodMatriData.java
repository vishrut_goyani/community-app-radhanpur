package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchBloodMatriData {

    @SerializedName("data")
    @Expose
    Search_Data data;

    @SerializedName("message")
    String message;

    public static class Search_Data {
        @SerializedName("current_page")
        int current_page;

        @SerializedName("data")
        @Expose
        ArrayList<SearchList> data;

        @SerializedName("from")
        int from;

        @SerializedName("total")
        int total;

        @SerializedName("last_page")
        int last_page;

        public ArrayList<SearchList> getData() {
            return data;
        }

        public int getFrom() {
            return from;
        }

        public int getTotal() {
            return total;
        }

        public int getLast_page() {
            return last_page;
        }

        public static class SearchList {
            @SerializedName("id")
            int id;

            @SerializedName("family_id")
            int family_id;

            @SerializedName("name")
            String name;

            @SerializedName("gender")
            String gender;

            @SerializedName("age")
            int age;

            @SerializedName("date_of_birth")
            String date_of_birth;

            @SerializedName("country_code")
            String country_code;

            @SerializedName("country_code2")
            String country_code2;

            @SerializedName("mobile_no")
            String mobile_no;

            @SerializedName("mobile_no2")
            String mobile_no2;

            @SerializedName("relation_with_vadil")
            String relation_with_vadil;

            @SerializedName("education")
            String education;

            @SerializedName("emailId")
            String emailId;

            @SerializedName("profile_image")
            String profile_image;

            @SerializedName("blood_group")
            String blood_group;

            @SerializedName("donor")
            String donor;

            @SerializedName("marital_status")
            String marital_status;

            @SerializedName("height")
            String height;

            @SerializedName("weight")
            String weight;

            @SerializedName("whatsappNo")
            String whatsappNo;

            @SerializedName("occupation_type")
            String occupation_type;

            @SerializedName("company_name")
            String company_name;

            @SerializedName("company_address")
            String company_address;

            @SerializedName("designation")
            String designation;

            @SerializedName("hobbies")
            String hobbies;

            @SerializedName("display_matrimonial")
            String display_matrimonial;

            @SerializedName("state_id")
            int state_id;

            @SerializedName("city_id")
            int city_id;

            @SerializedName("is_active")
            int is_active;

            @SerializedName("thumb")
            String thumb;

            @SerializedName("original")
            String original;

            @SerializedName("pincode")
            String pincode;

            @SerializedName("city")
            CityData city;

            @SerializedName("state")
            StateData state;

            public int getId() {
                return id;
            }

            public int getFamily_id() {
                return family_id;
            }

            public String getName() {
                return name;
            }

            public String getGender() {
                return gender;
            }

            public int getAge() {
                return age;
            }

            public String getDate_of_birth() {
                return date_of_birth;
            }

            public String getCountry_code() {
                return country_code;
            }

            public String getCountry_code2() {
                return country_code2;
            }

            public String getMobile_no() {
                return mobile_no;
            }

            public String getMobile_no2() {
                return mobile_no2;
            }

            public String getRelation_with_vadil() {
                return relation_with_vadil;
            }

            public String getEducation() {
                return education;
            }

            public String getEmailId() {
                return emailId;
            }

            public String getProfile_image() {
                return profile_image;
            }

            public String getBlood_group() {
                return blood_group;
            }

            public String getDonor() {
                return donor;
            }

            public String getMarital_status() {
                return marital_status;
            }

            public String getHeight() {
                return height;
            }

            public String getWeight() {
                return weight;
            }

            public String getWhatsappNo() {
                return whatsappNo;
            }

            public String getOccupation_type() {
                return occupation_type;
            }

            public String getCompany_name() {
                return company_name;
            }

            public String getCompany_address() {
                return company_address;
            }

            public String getDesignation() {
                return designation;
            }

            public String getHobbies() {
                return hobbies;
            }

            public String getDisplay_matrimonial() {
                return display_matrimonial;
            }

            public int getState_id() {
                return state_id;
            }

            public int getCity_id() {
                return city_id;
            }

            public int getIs_active() {
                return is_active;
            }

            public String getThumb() {
                return thumb;
            }

            public String getOriginal() {
                return original;
            }

            public String getPincode() {
                return pincode;
            }

            public CityData getCity() {
                return city;
            }

            public StateData getState() {
                return state;
            }

            public static class CityData {
                @SerializedName("id")
                int id;

                @SerializedName("city")
                String city;

                @SerializedName("state_id")
                int state_id;

                public int getId() {
                    return id;
                }

                public String getCity() {
                    return city;
                }

                public int getState_id() {
                    return state_id;
                }
            }

            public static class StateData {
                @SerializedName("id")
                int id;

                @SerializedName("name")
                String name;

                @SerializedName("country_id")
                int country_id;

                public int getId() {
                    return id;
                }

                public String getName() {
                    return name;
                }

                public int getCountry_id() {
                    return country_id;
                }
            }

        }

        public int getCurrent_page() {
            return current_page;
        }

        public ArrayList<SearchList> getProfilesList() {
            return data;
        }

    }

    public Search_Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
