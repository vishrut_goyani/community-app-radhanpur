package oswal.jain.samaj.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile_Setup {
    @SerializedName("data")
    @Expose
    User_Data data;

    @SerializedName("message")
    String message;

    public User_Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public static class User_Data {
        @SerializedName("user")
        @Expose
        User_Details user_details;

        @SerializedName("is_filled")
        int is_filled;

        public int getIs_filled() {
            return is_filled;
        }

        public User_Details getUser_details() {
            return user_details;
        }

        public static class User_Details {
            @SerializedName("id")
            int id;

            @SerializedName("family_id")
            int family_id;

            @SerializedName("name")
            String name;

            @SerializedName("gender")
            String gender;

            @SerializedName("age")
            String age;

            @SerializedName("date_of_birth")
            String date_of_birth;

            @SerializedName("country_code")
            String country_code;

            @SerializedName("mobile_no")
            String mobile_no;

            @SerializedName("country_code2")
            String country_code2;

            @SerializedName("mobile_no2")
            String mobile_no2;

            @SerializedName("relation_with_vadil")
            String relation_with_vadil;

            @SerializedName("education")
            String education;

            @SerializedName("emailId")
            String emailId;

            @SerializedName("profile_image")
            String profile_image;

            @SerializedName("blood_group")
            String blood_group;

            @SerializedName("donor")
            boolean donor;

            @SerializedName("marital_status")
            String marital_status;

            @SerializedName("whatsappNo")
            String whatsappNo;

            @SerializedName("occupation_type")
            String occupation_type;

            @SerializedName("company_name")
            String company_name;

            @SerializedName("company_address")
            String company_address;

            @SerializedName("designation")
            String designation;

            @SerializedName("no_of_unmarried_brother")
            String no_of_unmarried_brother;

            @SerializedName("no_of_unmarried_sister")
            String no_of_unmarried_sister;

            @SerializedName("father_name")
            String father_name;

            @SerializedName("mother_name")
            String mother_name;

            @SerializedName("height")
            String height;

            @SerializedName("weight")
            String weight;

            @SerializedName("mama_name")
            String mama_name;

            @SerializedName("mother_mama")
            String mother_mama;

            @SerializedName("father_mama")
            String father_mama;

            @SerializedName("total_brother")
            String total_brother;

            @SerializedName("total_sister")
            String total_sister;

            @SerializedName("mosal_name")
            String mosal_name;

            @SerializedName("kutum_motu_name")
            String kutum_motu_name;

            @SerializedName("father_profession")
            String father_profession;

            @SerializedName("hobbies")
            String hobbies;

            @SerializedName("display_matrimonial")
            String display_matrimonial;

            @SerializedName("anniversary_date")
            String anniversary_date;

            @SerializedName("sector")
            String sector;

            @SerializedName("otp")
            String otp;

//            @SerializedName("is_otp_verify")
//            int is_otp_verify;

            @SerializedName("device_token")
            String device_token;

            @SerializedName("device_type")
            String device_type;

            @SerializedName("created_at")
            String created_at;

            @SerializedName("updated_at")
            String updated_at;

            @SerializedName("deleted_at")
            String deleted_at;

            @SerializedName("is_active")
            int is_active;

            @SerializedName("thumb")
            String thumb;

            @SerializedName("original")
            String original;

            public int getId() {
                return id;
            }

            public int getFamily_id() {
                return family_id;
            }

            public String getName() {
                return name;
            }

            public String getGender() {
                return gender;
            }

            public String getAge() {
                return age;
            }

            public String getDate_of_birth() {
                return date_of_birth;
            }

            public String getCountry_code() {
                return country_code;
            }

            public String getMobile_no() {
                return mobile_no;
            }

            public String getCountry_code2() {
                return country_code2;
            }

            public String getMobile_no2() {
                return mobile_no2;
            }

            public String getRelation_with_vadil() {
                return relation_with_vadil;
            }

            public String getEducation() {
                return education;
            }

            public String getEmailId() {
                return emailId;
            }

            public String getProfile_image() {
                return profile_image;
            }

            public String getBlood_group() {
                return blood_group;
            }

            public boolean isDonor() {
                return donor;
            }

            public String getMarital_status() {
                return marital_status;
            }

            public String getWhatsappNo() {
                return whatsappNo;
            }

            public String getOccupation_type() {
                return occupation_type;
            }

            public String getCompany_name() {
                return company_name;
            }

            public String getCompany_address() {
                return company_address;
            }

            public String getDesignation() {
                return designation;
            }

            public String getNo_of_unmarried_brother() {
                return no_of_unmarried_brother;
            }

            public String getNo_of_unmarried_sister() {
                return no_of_unmarried_sister;
            }

            public String getFather_name() {
                return father_name;
            }

            public String getMother_name() {
                return mother_name;
            }

            public String getHeight() {
                return height;
            }

            public String getMama_name() {
                return mama_name;
            }

            public String getMother_mama() {
                return mother_mama;
            }

            public String getFather_mama() {
                return father_mama;
            }

            public String getWeight() {
                return weight;
            }

            public String getTotal_brother() {
                return total_brother;
            }

            public String getTotal_sister() {
                return total_sister;
            }

            public String getMosal_name() {
                return mosal_name;
            }

            public String getKutum_motu_name() {
                return kutum_motu_name;
            }

            public String getFather_profession() {
                return father_profession;
            }

            public String getHobbies() {
                return hobbies;
            }

            public String getDisplay_matrimonial() {
                return display_matrimonial;
            }

            public String getAnniversary_date() {
                return anniversary_date;
            }

            public String getSector() {
                return sector;
            }

            public String getOtp() {
                return otp;
            }

//            public int getIs_otp_verify() {
//                return is_otp_verify;
//            }

            public String getDevice_token() {
                return device_token;
            }

            public String getDevice_type() {
                return device_type;
            }

            public String getCreated_at() {
                return created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public String getDeleted_at() {
                return deleted_at;
            }

            public int getIs_active() {
                return is_active;
            }

            public String getThumb() {
                return thumb;
            }

            public String getOriginal() {
                return original;
            }
        }
    }
}
