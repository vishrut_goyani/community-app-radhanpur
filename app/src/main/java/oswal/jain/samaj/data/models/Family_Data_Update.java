package oswal.jain.samaj.data.models;

import com.google.gson.annotations.SerializedName;

public class Family_Data_Update {
    @SerializedName("status")
    boolean status;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    Family_Details_Update data;

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Family_Details_Update getData() {
        return data;
    }

    public static class Family_Details_Update {
        @SerializedName("id")
        int id;

        @SerializedName("family_code")
        String family_code;

        @SerializedName("family_head_id")
        int family_head_id;

        @SerializedName("address_line1")
        String address_line1;

        @SerializedName("address_line2")
        String address_line2;

        @SerializedName("state_id")
        int state_id;

        @SerializedName("city_id")
        int city_id;

        @SerializedName("city")
        City_Details city;

        @SerializedName("state")
        State_Details state;

        @SerializedName("pincode")
        String pincode;

        @SerializedName("native_place")
        String native_place;

        public int getId() {
            return id;
        }

        public String getFamily_code() {
            return family_code;
        }

        public int getFamily_head_id() {
            return family_head_id;
        }

        public String getAddress_line1() {
            return address_line1;
        }

        public String getAddress_line2() {
            return address_line2;
        }

        public int getState_id() {
            return state_id;
        }

        public int getCity_id() {
            return city_id;
        }

        public String getPincode() {
            return pincode;
        }

        public String getNative_place() {
            return native_place;
        }

        public City_Details getCity() {
            return city;
        }

        public State_Details getState() {
            return state;
        }

        public static class City_Details {
            @SerializedName("id")
            int id;

            @SerializedName("city")
            String city;

            public int getId() {
                return id;
            }

            public String getCity() {
                return city;
            }
        }

        public static class State_Details {
            @SerializedName("id")
            int id;

            @SerializedName("name")
            String name;

            public int getId() {
                return id;
            }

            public String getName() {
                return name;
            }
        }
    }
}
