package oswal.jain.samaj.data.models;

public class ViewPagerModel {
    int image;

    public ViewPagerModel() {
    }

    public ViewPagerModel(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
