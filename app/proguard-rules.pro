# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

## retrofit
-keep class com.squareup.okhttp.* { *; }
-keep class retrofit.* { *; }
-keepnames class com.squareup.okhttp.* { *; }
-keep interface com.squareup.okhttp.* { *; }

-dontwarn com.squareup.okhttp.**
-dontwarn okio.**
-dontwarn retrofit.**
-dontwarn rx.**
-dontwarn com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

#-keep class com.jain.samaj.ui.activities.** { *; }
#
## keep everything in this package from being renamed only
#-keepnames class com.jain.samaj.ui.activities.** { *; }

# keep everything in this package from being removed or renamed
-keep class oswal.jain.samaj.data.models.** { *; }

# keep everything in this package from being renamed only
-keepnames class oswal.jain.samaj.data.models.** { *; }